package de.uni.stuttgart.tickettoride;

import com.badlogic.gdx.ApplicationAdapter;

import de.uni.stuttgart.tickettoride.data.Data;
import de.uni.stuttgart.tickettoride.gui.MainScreen;
import lib.gui.osm.OSMConfiguration;
import lib.gui.osm.OSMPanel;

import lib.gui.*;

public class Main extends ApplicationAdapter {

    public static final boolean USE_OPEN_RAILWAY_MAP_TILES = false;

    @Override
    public void create() {
        OSMConfiguration osmConfig = new OSMConfiguration();
        osmConfig.userAgent = "de.uni.stuttgart.tickettoride";
        osmConfig.osmTileProviderURL = "https://tile.openstreetmap.org";

        if (USE_OPEN_RAILWAY_MAP_TILES) {
            osmConfig.osmTileProviderURL = "https://tiles.openrailwaymap.org/standard";
            osmConfig.textureSize = 512;
            osmConfig.cacheFolder = "cache/tiles_railway";
        }

		OSMPanel.setConfig(osmConfig);

		GUIManager.setUp(new GUIConfiguration());

        Data.init();
		new MainScreen();
	}

	@Override
	public void render() {
		GUIManager.render();
	}

	@Override
	public void resize(int width, int height) {
		GUIManager.resize(width, height);
	}

    @Override
    public void dispose() {
        System.exit(0);
    }
}
