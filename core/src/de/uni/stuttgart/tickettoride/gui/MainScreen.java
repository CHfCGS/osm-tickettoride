package de.uni.stuttgart.tickettoride.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import de.uni.stuttgart.tickettoride.Main;
import de.uni.stuttgart.tickettoride.data.Data;
import de.uni.stuttgart.tickettoride.data.graph.GameGraph;
import de.uni.stuttgart.tickettoride.data.graph.StationGraph;
import de.uni.stuttgart.tickettoride.data.graph.StationSelection;
import de.uni.stuttgart.tickettoride.data.graph.original_game.CreateOriginalGameGraph;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.structure.EfficientGraph;
import de.uni.stuttgart.tickettoride.data.graph.structure.Route;
import lib.gui.Draw;
import lib.gui.osm.OSMPanel;
import lib.gui.annotations.ZLayer;
import lib.gui.components.screen.Screen;
import lib.gui.osm.OSMCopyrightOverlay;

public class MainScreen extends Screen {

    @ZLayer(-1)
    public final OSMPanel osmPanel = new OSMPanel();

    @ZLayer(1)
    private final OSMCopyrightOverlay osmCopyright = new OSMCopyrightOverlay();

    @ZLayer(1)
    public final SettingsOverlayPanel settings = new SettingsOverlayPanel();

    private float drawSize;

    @Override
    protected void render() {
        if (!Main.USE_OPEN_RAILWAY_MAP_TILES) {
            Draw.setColor(Color.BLACK);
            Draw.setAlpha(0.5f);
            Draw.rect(this);
            Draw.setAlpha(1f);
        }

        drawSize = osmPanel.getZoomLevel() * osmPanel.getZoomLevel() / 15;

        Draw.setTextAlignment(Align.center);
        Draw.setTextSize(drawSize);

        if (Data.stationGraph != null && settings.showFullGraph.isChecked()) {
            drawGraph(Data.stationGraph, Color.BLUE);
        }
        if (Data.gameGraph != null && settings.showFinalGraph.isChecked()) {
            drawGraph(Data.gameGraph, Color.RED);
        }

        if (CreateOriginalGameGraph.gameGraph != null && settings.showOriginalGraph.isChecked()) {
            drawGraph(CreateOriginalGameGraph.gameGraph, Color.ORANGE);
        }

        Draw.resetColor();
    }


    private void drawGraph(StationGraph graph, Color color) {
        for (TrainStation node : graph.getTrainStations().getAll()) {
            if (!node.isInLargestConnectedComponent()) continue;

            Vector2 pos = osmPanel.getPixelPosition(node);

            // Edge
            graph.prepareGetEdges(node.getTrainStationId());
            int neighborStationId;
            while ((neighborStationId = graph.getNextEdge()) != EfficientGraph.NO_EDGE) {
                TrainStation neighbor = graph.getTrainStations().getStationByTrainStationId(neighborStationId);

                Vector2 pos2 = osmPanel.getPixelPosition(neighbor);

                if (isVisible(pos, pos2)) {
                    Draw.setColor(color);
                    Draw.line(pos, pos2, drawSize * 0.5f);

                    /*
                    int[] nodeIds = graph.getEdgeNodeIds(node.getTrainStationId(), neighborStationId);

                    for (int i = 0; i < nodeIds.length - 1; i++) {

                        Draw.setColor(Color.GREEN);
                        Vector2 pos3 = osmPanel.getMapPixelPosition(graph.getMapX(nodeIds[i]), graph.getMapY(nodeIds[i]));
                        Vector2 pos4 = osmPanel.getMapPixelPosition(graph.getMapX(nodeIds[i + 1]), graph.getMapY(nodeIds[i + 1]));

                        Draw.line(pos3, pos4, drawSize * 0.5f);
                    }
                     */
                }
            }
        }

        for (TrainStation node : graph.getTrainStations().getAll()) {
            if (!node.isInLargestConnectedComponent()) continue;
            Vector2 pos = osmPanel.getPixelPosition(node);

            if (isVisible(pos)) {
                Draw.setColor(Color.WHITE);
                Draw.roundedRect(pos.x - drawSize / 2, pos.y - drawSize / 2, drawSize, drawSize, drawSize / 2, 1f);

                if (osmPanel.getZoomLevel() > 10) {
                    Draw.font(pos.x - drawSize * 30, pos.y + drawSize, drawSize * 60, node.getName());

                    int nodeValue = StationSelection.calculateStationScore(graph, node);

                    Draw.font(pos.x - drawSize * 30, pos.y + drawSize * 2, drawSize * 60, "" + nodeValue);
                }
            }
        }
    }

    private void drawGraph(GameGraph graph, Color color) {
        // Edges
        for (TrainStation node : graph.getNodes()) {
            Vector2 pos = osmPanel.getPixelPosition(node);

            for (Route edge : graph.getEdges(node)) {
                Vector2 pos2 = osmPanel.getPixelPosition(edge.getDestNode(node));

                if (isVisible(pos, pos2)) {
                    edge.drawDebug(pos, pos2, drawSize, color);
                }
            }
        }

        // Nodes
        for (TrainStation node : graph.getNodes()) {
            Vector2 pos = osmPanel.getPixelPosition(node);

            if (isVisible(pos)) {
                Draw.setColor(Color.WHITE);
                Draw.roundedRect(pos.x - drawSize, pos.y - drawSize, drawSize * 2, drawSize * 2, drawSize, 1f);

                if (osmPanel.getZoomLevel() > 2) {
                    Draw.setTextSize(drawSize * 4);
                    Draw.font(pos.x - drawSize * 30, pos.y + drawSize * 1.5f, drawSize * 60, node.getName());
                }
            }
        }
    }

    private boolean isVisible(Vector2 pos) {
        return pos.x >= -Draw.screenW * 0.1f && pos.x <= Draw.screenW * 1.1f
                && pos.y >= -Draw.screenH * 0.1f && pos.y <= Draw.screenH * 1.1f;
    }

    private boolean isVisible(Vector2 pos, Vector2 pos2) {
        return Math.max(pos.x, pos2.x) >= 0 && Math.min(pos.x, pos2.x) <= Draw.screenW
                && Math.max(pos.y, pos2.y) >= 0 && Math.min(pos.y, pos2.y) <= Draw.screenH;
    }
}
