package de.uni.stuttgart.tickettoride.gui;

import de.uni.stuttgart.tickettoride.data.Data;
import lib.gui.annotations.position.*;
import lib.gui.annotations.text.TextValue;
import lib.gui.components.basic.CheckBox;
import lib.gui.components.basic.NumberSpinner;
import lib.gui.components.basic.TextButton;
import lib.gui.components.panel.Panel;
import lib.gui.util.TextRenderer;

public class SettingsOverlayPanel extends Panel {

    // Left
    @PosX(h = 0.01)
    @PosY(h = 0.01)
    @Width(h = 0.3)
    @Height(h = 0.04)
    @TextValue("Show OSM Station Graph")
    final CheckBox showFullGraph = new CheckBox();

    @PosX(h = 0.01)
    @PosY(h = 0.05)
    @Width(h = 0.3)
    @Height(h = 0.04)
    @TextValue("Show  Final Station Graph")
    final CheckBox showFinalGraph = new CheckBox();

    @PosX(h = 0.01)
    @PosY(h = 0.09)
    @Width(h = 0.3)
    @Height(h = 0.04)
    @TextValue("Show Original Game Graph")
    final CheckBox showOriginalGraph = new CheckBox();

    // Right
    @PosX(h = 0.01, align = Alignment.RIGHT)
    @PosY(h = 0.01)
    @Width(h = 0.075)
    @Height(h = 0.04)
    private final NumberSpinner distanceExponent = new NumberSpinner(0, 10);

    @PosX(h = 0.01, align = Alignment.RIGHT)
    @PosY(h = 0.055)
    @Width(h = 0.075)
    @Height(h = 0.04)
    @TextValue("Run")
    private final TextButton recalculate = new TextButton(TextButton.COLOR_BLUE);

    public SettingsOverlayPanel() {
        distanceExponent.setValue(Data.config.getStationDistanceRatioExponent());

        recalculate.setEvent(() -> {
            try {
                Data.config.setStationDistanceRatioExponent(distanceExponent.getValue());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            Data.createGameGraphFromStationGraph();
        });
    }

    @Override
    protected void render() {
        float textX = w.value() - h.value() * 0.385f;
        float textW = h.value() * 0.3f;
        float textH = h.value() * 0.04f;

        TextRenderer.render(textX, h.value() * 0.01f, textW, textH,
                "Distance Exponent:", textW * 0.85f, textH * 0.4f);
    }
}
