package de.uni.stuttgart.tickettoride.data.util;

public final class IntegerPair {

    public static long of(int left, int right) {
        return (((long) left) << 32) | (right & 0xffffffffL);
    }

    public static int getLeft(long pair) {
        return (int) (pair >> 32);
    }

    public static int getRight(long pair) {
        return (int) pair;
    }
}
