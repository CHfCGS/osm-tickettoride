package de.uni.stuttgart.tickettoride.data.util;

import com.badlogic.gdx.files.FileHandle;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class GameGraphConfig {

    @SerializedName("amountOfStations")
    private int amountOfStations;

    @SerializedName("amountOfEdges")
    private int amountOfEdges;

    @SerializedName("amountOfSegments")
    private int amountOfSegments;

    @SerializedName("maxEdgeLengthInSegments")
    private int maxEdgeLengthInSegments;

    /**
     * Used for station selection
     * <p>
     * The relevance of the minimal distance to other stations
     * 0 = distance has no relevance,
     * the higher the exponent the more relevant is the distance
     */
    @SerializedName("stationDistanceRatioExponent")
    private float stationDistanceRatioExponent;

    /**
     * Used for edge selection
     * <p>
     * The relevance of the max perpendicular distance from an edge
     * 1 = no relevance,
     * the higher this value the more relevant is the max perpendicular distance
     */
    @SerializedName("perpendicularDistanceRelevance")
    private float perpendicularDistanceRelevance;

    /**
     * Used for edge selection
     * <p>
     * Defines at which angle the edge score will be double the base score.
     * <p>
     * The minimal angle multiplier is 1 at the max. possible angle of 180 degrees
     * (when there is exactly one edge added to the station).
     * <p>
     * The angle multiplier is indirectly proportional to the angle and is doubled
     * at the angle "edgeScoreDuplicationAngle"
     */
    @SerializedName("edgeScoreDuplicationAngle")
    private float edgeScoreDuplicationAngle;

    private float edgeScoreDuplicationAngleExponent;

    /**
     * Used for edge selection
     * <p>
     * This is a hard border, smaller angles than this
     * are not accepted and the edge will not be added
     */
    @SerializedName("smallestAllowedAngle")
    private float smallestAllowedAngle;

    /**
     * Used for edge selection
     * <p>
     * This is a hard border, larger max. perpendicular distances
     * than this are not accepted and the edge will not be added
     */
    @SerializedName("largestAllowedPerpendicularDistance")
    private float largestAllowedPerpendicularDistance;

    public static GameGraphConfig loadFromConfigFile(FileHandle fileHandle) {
        GameGraphConfig config = new Gson().fromJson(fileHandle.readString(), GameGraphConfig.class);

        config.setEdgeScoreDuplicationAngle(config.edgeScoreDuplicationAngle);

        return config;
    }

    public int getAmountOfStations() {
        return amountOfStations;
    }

    public void setAmountOfStations(int amountOfStations) {
        this.amountOfStations = amountOfStations;
    }

    public int getAmountOfEdges() {
        return amountOfEdges;
    }

    public void setAmountOfEdges(int amountOfEdges) {
        this.amountOfEdges = amountOfEdges;
    }

    public int getAmountOfSegments() {
        return amountOfSegments;
    }

    public void setAmountOfSegments(int amountOfSegments) {
        this.amountOfSegments = amountOfSegments;
    }

    public float getStationDistanceRatioExponent() {
        return stationDistanceRatioExponent;
    }

    public void setStationDistanceRatioExponent(float stationDistanceRatioExponent) {
        this.stationDistanceRatioExponent = stationDistanceRatioExponent;
    }

    public float getPerpendicularDistanceRelevance() {
        return perpendicularDistanceRelevance;
    }

    public void setPerpendicularDistanceRelevance(float perpendicularDistanceRelevance) {
        this.perpendicularDistanceRelevance = perpendicularDistanceRelevance;
    }

    public float getEdgeScoreDuplicationAngleExponent() {
        return edgeScoreDuplicationAngleExponent;
    }

    public float getEdgeScoreDuplicationAngle() {
        return edgeScoreDuplicationAngle;
    }

    public void setEdgeScoreDuplicationAngle(float edgeScoreDuplicationAngle) {
        this.edgeScoreDuplicationAngle = edgeScoreDuplicationAngle;

        this.edgeScoreDuplicationAngleExponent = (float) (
                -Math.log(2) / Math.log((1 / 360f) * edgeScoreDuplicationAngle)
        );
    }

    public float getSmallestAllowedAngle() {
        return smallestAllowedAngle;
    }

    public void setSmallestAllowedAngle(float smallestAllowedAngle) {
        this.smallestAllowedAngle = smallestAllowedAngle;
    }

    public float getLargestAllowedPerpendicularDistance() {
        return largestAllowedPerpendicularDistance;
    }

    public void setLargestAllowedPerpendicularDistance(float largestAllowedPerpendicularDistance) {
        this.largestAllowedPerpendicularDistance = largestAllowedPerpendicularDistance;
    }

    public int getMaxEdgeLengthInSegments() {
        return maxEdgeLengthInSegments;
    }

    public void setMaxEdgeLengthInSegments(int maxEdgeLengthInSegments) {
        this.maxEdgeLengthInSegments = maxEdgeLengthInSegments;
    }
}
