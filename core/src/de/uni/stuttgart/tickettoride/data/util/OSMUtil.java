package de.uni.stuttgart.tickettoride.data.util;

import com.badlogic.gdx.math.MathUtils;
import de.uni.stuttgart.tickettoride.data.graph.structure.GraphNode;
import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleNode;
import org.openstreetmap.osmosis.core.domain.v0_6.Tag;

public class OSMUtil {

    /**
     * Checks if the tag matches with the key and value
     */
    public static boolean matches(Tag tag, String key, String value) {
        return key.equalsIgnoreCase(tag.getKey()) && value.equalsIgnoreCase(tag.getValue());
    }

    public static void analyseTag(Tag tag, SimpleNode node) {
        if (OSMUtil.isStationMainNode(tag)) {
            node.setStationMainNode(true);
        }
        if (OSMUtil.isPartOfTrainStation(tag)) {
            node.setIsPartOfTrainStation(true);
        }

        checkIfTagFitsForTrainStation(tag, node);
        searchForStationName(tag, node);
    }

    /**
     * @param tag the checked tag
     * @return if the tag signals that the node belongs to a train station
     */
    public static boolean isPartOfTrainStation(Tag tag) {
        return isStationMainNode(tag)
                || matches(tag, "public_transport", "stop_position")
                || matches(tag, "railway", "stop");
    }

    /**
     * @param tag the checked tag
     * @return if the tag signals that the node is station main node
     */
    public static boolean isStationMainNode(Tag tag) {
        return matches(tag, "railway", "station")
                || matches(tag, "railway", "halt");
    }

    /**
     * Determines if the provided node is a train station
     * (and not e.g. a bus station)
     * <p>
     * If there are no tags specifying it,
     * the method assumes it is a train station
     */
    public static void checkIfTagFitsForTrainStation(Tag tag, SimpleNode node) {

        if (tag.getKey().equals("train")) {

            if (tag.getValue().equals("yes")) {
                node.positiveTrainStationTagsFound();
            } else {
                node.negativeTrainStationTagsFound();
            }

        } else if (OSMUtil.matches(tag, "subway", "yes")
                || OSMUtil.matches(tag, "monorail", "yes")
                || OSMUtil.matches(tag, "tram", "yes")
                || OSMUtil.matches(tag, "light_rail", "yes")) {

            node.negativeTrainStationTagsFound();

        } else if (OSMUtil.matches(tag, "bus", "yes")
                || OSMUtil.matches(tag, "highway", "bus_stop")
                || OSMUtil.matches(tag, "trolleybus", "yes")
                || OSMUtil.matches(tag, "aerialway", "yes")
                || OSMUtil.matches(tag, "aerialway", "station")
                || OSMUtil.matches(tag, "ferry", "yes")) {

            node.negativeTrainStationTagsFound();
        }

    }

    /**
     * Returns the value of the tag 'name'
     * <p>
     * If this tag does not exist '-' is returned
     */
    public static void searchForStationName(Tag tag, SimpleNode node) {

        if (tag.getKey().equals("name")) {
            node.setStationName(tag.getValue());
        } else if (tag.getKey().contains("name")) {
            if (node.getStationNameRaw() == null) {
                node.setStationName(tag.getValue());
            }
        }
    }

    public static boolean isRailway(String key, String value) {
        return key.equals("railway") && value.equals("rail");
    }

    public static boolean isTrainRouteRelation(String key, String value) {

        if (key.equals("route")) {
            return value.equals("train")
                    || value.equals("railway")
                    || value.equals("tracks");
        }

        return false;
    }

    public static float getDistanceInMeters(GraphNode node1, SimpleNode node2) {
        return getDistanceInMeters(node1.getLatitude(), node1.getLongitude(), node2.getLatitude(), node2.getLongitude());
    }

    public static float getDistanceInMeters(GraphNode node1, GraphNode node2) {
        return getDistanceInMeters(node1.getLatitude(), node1.getLongitude(), node2.getLatitude(), node2.getLongitude());
    }

    /**
     * See https://www.mkompf.com/gps/distcalc.html#improved_method for information about the calculation
     * <p>
     * This method is not exact for large distances, because the calculation is approximate.
     */
    public static float getDistanceInMeters(double lat1, double lon1, double lat2, double lon2) {
        final double metersPerLatitude = 111_300d;
        final double metersPerLongitude = 111_300d * Math.cos((lat1 + lat2) / 2 * MathUtils.degreesToRadians);

        double dX = metersPerLongitude * (lon1 - lon2);
        double dY = metersPerLatitude * (lat1 - lat2);

        return (float) Math.sqrt(dX * dX + dY * dY);
    }
}
