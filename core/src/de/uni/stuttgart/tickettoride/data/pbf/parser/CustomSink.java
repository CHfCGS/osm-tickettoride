package de.uni.stuttgart.tickettoride.data.pbf.parser;

import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleNode;
import org.openstreetmap.osmosis.core.domain.v0_6.Entity;

public interface CustomSink {
    void process(Entity var1);
    void processNode(SimpleNode node);
}
