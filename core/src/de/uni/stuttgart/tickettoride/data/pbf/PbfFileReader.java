package de.uni.stuttgart.tickettoride.data.pbf;

import java.io.FileInputStream;
import java.io.IOException;

import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleNode;
import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleRelation;
import de.uni.stuttgart.tickettoride.data.pbf.parser.CustomOsmosisBinaryParser;
import de.uni.stuttgart.tickettoride.data.pbf.parser.CustomSink;
import de.uni.stuttgart.tickettoride.data.pbf.parser.PbfOsmIdConverter;
import lib.gui.util.FormatString;
import org.openstreetmap.osmosis.core.domain.v0_6.*;

public class PbfFileReader implements CustomSink {

    private final String filePathInput;
    private CustomOsmosisBinaryParser parser;

    private OSMNodeReceiver osmNodeReceiver;

    public PbfFileReader(String filePathInput) {
        this.filePathInput = filePathInput;
    }

    public void setOsmNodeReceiver(OSMNodeReceiver osmNodeReceiver) {
        this.osmNodeReceiver = osmNodeReceiver;
    }

    public void readInputFile() {
        try {
            System.out.println("Preparing IdConverter...");
            PbfOsmIdConverter idConverter = new PbfOsmIdConverter();
            idConverter.start(new FileInputStream(filePathInput));

            System.out.println("-> " + FormatString.format(idConverter.getAmountOfNodes()) + " Nodes");
            System.out.println("-> " + FormatString.format(idConverter.getAmountOfWays()) + " Ways");
            System.out.println("-> " + FormatString.format(idConverter.getAmountOfTrainRouteRelations()) + " Relations");

            System.out.println("Loading Entities...");
            parser = new CustomOsmosisBinaryParser(idConverter);
            parser.setSink(this);
            parser.start(new FileInputStream(filePathInput));

            idConverter.dispose();

            System.out.println("Pbf import finished!");
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public double[] getAllNodesLatitude() {
        return parser.getAllNodesLatitude();
    }

    public double[] getAllNodesLongitude() {
        return parser.getAllNodesLongitude();
    }

    /**
     * @return an array of ways.
     * Each way element is an array of way nodes.
     * Ways contain null arrays for ways that are not a railway.
     */
    public int[][] getAllWaysWayNodes() {
        return parser.getAllWaysWayNodes();
    }

    public SimpleRelation[] getAllTrainRouteRelations() {
        return parser.getAllTrainRouteRelations();
    }

    @Override
    public void process(Entity entity) {
    }

    @Override
    public void processNode(SimpleNode node) {
        if (osmNodeReceiver != null) {
            osmNodeReceiver.nodeRead(node);
        }
    }
}