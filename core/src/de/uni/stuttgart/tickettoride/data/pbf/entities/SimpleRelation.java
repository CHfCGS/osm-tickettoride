package de.uni.stuttgart.tickettoride.data.pbf.entities;

public class SimpleRelation {

    /**
     * Relations can contain ways, which are not part of the pbf file.
     * In this case the id is negative.
     * Otherwise, the id is always positive.
     */
    private final int[] wayMemberIds;

    public SimpleRelation(int[] wayMemberIds) {
        this.wayMemberIds = wayMemberIds;
    }

    public int[] getWayMemberIds() {
        return wayMemberIds;
    }
}
