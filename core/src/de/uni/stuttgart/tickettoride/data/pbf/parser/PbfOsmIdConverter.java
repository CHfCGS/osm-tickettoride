package de.uni.stuttgart.tickettoride.data.pbf.parser;

import de.uni.stuttgart.tickettoride.data.util.OSMUtil;
import org.openstreetmap.osmosis.osmbinary.BinaryParser;
import org.openstreetmap.osmosis.osmbinary.Osmformat;
import org.openstreetmap.osmosis.osmbinary.file.BlockInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * This class reads all ids into a long array.
 * After reading all ids the array is sorted.
 * <p>
 * The new converted id is the index of the old id in the array,
 * which can be found using binary search in O(log(n))
 * <p>
 * (These steps are done separately twice, for node and way ids)
 */
public class PbfOsmIdConverter extends BinaryParser {

    private int amountOfNodes;
    private int amountOfWays;
    private int amountOfTrainRouteRelations;

    private long[] nodeIds;
    private long[] wayIds;

    public PbfOsmIdConverter() {
        final int INITIAL_CAPACITY = 1000;
        nodeIds = new long[INITIAL_CAPACITY];
        wayIds = new long[INITIAL_CAPACITY];
    }

    public void start(InputStream inputStream) throws IOException {
        BlockInputStream blockInputStream = new BlockInputStream(inputStream, this);
        try {
            blockInputStream.process();
        } finally {
            blockInputStream.close();
        }

        // Trim array to required size
        nodeIds = Arrays.copyOf(nodeIds, amountOfNodes);
        wayIds = Arrays.copyOf(wayIds, amountOfWays);

        // Sort arrays for binary search
        Arrays.sort(nodeIds);
        Arrays.sort(wayIds);
    }

    public int convertNodeId(long originalNodeId) {
        return Arrays.binarySearch(nodeIds, originalNodeId);
    }

    public int convertWayId(long originalWayId) {
        return Arrays.binarySearch(wayIds, originalWayId);
    }

    public int getAmountOfNodes() {
        return amountOfNodes;
    }

    public int getAmountOfWays() {
        return amountOfWays;
    }

    public int getAmountOfTrainRouteRelations() {
        return amountOfTrainRouteRelations;
    }

    public void dispose() {
        nodeIds = null;
        wayIds = null;
    }

    private void checkNodeArrayCapacity() {
        if (amountOfNodes >= nodeIds.length) {
            // +50% capacity when edge array is full
            int newCapacity = nodeIds.length + (nodeIds.length / 2);

            nodeIds = Arrays.copyOf(nodeIds, newCapacity);
        }
    }

    private void checkWayArrayCapacity() {
        if (amountOfWays >= wayIds.length) {
            // +50% capacity when edge array is full
            int newCapacity = wayIds.length + (wayIds.length / 2);

            wayIds = Arrays.copyOf(wayIds, newCapacity);
        }
    }

    @Override
    protected void parseNodes(List<Osmformat.Node> nodes) {
        for (Osmformat.Node i : nodes) {
            checkNodeArrayCapacity();
            nodeIds[amountOfNodes++] = i.getId();
        }
    }

    @Override
    protected void parseDense(Osmformat.DenseNodes nodes) {
        long lastId = 0;

        for (int i = 0; i < nodes.getIdCount(); i++) {
            long id = nodes.getId(i) + lastId;
            lastId = id;

            checkNodeArrayCapacity();
            nodeIds[amountOfNodes++] = id;
        }
    }

    @Override
    protected void parseWays(List<Osmformat.Way> ways) {
        for (Osmformat.Way i : ways) {
            checkWayArrayCapacity();
            wayIds[amountOfWays++] = i.getId();
        }
    }

    @Override
    protected void parseRelations(List<Osmformat.Relation> rels) {
        for (Osmformat.Relation i : rels) {
            // Only count train route relations
            for (int j = 0; j < i.getKeysCount(); j++) {
                if (OSMUtil.isTrainRouteRelation(getStringById(i.getKeys(j)), getStringById(i.getVals(j)))) {
                    amountOfTrainRouteRelations++;
                    break;
                }
            }
        }
    }

    @Override
    public void parse(Osmformat.HeaderBlock block) {
    }

    @Override
    public void complete() {
    }
}