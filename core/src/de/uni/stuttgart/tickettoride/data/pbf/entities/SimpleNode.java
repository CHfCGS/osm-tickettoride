package de.uni.stuttgart.tickettoride.data.pbf.entities;

public class SimpleNode {

    private final int id;
    private final double latitude;
    private final double longitude;
    private String stationName = null;

    private boolean isStationMainNode;
    private boolean isPartOfTrainStation;

    private boolean positiveTrainStationTagsFound;
    private boolean negativeTrainStationTagsFound;

    public SimpleNode(int id, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public boolean isStationMainNode() {
        return isStationMainNode;
    }

    public void setStationMainNode(boolean stationMainNode) {
        isStationMainNode = stationMainNode;
    }

    public boolean isPartOfTrainStation() {
        return isPartOfTrainStation;
    }

    public void setIsPartOfTrainStation(boolean partOfTrainStation) {
        isPartOfTrainStation = partOfTrainStation;
    }

    public String getStationName() {
        if (stationName == null) return "-";
        return stationName;
    }

    public String getStationNameRaw() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    /**
     * Additional check to ensure the station is a train station
     * and not e.g. a bus station
     */
    public boolean tagsFitForTrainStation() {
        if (positiveTrainStationTagsFound) return true;
        return !negativeTrainStationTagsFound;
    }

    /**
     * A "positive" train station tag signals
     * that the station is definitely a train station,
     * even if a "negative" tag was found.
     */
    public void positiveTrainStationTagsFound() {
        positiveTrainStationTagsFound = true;
    }

    /**
     * If no "positive" tag was found, but a "negative" tag
     * was found, the node is probably not a train station,
     * but maybe a bus station
     */
    public void negativeTrainStationTagsFound() {
        negativeTrainStationTagsFound = true;
    }
}
