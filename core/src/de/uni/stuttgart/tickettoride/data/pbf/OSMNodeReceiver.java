package de.uni.stuttgart.tickettoride.data.pbf;

import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleNode;

@FunctionalInterface
public interface OSMNodeReceiver {
    void nodeRead(SimpleNode node);
}
