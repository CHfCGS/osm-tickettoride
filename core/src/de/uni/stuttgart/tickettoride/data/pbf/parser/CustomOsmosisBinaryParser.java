package de.uni.stuttgart.tickettoride.data.pbf.parser;

import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleNode;
import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleRelation;
import de.uni.stuttgart.tickettoride.data.util.OSMUtil;
import org.openstreetmap.osmosis.core.OsmosisConstants;
import org.openstreetmap.osmosis.core.OsmosisRuntimeException;
import org.openstreetmap.osmosis.core.domain.v0_6.*;
import org.openstreetmap.osmosis.osmbinary.BinaryParser;
import org.openstreetmap.osmosis.osmbinary.Osmformat;
import org.openstreetmap.osmosis.osmbinary.file.BlockInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * This class is modified, using this class as source:
 * https://github.com/openstreetmap/osmosis/blob/main/osmosis-pbf/src/main/java/crosby/binary/osmosis/OsmosisBinaryParser.java
 */
public class CustomOsmosisBinaryParser extends BinaryParser {

    private final PbfOsmIdConverter idConverter;
    private int currentRelationIndex;

    private final double[] allNodesLatitude;
    private final double[] allNodesLongitude;
    private final int[][] allWaysWayNodes;
    private final SimpleRelation[] allTrainRouteRelations;

    public CustomOsmosisBinaryParser(PbfOsmIdConverter idConverter) {
        this.idConverter = idConverter;

        allNodesLatitude = new double[idConverter.getAmountOfNodes()];
        allNodesLongitude = new double[idConverter.getAmountOfNodes()];
        allWaysWayNodes = new int[idConverter.getAmountOfWays()][];
        allTrainRouteRelations = new SimpleRelation[idConverter.getAmountOfTrainRouteRelations()];
    }

    public void start(InputStream inputStream) throws IOException {
        BlockInputStream blockInputStream = new BlockInputStream(inputStream, this);
        try {
            blockInputStream.process();
        } finally {
            blockInputStream.close();
        }
    }

    public double[] getAllNodesLatitude() {
        return allNodesLatitude;
    }

    public double[] getAllNodesLongitude() {
        return allNodesLongitude;
    }

    /**
     * @return an array of ways.
     * Each way element is an array of way nodes.
     * Ways contain null arrays for ways that are not a railway.
     */
    public int[][] getAllWaysWayNodes() {
        return allWaysWayNodes;
    }

    public SimpleRelation[] getAllTrainRouteRelations() {
        return allTrainRouteRelations;
    }

    @Override
    protected void parseNodes(List<Osmformat.Node> nodes) {
        for (Osmformat.Node i : nodes) {
            long id = i.getId();

            double latf = parseLat(i.getLat());
            double lonf = parseLon(i.getLon());

            int convertedNodeId = idConverter.convertNodeId(id);
            SimpleNode node = new SimpleNode(convertedNodeId, latf, lonf);

            for (int j = 0; j < i.getKeysCount(); j++) {
                Tag tag = new Tag(getStringById(i.getKeys(j)), getStringById(i.getVals(j)));
                OSMUtil.analyseTag(tag, node);
            }

            allNodesLatitude[convertedNodeId] = latf;
            allNodesLongitude[convertedNodeId] = lonf;
            sink.processNode(node);
        }
    }

    @Override
    protected void parseDense(Osmformat.DenseNodes nodes) {
        long lastId = 0, lastLat = 0, lastLon = 0;

        int j = 0; // Index into the keysvals array.

        // Stuff for dense info
        for (int i = 0; i < nodes.getIdCount(); i++) {
            long lat = nodes.getLat(i) + lastLat;
            lastLat = lat;
            long lon = nodes.getLon(i) + lastLon;
            lastLon = lon;
            long id = nodes.getId(i) + lastId;
            lastId = id;
            double latf = parseLat(lat), lonf = parseLon(lon);

            int convertedNodeId = idConverter.convertNodeId(id);
            SimpleNode node = new SimpleNode(convertedNodeId, latf, lonf);

            // If empty, assume that nothing here has keys or vals.
            if (nodes.getKeysValsCount() > 0) {
                while (nodes.getKeysVals(j) != 0) {
                    int keyid = nodes.getKeysVals(j++);
                    int valid = nodes.getKeysVals(j++);
                    Tag tag = new Tag(getStringById(keyid), getStringById(valid));

                    OSMUtil.analyseTag(tag, node);
                }
                j++; // Skip over the '0' delimiter.
            }

            allNodesLatitude[convertedNodeId] = (float) latf;
            allNodesLongitude[convertedNodeId] = (float) lonf;
            sink.processNode(node);
        }
    }

    @Override
    protected void parseWays(List<Osmformat.Way> ways) {
        for (Osmformat.Way i : ways) {
            boolean isRailway = false;

            for (int j = 0; j < i.getKeysCount(); j++) {
                if (OSMUtil.isRailway(getStringById(i.getKeys(j)), getStringById(i.getVals(j)))) {
                    isRailway = true;
                    break;
                }
            }

            long lastId = 0;

            int[] wayNodes = new int[i.getRefsCount()];

            for (int index = 0; index < i.getRefsCount(); index++) {
                long identifier = lastId + i.getRefs(index);

                wayNodes[index] = idConverter.convertNodeId(identifier);

                lastId = identifier;
            }

            int convertedWayId = idConverter.convertWayId(i.getId());

            if (isRailway) {
                allWaysWayNodes[convertedWayId] = wayNodes;
            }
        }
    }

    @Override
    protected void parseRelations(List<Osmformat.Relation> rels) {
        for (Osmformat.Relation i : rels) {
            boolean isTrainRouteRelation = false;

            for (int j = 0; j < i.getKeysCount(); j++) {
                if (OSMUtil.isTrainRouteRelation(getStringById(i.getKeys(j)), getStringById(i.getVals(j)))) {
                    isTrainRouteRelation = true;
                }
            }

            // Only process train route relations
            if (!isTrainRouteRelation) continue;

            int numWays = 0;

            for (int j = 0; j < i.getMemidsCount(); j++) {
                if (i.getTypes(j) == Osmformat.Relation.MemberType.WAY) {
                    numWays++;
                }
            }

            long lastMemberId = 0;

            int[] wayMemberIds = new int[numWays];
            int index = 0;

            for (int j = 0; j < i.getMemidsCount(); j++) {
                long memberId = lastMemberId + i.getMemids(j);
                lastMemberId = memberId;

                if (i.getTypes(j) == Osmformat.Relation.MemberType.WAY) {
                    wayMemberIds[index++] = idConverter.convertWayId(memberId);
                }
            }

            allTrainRouteRelations[currentRelationIndex++] = new SimpleRelation(wayMemberIds);
        }
    }

    @Override
    public void parse(Osmformat.HeaderBlock block) {
        for (String s : block.getRequiredFeaturesList()) {
            if (s.equals("OsmSchema-V0.6")) continue; // We can parse this.
            if (s.equals("DenseNodes")) continue; // We can parse this.
            throw new OsmosisRuntimeException("File requires unknown feature: " + s);
        }

        if (block.hasBbox()) {
            String source = OsmosisConstants.VERSION;
            if (block.hasSource()) {
                source = block.getSource();
            }

            double multiplier = .000000001;
            double rightf = block.getBbox().getRight() * multiplier;
            double leftf = block.getBbox().getLeft() * multiplier;
            double topf = block.getBbox().getTop() * multiplier;
            double bottomf = block.getBbox().getBottom() * multiplier;

            Bound bounds = new Bound(rightf, leftf, topf, bottomf, source);
            sink.process(bounds);
        }
    }


    /**
     * Sets the osm sink to send data to.
     *
     * @param sink The sink for receiving all produced data.
     */
    public void setSink(CustomSink sink) {
        this.sink = sink;
    }

    private CustomSink sink;

    @Override
    public void complete() {
    }
}