package de.uni.stuttgart.tickettoride.data;

import com.badlogic.gdx.Gdx;
import de.uni.stuttgart.tickettoride.data.graph.*;
import de.uni.stuttgart.tickettoride.data.graph.export.GraphExport;
import de.uni.stuttgart.tickettoride.data.graph.original_game.CreateOriginalGameGraph;
import de.uni.stuttgart.tickettoride.data.pbf.PbfFileReader;
import de.uni.stuttgart.tickettoride.data.util.GameGraphConfig;
import lib.gui.util.Stopwatch;

import java.util.Scanner;

public class Data {

    public static StationGraph stationGraph;
    public static GameGraph gameGraph;
    public static GameGraphConfig config;

    public static void init() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter file path to (filtered) pbf file:");
        String pbfFilePath = scanner.nextLine();

        Stopwatch stopwatch = new Stopwatch();
        PbfFileReader fileReader = new PbfFileReader(pbfFilePath);

        stationGraph = new StationGraphLoader(fileReader).getStationGraph();
        LargestConnectedComponent.findLargestConnectedComponent(stationGraph);

        config = GameGraphConfig.loadFromConfigFile(Gdx.files.local("game_graph_config.json"));

        createGameGraphFromStationGraph();

        System.out.println("DONE (in " + stopwatch.elapsedTimeInS() + "s)");

        CreateOriginalGameGraph.init();
    }

    public static void createGameGraphFromStationGraph() {
        // 1) Select a subset of stations from the station graph
        // These stations will be the train stations of the final game graph
        StationSelection stationSelection = new StationSelection(stationGraph);
        stationSelection.selectStations(config.getAmountOfStations(), config.getStationDistanceRatioExponent());

        // 2) Select a subset of all possible edges between the selected stations
        EdgeSelection edgeSelection = new EdgeSelection(stationGraph, stationSelection.getSelectedStations());

        gameGraph = edgeSelection.selectEdges(config);

        // 3) Add properties (width and length [amount of segments]) to each edge
        EdgePropertiesSelection edgePropertiesSelection = new EdgePropertiesSelection(gameGraph);
        edgePropertiesSelection.addEdgeProperties(config);

        // 4) Export the game graph to a .map file
        GraphExport.exportToMapFile(gameGraph, Gdx.files.local("export.map"));
    }
}
