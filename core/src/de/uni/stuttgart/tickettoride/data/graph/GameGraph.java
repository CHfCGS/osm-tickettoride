package de.uni.stuttgart.tickettoride.data.graph;

import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.structure.Graph;
import de.uni.stuttgart.tickettoride.data.graph.structure.Route;

public class GameGraph extends Graph<TrainStation, Route> {

    private float averageMaxPerpendicularDistance;

    public GameGraph() {
    }

    public void setAverageMaxPerpendicularDistance(float averageMaxPerpendicularDistance) {
        this.averageMaxPerpendicularDistance = averageMaxPerpendicularDistance;
    }

    public float getAverageMaxPerpendicularDistance() {
        return averageMaxPerpendicularDistance;
    }
}
