package de.uni.stuttgart.tickettoride.data.graph;

import com.badlogic.gdx.math.Vector2;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.structure.Route;
import de.uni.stuttgart.tickettoride.data.util.GameGraphConfig;

import java.util.*;

public class EdgeSelection {

    private final GameGraph gameGraph;
    private final List<Route> allPotentialRoutes;

    public EdgeSelection(StationGraph stationGraph, Set<TrainStation> stationsToKeep) {
        gameGraph = new GameGraph();

        // Create allPotentialRoutes list
        allPotentialRoutes = new ArrayList<>();

        for (TrainStation station : stationsToKeep) {
            Dijkstra dijkstra = new Dijkstra(stationGraph, station, stationsToKeep);

            for (int neighborStationId : dijkstra.getFoundDestinationStationIds()) {
                TrainStation neighborStation = stationGraph.getTrainStations().getStationByTrainStationId(neighborStationId);

                Route potentialRoute = new Route(station, neighborStation);

                List<TrainStation> path = dijkstra.getShortestPath(neighborStation);
                potentialRoute.setMaxPerpendicularDistance(calculateMaxPerpendicularDistanceNormalized(stationGraph, path));

                if (!allPotentialRoutes.contains(potentialRoute)) {
                    allPotentialRoutes.add(potentialRoute);
                }
            }
        }
    }

    public GameGraph selectEdges(GameGraphConfig config) {
        float totalMaxPerpendicularDistance = 0f;
        int amountOfEdgesToAdd = config.getAmountOfEdges();

        while (amountOfEdgesToAdd > 0) {
            if (allPotentialRoutes.size() == 0) break;

            allPotentialRoutes.forEach(r -> {
                float score = r.getLengthInMeters();

                score *= (float) Math.pow(config.getPerpendicularDistanceRelevance(), r.getMaxPerpendicularDistance());

                float minAngle = 360;

                for (Route edge : gameGraph.getEdges(r.getNode1())) {
                    float angle = angleInDegrees(r.getNode1(), edge, r);
                    if (angle < minAngle) minAngle = angle;
                }
                for (Route edge : gameGraph.getEdges(r.getNode2())) {
                    float angle = angleInDegrees(r.getNode2(), edge, r);
                    if (angle < minAngle) minAngle = angle;
                }

                if (minAngle == 0f) {
                    score = Float.MAX_VALUE;
                } else {
                    float exponent = config.getEdgeScoreDuplicationAngleExponent();
                    float xScale = 1f / 360f;

                    score *= (float) Math.pow(1 / (minAngle * xScale), exponent);
                }

                r.setMinAngleToOtherRoute(minAngle);
                r.setEdgeSelectionScore(score);
            });

            Optional<Route> potentialRoute = allPotentialRoutes.stream()
                    .min(Comparator.comparingDouble(Route::getEdgeSelectionScore));

            if (!potentialRoute.isPresent()) continue;

            Route edgeNew = potentialRoute.get();

            boolean addEdge = true;

            // Check if new edge intersects with existing edge
            outerLoop:
            for (TrainStation node : gameGraph.getNodes()) {
                for (Route edge : gameGraph.getEdges(node)) {
                    if (edge.intersects(edgeNew)) {
                        addEdge = false;
                        break outerLoop;
                    }
                }
            }

            if (edgeNew.getMinAngleToOtherRoute() < config.getSmallestAllowedAngle()) {
                addEdge = false;
            }
            if (edgeNew.getMaxPerpendicularDistance() > config.getLargestAllowedPerpendicularDistance()) {
                addEdge = false;
            }

            if (addEdge) {
                gameGraph.addEdge(edgeNew);
                totalMaxPerpendicularDistance += edgeNew.getMaxPerpendicularDistance();
                amountOfEdgesToAdd--;
            }

            allPotentialRoutes.remove(edgeNew);
        }

        int addedEdges = config.getAmountOfEdges() - amountOfEdgesToAdd;

        gameGraph.setAverageMaxPerpendicularDistance(totalMaxPerpendicularDistance / addedEdges);

        LargestConnectedComponent.findLargestConnectedComponent(gameGraph);

        return gameGraph;
    }

    /**
     * Calculates the maximum perpendicular distance from the direct line AB,
     * where a and b are the start and end points of the path.
     * <p>
     * The result is the maximum distance of a point p (from path) to AB,
     * divided by the length of AB
     */
    private float calculateMaxPerpendicularDistanceNormalized(StationGraph stationGraph, List<TrainStation> path) {
        // Start and End points of line AB
        Vector2 a = new Vector2((float) path.get(0).getMapX(), (float) path.get(0).getMapY());
        Vector2 b = new Vector2((float) path.get(path.size() - 1).getMapX(), (float) path.get(path.size() - 1).getMapY());

        // Normalized direction vector AB
        Vector2 directionNormalized = new Vector2(b.x - a.x, b.y - a.y);
        float AB_length = directionNormalized.len();
        directionNormalized.nor();

        float maxDistance = 0;

        for (int stationIndex = 0; stationIndex < path.size() - 1; stationIndex++) {
            int[] nodeIds = stationGraph
                    .getEdgeNodeIds(path.get(stationIndex).getTrainStationId(), path.get(stationIndex + 1).getTrainStationId());

            for (int nodeId : nodeIds) {
                Vector2 p = new Vector2(stationGraph.getMapX(nodeId), stationGraph.getMapY(nodeId));

                Vector2 AP = new Vector2(p).sub(a);

                // L = Lotfußpunkt
                float AL_length = AP.dot(directionNormalized);

                Vector2 l = new Vector2(a).mulAdd(directionNormalized, AL_length);

                // Shortest distance from point p to line AB
                float distance = l.dst(p);

                if (distance > maxDistance) {
                    maxDistance = distance;
                }
            }
        }

        return maxDistance / AB_length;
    }

    /**
     * @param intersectionPoint the point where both routes intersect at one of their end points
     * @return the angle between two routes, in degrees
     */
    private float angleInDegrees(TrainStation intersectionPoint, Route route1, Route route2) {
        TrainStation a = (intersectionPoint == route1.getNode1()) ? route1.getNode2() : route1.getNode1();
        TrainStation b = (intersectionPoint == route2.getNode1()) ? route2.getNode2() : route2.getNode1();

        Vector2 vector = new Vector2(
                (float) (a.getMapX() - intersectionPoint.getMapX()),
                (float) (a.getMapY() - intersectionPoint.getMapY()));
        Vector2 vector2 = new Vector2(
                (float) (b.getMapX() - intersectionPoint.getMapX()),
                (float) (b.getMapY() - intersectionPoint.getMapY()));


        float angle = vector.angleDeg(vector2);

        if (angle > 180) angle = 360 - angle;

        return angle;
    }
}
