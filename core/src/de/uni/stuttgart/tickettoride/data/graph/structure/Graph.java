package de.uni.stuttgart.tickettoride.data.graph.structure;


import java.util.*;

/**
 * @param <N> type of the node objects
 * @param <E> type of the edge objects
 */
public class Graph<N extends GraphNode, E extends GraphEdge<N>> {

    /**
     * This HashMap contains the node ids from all nodes  [as keys]
     * and their corresponding node objects [as values].
     */
    private final HashMap<Integer, N> nodeIdToNodeObjectMap;

    /**
     * This HashMap contains all nodes (of nodes with at least one edge) [as keys]
     * and their corresponding edges: a set of nodes [as values].
     */
    private final HashMap<N, Set<E>> nodeToEdgesMap;

    public Graph() {
        nodeIdToNodeObjectMap = new HashMap<>();
        nodeToEdgesMap = new HashMap<>();
    }

    public void addNode(N node) {
        nodeIdToNodeObjectMap.put(node.getNodeId(), node);
        nodeToEdgesMap.put(node, new HashSet<>(4));
    }

    public Collection<N> getNodes() {
        return nodeIdToNodeObjectMap.values();
    }

    public N getNodeById(int nodeId) {
        return nodeIdToNodeObjectMap.get(nodeId);
    }

    public int getNodeDegree(N node) {
        if (!nodeToEdgesMap.containsKey(node)) return 0;
        return nodeToEdgesMap.get(node).size();
    }

    public Set<E> getEdges(N node) {
        if (!nodeToEdgesMap.containsKey(node)) return Collections.emptySet();
        return nodeToEdgesMap.get(node);
    }

    public void addEdge(E edge) {
        if (edge.getNode1() == edge.getNode2()) {
            return;
        }

        addDirectedEdge(edge.getNode1(), edge);
        addDirectedEdge(edge.getNode2(), edge);
    }

    private void addDirectedEdge(N originNode, E edge) {
        Set<E> edges;

        if (nodeToEdgesMap.containsKey(originNode)) {
            edges = nodeToEdgesMap.get(originNode);
        } else {
            nodeIdToNodeObjectMap.put(originNode.getNodeId(), originNode);
            nodeToEdgesMap.put(originNode, edges = new HashSet<>(4));
        }

        edges.add(edge);
    }

}
