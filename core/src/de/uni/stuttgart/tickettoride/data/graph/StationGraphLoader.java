package de.uni.stuttgart.tickettoride.data.graph;

import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStations;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStationsLoader;
import de.uni.stuttgart.tickettoride.data.graph.structure.EfficientGraph;
import de.uni.stuttgart.tickettoride.data.pbf.PbfFileReader;
import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleRelation;

public class StationGraphLoader {

    private final EfficientGraph railwayGraph;
    private final StationGraph stationGraph;

    private int[][] allWaysWayNodes;

    public StationGraphLoader(PbfFileReader fileReader) {
        TrainStationsLoader stationsLoader = new TrainStationsLoader();
        fileReader.setOsmNodeReceiver(stationsLoader::receiveNode);
        fileReader.readInputFile();

        allWaysWayNodes = fileReader.getAllWaysWayNodes();

        TrainStations trainStations = stationsLoader.getTrainStations(fileReader);

        stationGraph = new StationGraph(fileReader, trainStations);

        System.out.println("Creating RailwayGraph...");
        railwayGraph = new EfficientGraph(fileReader.getAllNodesLatitude().length);

        SimpleRelation[] allTrainRouteRelations = fileReader.getAllTrainRouteRelations();

        for (SimpleRelation relation : allTrainRouteRelations) {
            addEdgesToRailwayGraph(relation);
        }

        System.out.println("Create StationGraph...");
        stationGraph.createEdgesUsingRailwayGraph(railwayGraph);

        railwayGraph.dispose();
        allWaysWayNodes = null;
    }

    private void addEdgesToRailwayGraph(SimpleRelation relation) {

        for (int wayId : relation.getWayMemberIds()) {
            // Relations can contain ways, which are not part of the pbf file => Negative wayId
            if (wayId >= 0) {

                int[] wayNodes = allWaysWayNodes[wayId];

                // Memory optimization: (wayNodes == null) <==> !way.isRailway()
                if (wayNodes == null) continue;

                int lastNodeId = wayNodes[0];

                for (int i = 1; i < wayNodes.length; i++) {
                    int nodeId = wayNodes[i];

                    railwayGraph.addEdge(lastNodeId, nodeId);

                    lastNodeId = nodeId;
                }
            }
        }
    }

    public StationGraph getStationGraph() {
        return stationGraph;
    }
}
