package de.uni.stuttgart.tickettoride.data.graph.switches;

import com.badlogic.gdx.math.Vector2;
import de.uni.stuttgart.tickettoride.data.graph.structure.EfficientGraph;
import lib.gui.osm.util.TileMath;

public class SwitchAngles {

    public static RailwaySwitch analyseRailwaySwitch(EfficientGraph graph,
                                                     double[] allNodesLatitude, double[] allNodesLongitude,
                                                     int nodeId) {
        double centerNodeLat = allNodesLatitude[nodeId];
        double centerNodeLon = allNodesLongitude[nodeId];

        RailwaySwitch railwaySwitch = new RailwaySwitch();

        // Calculate angles

        graph.prepareGetEdges(nodeId);
        int neighbourNodeId;

        while ((neighbourNodeId = graph.getNextEdge()) != EfficientGraph.NO_EDGE) {
            double neighbourNodeLat = allNodesLatitude[neighbourNodeId];
            double neighbourNodeLon = allNodesLongitude[neighbourNodeId];

            Vector2 vector = new Vector2(
                    (float) (TileMath.longitudeToMapX(neighbourNodeLon) - TileMath.longitudeToMapX(centerNodeLon)),
                    (float) (TileMath.latitudeToMapY(neighbourNodeLat) - TileMath.latitudeToMapY(centerNodeLat))
            );

            graph.prepareGetEdgesNested(nodeId);
            int otherNeighbourNodeId;
            while ((otherNeighbourNodeId = graph.getNextEdgeNested()) != EfficientGraph.NO_EDGE) {
                if (neighbourNodeId == otherNeighbourNodeId) continue;

                double otherNeighbourNodeLat = allNodesLatitude[otherNeighbourNodeId];
                double otherNeighbourNodeLon = allNodesLongitude[otherNeighbourNodeId];

                Vector2 vector2 = new Vector2(
                        (float) (TileMath.longitudeToMapX(otherNeighbourNodeLon) - TileMath.longitudeToMapX(centerNodeLon)),
                        (float) (TileMath.latitudeToMapY(otherNeighbourNodeLat) - TileMath.latitudeToMapY(centerNodeLat))
                );

                float angle = vector.angleDeg(vector2);

                if (angle > 180) angle = 360 - angle;

                // Use connection if angle > 135
                if (angle > 135) {
                    railwaySwitch.addAllowedRoute(neighbourNodeId, otherNeighbourNodeId);
                }
            }
        }

        return railwaySwitch;
    }
}
