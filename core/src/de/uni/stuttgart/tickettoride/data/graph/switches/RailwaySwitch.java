package de.uni.stuttgart.tickettoride.data.graph.switches;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class RailwaySwitch {

    private final HashMap<Integer, Set<Integer>> nodeIdToEdgesMap = new HashMap<>();

    public void addAllowedRoute(int neighbourNodeId, int otherNeighbourNodeId) {
        addEdge(neighbourNodeId, otherNeighbourNodeId);
        addEdge(otherNeighbourNodeId, neighbourNodeId);
    }

    public Set<Integer> getAllowedEdges(int nodeId) {
        if(!nodeIdToEdgesMap.containsKey(nodeId)) return new HashSet<>();
        return nodeIdToEdgesMap.get(nodeId);
    }

    private void addEdge(int fromNodeId, int toNodeId) {
        Set<Integer> neighbors;

        if (nodeIdToEdgesMap.containsKey(fromNodeId)) {
            neighbors = nodeIdToEdgesMap.get(fromNodeId);
        } else {
            nodeIdToEdgesMap.put(fromNodeId, neighbors = new HashSet<>());
        }

        neighbors.add(toNodeId);
    }
}
