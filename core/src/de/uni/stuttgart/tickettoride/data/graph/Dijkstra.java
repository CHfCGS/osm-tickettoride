package de.uni.stuttgart.tickettoride.data.graph;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.structure.EfficientGraph;
import de.uni.stuttgart.tickettoride.data.util.OSMUtil;
import lib.gui.Draw;
import lib.gui.osm.OSMPanel;

import java.util.*;

public class Dijkstra {

    private final StationGraph stationGraph;
    private final TrainStation startStation;
    private final Set<TrainStation> destinationStations;

    private final float[] costMap;
    private final PriorityQueue<Integer> priorityQueue;
    private final int[] parentMap;

    private final Set<Integer> foundDestinationStationIds;

    /**
     * Calculates the shortest ways (*) to all destination stations
     * (if existent).
     * <p>
     * (*) The shortest way to a destination station without crossing another destination station!
     * Pathfinding will stop at a destination station, similar to a dead end!
     *
     * @param stationGraph        the graph
     * @param startStation        the start station
     * @param destinationStations a set of destination stations
     */
    public Dijkstra(StationGraph stationGraph, TrainStation startStation, Set<TrainStation> destinationStations) {
        this.stationGraph = stationGraph;
        this.startStation = startStation;
        this.destinationStations = destinationStations;

        costMap = new float[stationGraph.getTrainStations().getAmount()];
        parentMap = new int[stationGraph.getTrainStations().getAmount()];
        priorityQueue = new PriorityQueue<>((station1Id, station2Id) -> Float.compare(costMap[station1Id], costMap[station2Id]));

        this.foundDestinationStationIds = new HashSet<>();
        calculateDijkstra();
    }

    /**
     * Calculates distance to all reachable nodes
     */
    private void calculateDijkstra() {
        for (int trainStationId = 0; trainStationId < stationGraph.getAmountOfNodes(); trainStationId++) {

            if (trainStationId == startStation.getTrainStationId()) {
                costMap[trainStationId] = 0;
                parentMap[trainStationId] = trainStationId;
            } else {
                costMap[trainStationId] = Float.POSITIVE_INFINITY;
                parentMap[trainStationId] = -1;
            }
        }

        priorityQueue.add(startStation.getTrainStationId());

        while (!priorityQueue.isEmpty()) {
            int nextStationId = priorityQueue.poll();
            TrainStation nextStation = stationGraph.getTrainStations().getStationByTrainStationId(nextStationId);


            stationGraph.prepareGetEdges(nextStationId);
            int neighborStationId;
            while ((neighborStationId = stationGraph.getNextEdge()) != EfficientGraph.NO_EDGE) {
                TrainStation neighborStation = stationGraph.getTrainStations().getStationByTrainStationId(neighborStationId);

                float cost = OSMUtil.getDistanceInMeters(nextStation, neighborStation);

                if (costMap[nextStationId] + cost < costMap[neighborStationId]) {

                    costMap[neighborStationId] = costMap[nextStationId] + cost;

                    parentMap[neighborStationId] = nextStationId;
                    priorityQueue.remove(neighborStationId);

                    if (destinationStations.contains(neighborStation)) {
                        foundDestinationStationIds.add(neighborStationId);
                    }
                    // Only add station to queue again, if it is not a destination station
                    else {
                        priorityQueue.add(neighborStationId);
                    }
                }
            }
        }
    }

    /**
     * Returns the shortest path (*) from the start station the destination station
     * <p>
     * (*) The shortest way to a destination station without crossing another destination station!
     * Pathfinding will stop at a destination station, similar to a dead end!
     *
     * @param destinationStation NodeID of destination
     * @return path sequence of TrainStations or null when no path exists
     */
    public LinkedList<TrainStation> getShortestPath(TrainStation destinationStation) {
        LinkedList<TrainStation> pathNodes = new LinkedList<>();

        TrainStation currentStation = destinationStation;

        while (currentStation != startStation) {
            pathNodes.addFirst(currentStation);

            currentStation = stationGraph.getTrainStations()
                    .getStationByTrainStationId(parentMap[currentStation.getTrainStationId()]);
            if (currentStation == null) return null;
        }
        pathNodes.addFirst(currentStation);
        return pathNodes;
    }

    /**
     * @return the distance in meters
     */
    public double getDistanceTo(TrainStation destinationStation) {
        return costMap[destinationStation.getTrainStationId()];
    }

    public Set<Integer> getFoundDestinationStationIds() {
        return foundDestinationStationIds;
    }

    public void drawDebug(OSMPanel osmPanel, float drawSize) {
        for (TrainStation destinationStation : destinationStations) {
            if (parentMap[destinationStation.getTrainStationId()] < 0) {
                Draw.setColor(Color.RED); // No path
            } else {
                Draw.setColor(Color.GREEN);
                TrainStation currentStation = destinationStation;

                while (currentStation != startStation) {
                    Vector2 pos1 = osmPanel.getPixelPosition(currentStation);

                    currentStation = stationGraph.getTrainStations()
                            .getStationByTrainStationId(parentMap[currentStation.getTrainStationId()]);

                    Vector2 pos2 = osmPanel.getPixelPosition(currentStation);

                    Draw.line(pos1, pos2, drawSize * 0.5f);
                }
            }

            Vector2 pos = osmPanel.getPixelPosition(destinationStation);
            Draw.rect(pos.x - drawSize, pos.y - drawSize, drawSize * 2, drawSize * 2);
        }
    }
}