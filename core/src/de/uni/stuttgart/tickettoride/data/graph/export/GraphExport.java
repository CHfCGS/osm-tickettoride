package de.uni.stuttgart.tickettoride.data.graph.export;

import com.badlogic.gdx.files.FileHandle;
import de.uni.stuttgart.tickettoride.data.graph.GameGraph;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.structure.Route;

import java.util.HashSet;
import java.util.Set;

public class GraphExport {

    private static final String EDGE_PREFIX = "|";

    public static void exportToMapFile(GameGraph gameGraph, FileHandle fileHandle) {
        StringBuilder builder = new StringBuilder();

        removeDuplicateStationNames(gameGraph);
        createHeader(builder, gameGraph);

        Set<TrainStation> addedNodes = new HashSet<>();

        for (TrainStation trainStation : gameGraph.getNodes()) {
            builder.append(trainStation.getName());
            builder.append("; ");
            builder.append((float) trainStation.getLatitude());
            builder.append("; ");
            builder.append((float) trainStation.getLongitude());
            builder.append("\n");

            for (Route edge : gameGraph.getEdges(trainStation)) {
                TrainStation destNode = edge.getDestNode(trainStation);

                // Add every edge only once
                if (!addedNodes.contains(destNode)) {
                    final int edgeLength = edge.getNumSegments();
                    final int edgeWidth = edge.getColors().length;

                    for (int i = 0; i < edgeWidth; i++) {
                        builder.append(EDGE_PREFIX);
                    }
                    builder.append(" ");
                    builder.append(destNode.getName());
                    builder.append("; ");
                    builder.append(edgeLength);
                    builder.append("\n");
                }
            }

            addedNodes.add(trainStation);
        }

        fileHandle.writeString(builder.toString(), false, "UTF-8");
    }

    private static void createHeader(StringBuilder builder, GameGraph gameGraph) {
        double latSum = 0;
        double lonSum = 0;

        for (TrainStation station : gameGraph.getNodes()) {
            latSum += station.getLatitude();
            lonSum += station.getLongitude();
        }

        builder.append("map-center: ");
        builder.append((float) (latSum / gameGraph.getNodes().size()));
        builder.append(" ");
        builder.append((float) (lonSum / gameGraph.getNodes().size()));
        builder.append("\n");
        builder.append("map-zoom: 4");
        builder.append("\n");
        builder.append("\n");
    }

    /**
     * While it is very unlikely that this will happen, it is possible in theory
     * that two train stations have the exact same station name.
     * <p>
     * The train station names need to be distinct, therefore this method
     * will detect duplicates and rename them.
     */
    private static void removeDuplicateStationNames(GameGraph gameGraph) {
        HashSet<String> names = new HashSet<>();

        for (TrainStation station : gameGraph.getNodes()) {
            if (names.contains(station.getName())) {

                String newName;
                int counter = 2;

                do {
                    newName = station.getName() + " (" + counter + ")";
                    counter++;
                } while (names.contains(newName));

                station.setName(newName);
            }

            names.add(station.getName());
        }
    }
}
