package de.uni.stuttgart.tickettoride.data.graph.structure;

public interface GraphNode {

    int getNodeId();

    double getLatitude();

    double getLongitude();
}
