package de.uni.stuttgart.tickettoride.data.graph;

import de.uni.stuttgart.tickettoride.data.graph.structure.EfficientGraph;
import de.uni.stuttgart.tickettoride.data.graph.switches.RailwaySwitch;
import de.uni.stuttgart.tickettoride.data.graph.switches.SwitchAngles;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStations;
import de.uni.stuttgart.tickettoride.data.pbf.PbfFileReader;
import de.uni.stuttgart.tickettoride.data.util.IntegerPair;
import lib.gui.osm.util.TileMath;

import java.util.*;

/**
 * The Station Graph represents the network of all train station
 * and their connected neighbor train stations.
 * <p>
 * When using the constructor StationGraph(PbfFileReader fileReader),
 * the graph is generated automatically from a .osm.pbf file
 */
public class StationGraph extends EfficientGraph {

    private final TrainStations trainStations;

    private final double[] allNodesLatitude;
    private final double[] allNodesLongitude;

    /**
     * Saves a list of ids for all edges.
     * <p>
     * This list are all nodeIds of nodes
     * between the two train stations from the edge
     */
    private int[][] edgeNodes = new int[INITIAL_CAPACITY][];

    public StationGraph(PbfFileReader fileReader, TrainStations trainStations) {
        super(trainStations.getAmount());
        this.trainStations = trainStations;

        allNodesLatitude = fileReader.getAllNodesLatitude();
        allNodesLongitude = fileReader.getAllNodesLongitude();
    }

    private void checkEdgeNodesArrayCapacity() {
        if (newEdgeInsertionIndex >= edgeNodes.length - 1) {
            // +50% capacity when edge arrays are full
            int newCapacity = edgeNodes.length + (edgeNodes.length / 2);
            edgeNodes = Arrays.copyOf(edgeNodes, newCapacity);
        }
    }

    private void addEdge(int trainStationId1, int trainStationId2, int[] edgeNodes) {
        if (trainStationId1 == trainStationId2) return;

        checkEdgeNodesArrayCapacity();

        // In case an edge did not get added (because it already exists),
        // this array entry will get overwritten, therefore a check is not necessary
        this.edgeNodes[newEdgeInsertionIndex] = edgeNodes;
        addDirectedEdge(trainStationId1, trainStationId2);

        this.edgeNodes[newEdgeInsertionIndex] = edgeNodes;
        addDirectedEdge(trainStationId2, trainStationId1);
    }

    /**
     * Returns a list of all nodeIds from all nodes which are part
     * of the edge from trainStationId1 to trainStationId2
     */
    public int[] getEdgeNodeIds(int trainStationId1, int trainStationId2) {
        prepareGetEdges(trainStationId1);

        int edgeTrainStationId;
        while ((edgeTrainStationId = getNextEdge()) != NO_EDGE) {
            if (edgeTrainStationId == trainStationId2) {
                return edgeNodes[currentEdgeArrayIndex];
            }
        }

        return new int[0];
    }

    public void createEdgesUsingRailwayGraph(EfficientGraph railwayGraph) {
        // Extract Train Stations and their connections from the railway graph
        for (TrainStation station : trainStations.getAll()) {
            findAdjacentTrainStations(station, railwayGraph);
        }
    }


    private void findAdjacentTrainStations(TrainStation startTrainStation, EfficientGraph graph) {
        Set<Integer> visitedNodes = new HashSet<>();
        HashMap<Integer, Integer> parentMap = new HashMap<>();

        // Last Node, Node to Visit
        LinkedList<Long> nodesToVisit = new LinkedList<>(); // Long = IntegerPair

        for (int startStationNodeId : startTrainStation.stationNodes) {
            graph.prepareGetEdges(startStationNodeId);
            int neighborNodeId;

            while ((neighborNodeId = graph.getNextEdge()) != EfficientGraph.NO_EDGE) {
                nodesToVisit.addLast(IntegerPair.of(startStationNodeId, neighborNodeId));
                visitedNodes.add(startStationNodeId);

                parentMap.put(neighborNodeId, startStationNodeId);
            }
        }

        while (nodesToVisit.size() > 0) {
            long pairElement = nodesToVisit.removeFirst();
            int lastNode = IntegerPair.getLeft(pairElement);
            int nodeToVisit = IntegerPair.getRight(pairElement);

            visitedNodes.add(nodeToVisit);

            TrainStation trainStation = trainStations.getStationByNodeId(nodeToVisit);

            // TrainStation found
            if (trainStation != null) {
                if (trainStation.getTrainStationId() == startTrainStation.getTrainStationId()) continue;

                // Determine size of edgeNodes array
                int arraySize = 1;

                int nodeHistory = nodeToVisit;
                while (!startTrainStation.stationNodes.contains(nodeHistory)) {
                    nodeHistory = parentMap.get(nodeHistory);
                    arraySize++;
                }

                // Fill array using parentMap map
                int[] edgeNodes = new int[arraySize];
                arraySize = 0;

                nodeHistory = nodeToVisit;
                edgeNodes[arraySize++] = nodeHistory;
                while (!startTrainStation.stationNodes.contains(nodeHistory)) {
                    nodeHistory = parentMap.get(nodeHistory);
                    edgeNodes[arraySize++] = nodeHistory;
                }

                // Create Edge
                addEdge(startTrainStation.getTrainStationId(), trainStation.getTrainStationId(), edgeNodes);

            } else if (graph.getNodeDegree(nodeToVisit) >= 3) {

                RailwaySwitch railwaySwitch = SwitchAngles.analyseRailwaySwitch(graph, allNodesLatitude, allNodesLongitude, nodeToVisit);

                graph.prepareGetEdges(nodeToVisit);
                int nextNodeId;

                while ((nextNodeId = graph.getNextEdge()) != EfficientGraph.NO_EDGE) {
                    if (!visitedNodes.contains(nextNodeId)) {

                        if (railwaySwitch.getAllowedEdges(lastNode).contains(nextNodeId)) {
                            nodesToVisit.addLast(IntegerPair.of(nodeToVisit, nextNodeId));
                            parentMap.put(nextNodeId, nodeToVisit);
                        }
                    }
                }

            } else if (graph.getNodeDegree(nodeToVisit) == 2) {

                graph.prepareGetEdges(nodeToVisit);
                int nextNodeId;

                while ((nextNodeId = graph.getNextEdge()) != EfficientGraph.NO_EDGE) {
                    if (!visitedNodes.contains(nextNodeId)) {
                        nodesToVisit.addLast(IntegerPair.of(nodeToVisit, nextNodeId));
                        parentMap.put(nextNodeId, nodeToVisit);
                    }
                }

            }
        }
    }

    public TrainStations getTrainStations() {
        return trainStations;
    }

    public float getMapX(int nodeId) {
        return (float) TileMath.longitudeToMapX(allNodesLongitude[nodeId]);
    }

    public float getMapY(int nodeId) {
        return (float) TileMath.latitudeToMapY(allNodesLatitude[nodeId]);
    }
}
