package de.uni.stuttgart.tickettoride.data.graph.original_game;

import de.uni.stuttgart.tickettoride.data.graph.GameGraph;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.structure.Route;

import java.util.HashMap;

public class GameGraphAnalysis extends GameGraph {

    private final HashMap<Integer, Integer> numRoutesPerColor = new HashMap<>();
    private final HashMap<Integer, Integer> numSegmentsPerColor = new HashMap<>();
    private final HashMap<Integer, Integer> numRoutesPerNumSegments = new HashMap<>();

    private int numStations;
    private int numRoutes;
    private int numTotalRoutes;
    private int numTotalSegments;

    @Override
    public void addNode(TrainStation node) {
        super.addNode(node);

        numStations++;
    }

    @Override
    public void addEdge(Route edge) {
        super.addEdge(edge);

        numRoutes++;
        numTotalRoutes += edge.getColors().length;
        numTotalSegments += edge.getColors().length * edge.getNumSegments();

        for (int color : edge.getColors()) {
            numRoutesPerColor.put(color, numRoutesPerColor.getOrDefault(color, 0) + 1);
            numSegmentsPerColor.put(color, numSegmentsPerColor.getOrDefault(color, 0) + edge.getNumSegments());
        }

        numRoutesPerNumSegments.put(edge.getNumSegments(),
                numRoutesPerNumSegments.getOrDefault(edge.getNumSegments(), 0) + 1);
    }

    public void printGraphAnalysis() {
        System.out.println();
        System.out.println("--- General ---");

        System.out.println("Amount of Stations: " + numStations);
        System.out.println("Amount of different Routes: " + numRoutes); // Double routes count as one route
        System.out.println("Amount of total Routes: " + numTotalRoutes); // Double routes count as two routes
        System.out.println("Amount total Segments: " + numTotalSegments);

        System.out.println();
        System.out.println("--- Amount of different Routes by Amount of Segments ---");
        for (int segments : numRoutesPerNumSegments.keySet()) {
            System.out.println(segments + " Segments: " + numRoutesPerNumSegments.get(segments));
        }

        System.out.println();
        System.out.println("--- Colors ---");
        System.out.println("Total Routes per Color:");
        for (int color : numRoutesPerColor.keySet()) {
            System.out.println(color + ": " + numRoutesPerColor.get(color));
        }

        System.out.println();
        System.out.println("Segments per Color:");
        for (int color : numSegmentsPerColor.keySet()) {
            System.out.println(color + ": " + numSegmentsPerColor.get(color));
        }

        System.out.println();
        System.out.println("--- Node degree ---");
        System.out.println("Min: " + getNodes().stream().mapToInt(n -> getEdges(n).size()).min().getAsInt());
        System.out.println("Max: " + getNodes().stream().mapToInt(n -> getEdges(n).size()).max().getAsInt());
        System.out.println("Avg: " + (getNodes().stream().mapToInt(n -> getEdges(n).size()).sum() / (float) numStations));
    }
}
