package de.uni.stuttgart.tickettoride.data.graph.stations;

import de.uni.stuttgart.tickettoride.data.pbf.PbfFileReader;
import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleNode;
import de.uni.stuttgart.tickettoride.data.util.OSMUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Loads and creates the TrainStations from a pbf file
 */
public class TrainStationsLoader {

    /**
     * The TrainStations object,
     * created by this loader class
     */
    private final TrainStations trainStations;

    /**
     * Temporary filtered list of all nodes,
     * containing all nodes that are
     * potentially part of a station
     */
    private final List<SimpleNode> potentialStationNodes;

    public TrainStationsLoader() {
        trainStations = new TrainStations();
        potentialStationNodes = new ArrayList<>();
    }

    /**
     * This method must be passed to PbfFileReader#setOsmNodeReceiver
     * before calling PbfFileReader::readInputFile
     */
    public void receiveNode(SimpleNode node) {
        if (node.isPartOfTrainStation()) {
            potentialStationNodes.add(node);
        }
    }

    /**
     * @return the TrainStations, loaded from a pbf file
     */
    public TrainStations getTrainStations(PbfFileReader fileReader) {
        createStationsFromNodeList();
        addNearNonTaggedNodesToStation(fileReader);

        return trainStations;
    }

    private void addNearNonTaggedNodesToStation(PbfFileReader fileReader) {
        double[] allNodesLatitude = fileReader.getAllNodesLatitude();
        double[] allNodesLongitude = fileReader.getAllNodesLongitude();

        for (int nodeId = 0; nodeId < allNodesLatitude.length; nodeId++) {
            if (trainStations.isPartOfStation(nodeId)) continue;

            double lat = allNodesLatitude[nodeId];
            double lon = allNodesLongitude[nodeId];

            TrainStation nearestStation = trainStations.getNearestTrainStationUsingKDTree(lat, lon);

            if (OSMUtil.getDistanceInMeters(
                    nearestStation.getLatitude(), nearestStation.getLongitude(), lat, lon) < 250) {

                trainStations.registerNodeBelongingToStation(nearestStation, nodeId, false);
            }
        }
    }

    private void createStationsFromNodeList() {
        for (SimpleNode node : potentialStationNodes) {
            checkAndAddTrainStation(node);
        }
        trainStations.createKDTree();
        for (SimpleNode node : potentialStationNodes) {
            checkAndAddStopNode(node);
        }

        potentialStationNodes.clear();
    }

    /**
     * Checks if the node is a train station and
     * adds it to the train stations if that is the case
     */
    private void checkAndAddTrainStation(SimpleNode node) {
        // Check if node is train station main node
        if (node.isStationMainNode() && node.tagsFitForTrainStation()) {

            TrainStation newStation = new TrainStation(node, trainStations.getAmount());
            trainStations.addTrainStation(newStation);
        }
    }

    public void checkAndAddStopNode(SimpleNode node) {
        if (!node.isPartOfTrainStation()) return;
        if (!node.tagsFitForTrainStation()) return;

        String stationName = node.getStationName();
        TrainStation nearestStation = trainStations.getNearestTrainStationUsingKDTree(node.getLatitude(), node.getLongitude());

        // If station name is identical, allow a larger distance
        int maxDistanceInMeters = stationName.equals(nearestStation.getName()) ? 1000 : 250;

        if (OSMUtil.getDistanceInMeters(nearestStation, node) < maxDistanceInMeters) {
            trainStations.registerNodeBelongingToStation(nearestStation, node.getId(), true);
        }
    }
}
