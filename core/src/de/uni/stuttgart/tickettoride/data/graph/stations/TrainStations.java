package de.uni.stuttgart.tickettoride.data.graph.stations;

import org.design9.GeoNode;
import org.design9.kdtree.KDTree;

import java.util.*;

public class TrainStations {

    private final List<TrainStation> stations;
    private final HashMap<Integer, TrainStation> railNodeToStationMap;

    private KDTree<GeoNode> stationsKDTree;

    public TrainStations() {
        stations = new ArrayList<>();
        railNodeToStationMap = new HashMap<>();
    }

    public TrainStation getStationByTrainStationId(int trainStationId) {
        return stations.get(trainStationId);
    }

    public TrainStation getStationByNodeId(int railNodeId) {
        return railNodeToStationMap.get(railNodeId);
    }

    public boolean isPartOfStation(int nodeId) {
        return railNodeToStationMap.containsKey(nodeId);
    }

    public Collection<TrainStation> getAll() {
        return stations;
    }

    public int getAmount() {
        return stations.size();
    }

    public void addTrainStation(TrainStation station) {
        stations.add(station);
    }

    public void createKDTree() {
        stationsKDTree = new KDTree<>(stations);
    }

    private final GeoNode tempNode = new GeoNode();

    public TrainStation getNearestTrainStationUsingKDTree(double latitude, double longitude) {
        tempNode.setPoint(latitude, longitude);

        return (TrainStation) stationsKDTree.findNearest(tempNode);
    }

    public void registerNodeBelongingToStation(TrainStation station, int stopNodeId, boolean isTaggedAsStationNode) {
        railNodeToStationMap.put(stopNodeId, station);
        station.registerNodeBelongingToStation(stopNodeId, isTaggedAsStationNode);
    }
}
