package de.uni.stuttgart.tickettoride.data.graph;

import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.util.OSMUtil;

import java.util.*;
import java.util.stream.Collectors;

public class StationSelection {

    private final StationGraph graph;
    private final List<TrainStation> selectedStations;

    public StationSelection(StationGraph graph) {
        this.graph = graph;
        selectedStations = new ArrayList<>();
    }

    /**
     * Selects a subset of stations out of the station graph
     * <p>
     * Stations are selected based on their station score and
     * distance to other selected stations
     *
     * @param amountOfStations      the amount of stations to select
     * @param distanceRatioExponent the relevance of the minimal distance to other stations
     *                              0 = distance has no relevance,
     *                              the higher the exponent the more relevant is the distance
     */
    public void selectStations(int amountOfStations, float distanceRatioExponent) {

        List<TrainStation> allPotentialStations = graph.getTrainStations().getAll().stream()
                .filter(TrainStation::isInLargestConnectedComponent)
                .filter(station -> graph.getNodeDegree(station.getTrainStationId()) > 0)
                .sorted(
                        (ts1, ts2) -> -(calculateStationScore(graph, ts1) - calculateStationScore(graph, ts2)))
                .collect(Collectors.toList());

        for (int i = 0; i < amountOfStations; i++) {
            float maxDistanceTotal = 0f;

            for (TrainStation potentialStation : allPotentialStations) {
                float minDistance = Float.MAX_VALUE;

                for (TrainStation selectedStation : selectedStations) {
                    float distance = OSMUtil.getDistanceInMeters(potentialStation, selectedStation);

                    if (distance > maxDistanceTotal) {
                        maxDistanceTotal = distance;
                    }
                    if (distance < minDistance) {
                        minDistance = distance;
                    }
                }

                potentialStation.setDistanceToNearestStation(minDistance);
            }


            for (TrainStation station : allPotentialStations) {
                float score = calculateStationScore(graph, station);

                if (selectedStations.size() > 0) {
                    float distanceRatio = station.getDistanceToNearestStation() / maxDistanceTotal;
                    score *= (float) Math.pow(distanceRatio, distanceRatioExponent);
                }

                station.setStationSelectionScore(score);
            }

            Optional<TrainStation> bestStation = allPotentialStations.stream()
                    .max(Comparator.comparingDouble(TrainStation::getStationSelectionScore));

            if (bestStation.isPresent()) {
                selectedStations.add(bestStation.get());
                allPotentialStations.remove(bestStation.get());
            }
        }
    }

    public static int calculateStationScore(StationGraph graph, TrainStation station) {
        return station.getEstimatedAmountOfRails() * 3
                + graph.getNodeDegree(station.getTrainStationId());
    }

    public Set<TrainStation> getSelectedStations() {
        return new HashSet<>(selectedStations);
    }
}
