package de.uni.stuttgart.tickettoride.data.graph.structure;

import de.uni.stuttgart.tickettoride.data.util.OSMUtil;

/**
 * @param <N> type of the node objects
 */
public class GraphEdge<N extends GraphNode> {

    private final N node1;
    private final N node2;

    public GraphEdge(N node1, N node2) {
        this.node1 = node1;
        this.node2 = node2;
    }

    public N getNode1() {
        return node1;
    }

    public N getNode2() {
        return node2;
    }

    public N getDestNode(N srcNode) {
        return (srcNode.equals(node1)) ? node2 : node1;
    }

    public float getLengthInMeters() {
        return OSMUtil.getDistanceInMeters(node1, node2);
    }

    /**
     * Code used and modified from
     * com.badlogic.gdx.math.Intersector.intersectSegments(...)
     */
    public boolean intersects(GraphEdge<N> edge) {
        float x1 = (float) node1.getLongitude();
        float y1 = (float) node1.getLatitude();
        float x2 = (float) node2.getLongitude();
        float y2 = (float) node2.getLatitude();

        float x3 = (float) edge.node1.getLongitude();
        float y3 = (float) edge.node1.getLatitude();
        float x4 = (float) edge.node2.getLongitude();
        float y4 = (float) edge.node2.getLatitude();

        float d = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
        if (d == 0) return false;

        float yd = y1 - y3;
        float xd = x1 - x3;
        float ua = ((x4 - x3) * yd - (y4 - y3) * xd) / d;
        if (ua <= 0.00001 || ua >= 0.9999) return false;

        float ub = ((x2 - x1) * yd - (y2 - y1) * xd) / d;
        if (ub <= 0.00001 || ub >= 0.9999) return false;
        return true;
    }
}
