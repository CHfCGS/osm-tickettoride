package de.uni.stuttgart.tickettoride.data.graph.structure;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import lib.gui.Draw;

public class Route extends GraphEdge<TrainStation> {

    public static final int COLOR_GRAY = 0;
    public static final int COLOR_PINK = 1;
    public static final int COLOR_WHITE = 2;
    public static final int COLOR_BLUE = 3;
    public static final int COLOR_YELLOW = 4;
    public static final int COLOR_ORANGE = 5;
    public static final int COLOR_BLACK = 6;
    public static final int COLOR_RED = 7;
    public static final int COLOR_GREEN = 8;

    private int numSegments;
    private float numSegmentsUnrounded;
    private int[] colors;

    private float maxPerpendicularDistance;
    private float edgeSelectionScore;
    private float minAngleToOtherRoute;

    public Route(TrainStation station1, TrainStation station2) {
        this(station1, station2, 1, 1);
    }

    public Route(TrainStation station1, TrainStation station2, int numSections, int edgeWidth) {
        this(station1, station2, numSections, new int[edgeWidth]);
    }

    public Route(TrainStation station1, TrainStation station2, int numSections, int[] colors) {
        super(station1, station2);
        this.numSegments = numSections;
        this.colors = colors;
    }

    public int[] getColors() {
        return colors;
    }

    public void setEdgeWidth(int edgeWidth) {
        this.colors = new int[edgeWidth];
    }

    public int getEdgeWidth() {
        return colors.length;
    }

    public void setNumSegments(int numSegments) {
        this.numSegments = numSegments;
    }

    public int getNumSegments() {
        return numSegments;
    }

    public void setNumSegmentsUnrounded(float numSegmentsUnrounded) {
        this.numSegmentsUnrounded = numSegmentsUnrounded;
    }

    public float getNumSegmentsUnrounded() {
        return numSegmentsUnrounded;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Route)) return false;

        Route other = (Route) obj;

        return (getNode1().equals(other.getNode1()) && getNode2().equals(other.getNode2()))
                || (getNode1().equals(other.getNode2()) && getNode2().equals(other.getNode1()));
    }

    @Override
    public int hashCode() {
        return getNode1().hashCode() + getNode2().hashCode();
    }

    public boolean contains(TrainStation station) {
        return getNode1().equals(station) || getNode2().equals(station);
    }

    public float getMaxPerpendicularDistance() {
        return maxPerpendicularDistance;
    }

    public void setMaxPerpendicularDistance(float maxPerpendicularDistance) {
        this.maxPerpendicularDistance = maxPerpendicularDistance;
    }

    public void setEdgeSelectionScore(float edgeSelectionScore) {
        this.edgeSelectionScore = edgeSelectionScore;
    }

    public float getEdgeSelectionScore() {
        return edgeSelectionScore;
    }

    public float getMinAngleToOtherRoute() {
        return minAngleToOtherRoute;
    }

    public void setMinAngleToOtherRoute(float minAngleToOtherRoute) {
        this.minAngleToOtherRoute = minAngleToOtherRoute;
    }

    /**
     * Debug method to draw routes on the map
     */
    public void drawDebug(Vector2 pos, Vector2 pos2, float drawSize, Color color) {
        pos = new Vector2(pos);
        pos2 = new Vector2(pos2);

        Vector2 middlePos = new Vector2(pos).add(pos2).scl(0.5f);

        Vector2 dir = new Vector2(pos2).sub(pos);
        Vector2 dirOrthogonal;
        if (dir.y > 0) {
            dirOrthogonal = new Vector2(dir.y, -dir.x).nor();
        } else {
            dirOrthogonal = new Vector2(-dir.y, dir.x).nor();
        }

        pos.mulAdd(dir, 0.025f);

        for (int i = 0; i < getNumSegments(); i++) {

            pos2.set(pos).mulAdd(dir, (1 / (float) getNumSegments()) - 0.05f);

            for (int j = 0; j < getEdgeWidth(); j++) {
                Draw.setColor(Color.DARK_GRAY);
                Draw.line(pos, pos2, drawSize * 1.5f);
                Draw.setColor(color);
                Draw.line(pos, pos2, drawSize * 0.75f);

                pos.mulAdd(dirOrthogonal, drawSize * 1.75f);
                pos2.mulAdd(dirOrthogonal, drawSize * 1.75f);
            }

            pos.mulAdd(dirOrthogonal, -drawSize * 1.75f * getEdgeWidth());
            pos2.mulAdd(dirOrthogonal, -drawSize * 1.75f * getEdgeWidth());

            pos.mulAdd(dir, 1 / (float) getNumSegments());
        }

        Draw.setColor(Color.BLACK);
        Draw.setTextSize(drawSize * 2);
        Draw.font(middlePos.x - drawSize * 30, middlePos.y, drawSize * 60, String.valueOf(getMaxPerpendicularDistance()));
        Draw.setTextSize(drawSize);
    }
}