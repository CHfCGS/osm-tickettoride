package de.uni.stuttgart.tickettoride.data.graph.original_game;

import com.badlogic.gdx.Gdx;
import de.uni.stuttgart.tickettoride.data.graph.export.GraphExport;
import de.uni.stuttgart.tickettoride.data.graph.structure.Route;
import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;

public class CreateOriginalGameGraph {

    public static GameGraphAnalysis gameGraph;

    private static final boolean PRINT_ANALYSIS = false;
    private static final boolean EXPORT_GRAPH = false;

    public static void init() {
        gameGraph = new GameGraphAnalysis();
        createOriginalGameGraph();

        if (PRINT_ANALYSIS) {
            gameGraph.printGraphAnalysis();
        }
        if (EXPORT_GRAPH) {
            GraphExport.exportToMapFile(gameGraph, Gdx.files.local("usa.map"));
        }
    }

    public static void addStation(String name, double lat, double lon) {
        gameGraph.addNode(new TrainStation(name, name.hashCode(), lat, lon));
    }

    public static void addEdge(String station1, String station2, int sections, int... colors) {
        gameGraph.addEdge(new Route(
                gameGraph.getNodeById(station1.hashCode()),
                gameGraph.getNodeById(station2.hashCode()),
                sections, colors));
    }

    private static void createOriginalGameGraph() {
        addStation("Vancouver", 49.2578263, -123.1939435);
        addStation("Seattle", 47.6131746, -122.4821488);
        addStation("Portland", 45.5428688, -122.7944857);
        addStation("San Francisco", 37.757815, -122.5076403);
        addStation("Los Angeles", 34.0207305, -118.6919255);
        addStation("Calgary", 51.0277202, -114.3680182);
        addStation("Helena", 46.6053466, -112.0319591);
        addStation("Salt Lake City", 40.7767833, -112.0605703);
        addStation("Las Vegas", 36.1251958, -115.3150858);
        addStation("Phoenix", 33.6056711, -112.4052442);
        addStation("Winnipeg", 49.853959, -97.292309);
        addStation("Denver", 39.7645187, -104.9951968);
        addStation("Santa Fe", 35.6826126, -106.0530764);
        addStation("El Paso", 31.8113481, -106.5646037);
        addStation("Duluth", 46.7651382, -92.2509884);
        addStation("Omaha", 41.292032, -96.2213337);
        addStation("Kansas City", 39.0921167, -94.8559099);
        addStation("Oklahoma City", 35.4828833, -97.7593897);
        addStation("Dallas", 32.8209296, -97.0117461);
        addStation("Houston", 29.8174782, -95.6814863);
        addStation("Sault St. Marie", 46.5318913, -84.5638879);
        addStation("Chicago", 41.8339042, -88.0121528);
        addStation("Saint Louis", 38.6532851, -90.3835475);
        addStation("Little Rock", 34.723951, -92.476208);
        addStation("New Orleans", 30.0332195, -90.0226499);
        addStation("Toronto", 43.7184038, -79.5181429);
        addStation("Pittsburch", 40.431478, -80.0505407);
        addStation("Nashville", 36.1868683, -87.0654374);
        addStation("Atlanta", 33.7679192, -84.56069);
        addStation("Montéal", 45.5581968, -73.8703871);
        addStation("Boston", 42.3145186, -71.110369);
        addStation("New York", 40.6976701, -74.2598705);
        addStation("Washington", 38.893938, -77.1546615);
        addStation("Raleigh", 35.843965, -78.7851418);
        addStation("Charleston", 32.8212559, -80.1105642);
        addStation("Miami", 25.7825453, -80.2994991);

        addEdge("Vancouver", "Seattle", 1, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Vancouver", "Calgary", 3, Route.COLOR_GRAY);
        addEdge("Seattle", "Calgary", 4, Route.COLOR_GRAY);
        addEdge("Seattle", "Portland", 1, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Seattle", "Helena", 6, Route.COLOR_YELLOW);
        addEdge("Calgary", "Helena", 4, Route.COLOR_GRAY);
        addEdge("Calgary", "Winnipeg", 6, Route.COLOR_WHITE);
        addEdge("Portland", "San Francisco", 5, Route.COLOR_GREEN, Route.COLOR_PINK);
        addEdge("Portland", "Salt Lake City", 6, Route.COLOR_BLUE);
        addEdge("San Francisco", "Salt Lake City", 5, Route.COLOR_ORANGE, Route.COLOR_WHITE);
        addEdge("San Francisco", "Los Angeles", 3, Route.COLOR_YELLOW, Route.COLOR_PINK);
        addEdge("Los Angeles", "Las Vegas", 2, Route.COLOR_GRAY);
        addEdge("Los Angeles", "Phoenix", 3, Route.COLOR_GRAY);
        addEdge("Los Angeles", "El Paso", 6, Route.COLOR_BLACK);
        addEdge("Las Vegas", "Salt Lake City", 3, Route.COLOR_ORANGE);
        addEdge("Salt Lake City", "Helena", 3, Route.COLOR_PINK);
        addEdge("Salt Lake City", "Denver", 3, Route.COLOR_RED, Route.COLOR_YELLOW);
        addEdge("Phoenix", "Denver", 5, Route.COLOR_WHITE);
        addEdge("Phoenix", "Santa Fe", 3, Route.COLOR_GRAY);
        addEdge("Phoenix", "El Paso", 3, Route.COLOR_GRAY);
        addEdge("Santa Fe", "Denver", 2, Route.COLOR_GRAY);
        addEdge("Santa Fe", "Oklahoma City", 3, Route.COLOR_BLUE);
        addEdge("Santa Fe", "El Paso", 2, Route.COLOR_GRAY);
        addEdge("El Paso", "Oklahoma City", 5, Route.COLOR_YELLOW);
        addEdge("El Paso", "Dallas", 4, Route.COLOR_RED);
        addEdge("El Paso", "Houston", 6, Route.COLOR_GREEN);
        addEdge("Winnipeg", "Sault St. Marie", 6, Route.COLOR_GRAY);
        addEdge("Winnipeg", "Helena", 4, Route.COLOR_BLUE);
        addEdge("Winnipeg", "Duluth", 4, Route.COLOR_BLACK);
        addEdge("Helena", "Duluth", 6, Route.COLOR_ORANGE);
        addEdge("Helena", "Omaha", 5, Route.COLOR_RED);
        addEdge("Helena", "Denver", 4, Route.COLOR_GREEN);
        addEdge("Denver", "Omaha", 4, Route.COLOR_PINK);
        addEdge("Denver", "Kansas City", 4, Route.COLOR_BLACK, Route.COLOR_ORANGE);
        addEdge("Denver", "Oklahoma City", 4, Route.COLOR_RED);
        addEdge("Duluth", "Sault St. Marie", 3, Route.COLOR_GRAY);
        addEdge("Duluth", "Toronto", 6, Route.COLOR_PINK);
        addEdge("Duluth", "Chicago", 3, Route.COLOR_RED);
        addEdge("Duluth", "Omaha", 2, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Omaha", "Chicago", 4, Route.COLOR_BLUE);
        addEdge("Omaha", "Kansas City", 1, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Kansas City", "Saint Louis", 2, Route.COLOR_BLUE, Route.COLOR_PINK);
        addEdge("Kansas City", "Oklahoma City", 2, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Oklahoma City", "Little Rock", 2, Route.COLOR_GRAY);
        addEdge("Oklahoma City", "Dallas", 2, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Dallas", "Little Rock", 2, Route.COLOR_GRAY);
        addEdge("Dallas", "Houston", 1, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Houston", "New Orleans", 2, Route.COLOR_GRAY);
        addEdge("Chicago", "Toronto", 4, Route.COLOR_WHITE);
        addEdge("Chicago", "Pittsburch", 3, Route.COLOR_ORANGE, Route.COLOR_BLACK);
        addEdge("Chicago", "Saint Louis", 2, Route.COLOR_GREEN, Route.COLOR_WHITE);
        addEdge("Saint Louis", "Pittsburch", 5, Route.COLOR_GREEN);
        addEdge("Saint Louis", "Nashville", 2, Route.COLOR_GRAY);
        addEdge("Saint Louis", "Little Rock", 2, Route.COLOR_GRAY);
        addEdge("Little Rock", "Nashville", 3, Route.COLOR_WHITE);
        addEdge("Little Rock", "New Orleans", 3, Route.COLOR_GREEN);
        addEdge("Sault St. Marie", "Montéal", 5, Route.COLOR_BLACK);
        addEdge("Sault St. Marie", "Toronto", 2, Route.COLOR_GRAY);
        addEdge("Toronto", "Montéal", 3, Route.COLOR_GRAY);
        addEdge("Toronto", "Pittsburch", 2, Route.COLOR_GRAY);
        addEdge("Montéal", "Boston", 2, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Montéal", "New York", 3, Route.COLOR_BLUE);
        addEdge("Boston", "New York", 2, Route.COLOR_YELLOW, Route.COLOR_RED);
        addEdge("New York", "Pittsburch", 2, Route.COLOR_WHITE, Route.COLOR_GREEN);
        addEdge("New York", "Washington", 2, Route.COLOR_ORANGE, Route.COLOR_BLACK);
        addEdge("Pittsburch", "Washington", 2, Route.COLOR_GRAY);
        addEdge("Pittsburch", "Raleigh", 2, Route.COLOR_GRAY);
        addEdge("Pittsburch", "Nashville", 4, Route.COLOR_YELLOW);
        addEdge("Nashville", "Raleigh", 3, Route.COLOR_BLACK);
        addEdge("Nashville", "Atlanta", 1, Route.COLOR_GRAY);
        addEdge("Washington", "Raleigh", 1, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Raleigh", "Charleston", 2, Route.COLOR_GRAY);
        addEdge("Raleigh", "Atlanta", 2, Route.COLOR_GRAY, Route.COLOR_GRAY);
        addEdge("Charleston", "Atlanta", 2, Route.COLOR_GRAY);
        addEdge("Charleston", "Miami", 4, Route.COLOR_PINK);
        addEdge("Miami", "Atlanta", 5, Route.COLOR_BLUE);
        addEdge("Miami", "New Orleans", 6, Route.COLOR_RED);
        addEdge("New Orleans", "Atlanta", 4, Route.COLOR_YELLOW, Route.COLOR_ORANGE);
    }
}
