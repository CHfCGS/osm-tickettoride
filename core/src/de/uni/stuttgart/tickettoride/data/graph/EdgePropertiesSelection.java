package de.uni.stuttgart.tickettoride.data.graph;

import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.structure.Route;
import de.uni.stuttgart.tickettoride.data.util.GameGraphConfig;
import de.uni.stuttgart.tickettoride.data.util.OSMUtil;

import java.util.*;
import java.util.stream.Collectors;

public class EdgePropertiesSelection {

    private final GameGraph gameGraph;
    private final TreeSet<Route> allAddedRoutes;

    private final HashMap<Route, Integer> routeUsageCounter;

    public EdgePropertiesSelection(GameGraph gameGraph) {
        this.gameGraph = gameGraph;

        routeUsageCounter = new HashMap<>();
        allAddedRoutes = new TreeSet<>(Comparator.comparingDouble(Route::getLengthInMeters));

        for (TrainStation station : gameGraph.getNodes()) {
            allAddedRoutes.addAll(gameGraph.getEdges(station));
        }
    }

    /**
     * Adds the following properties to each route:
     * - Edge width (amount of parallel segments)
     * - Edge length in segments
     */
    public void addEdgeProperties(GameGraphConfig config) {
        int targetAmountOfSegments = config.getAmountOfSegments();

        // 1) Edge width (amount of parallel segments)
        addEdgePropertyWidth();

        // 2) Edge length in segments
        int totalSegments = addEdgePropertyLength(1f);

        float scale = targetAmountOfSegments / (float) totalSegments;

        totalSegments = addEdgePropertyLength(scale);

        if (totalSegments != targetAmountOfSegments) {
            fixFinalDeltaToTarget(totalSegments, targetAmountOfSegments, config);
        }
    }

    /**
     * Each route does already have a length (in segments).
     * In total there are currently currentTotalSegments, which is very near to the targetAmountOfSegments.
     * <p>
     * This method will remove the remaining delta to targetAmountOfSegments
     */
    private void fixFinalDeltaToTarget(int currentTotalSegments, int targetAmountOfSegments, GameGraphConfig config) {
        int amountOfSegmentsToChange = Math.abs(currentTotalSegments - targetAmountOfSegments);
        int segmentsDelta = (currentTotalSegments > targetAmountOfSegments) ? -1 : 1;

        List<Route> sortedRouteList = new ArrayList<>();

        // There are too many segments -> Find best routes, where one segment can be removed
        if (currentTotalSegments > targetAmountOfSegments) {

            sortedRouteList = allAddedRoutes.stream()
                    .filter(route -> route.getNumSegments() > 1)
                    .sorted((r1, r2) ->
                            // Compare the difference between the unrounded value
                            // to the next smaller integer value (than current integer value)
                            Float.compare(
                                    (r1.getNumSegmentsUnrounded() - (r1.getNumSegments() - 1)) / r1.getNumSegments(),
                                    (r2.getNumSegmentsUnrounded() - (r2.getNumSegments() - 1)) / r2.getNumSegments()
                            )
                    )
                    .collect(Collectors.toList());

        }
        // There are too few segments -> Find best routes, where one segment can be added
        else if (currentTotalSegments < targetAmountOfSegments) {

            sortedRouteList = allAddedRoutes.stream()
                    .filter(route -> route.getNumSegments() < config.getMaxEdgeLengthInSegments())
                    .sorted((r1, r2) ->
                            // Compare the difference between the unrounded value
                            // to the next larger integer value (than current integer value)
                            Float.compare(
                                    ((r1.getNumSegments() + 1) - r1.getNumSegmentsUnrounded()) / r1.getNumSegments(),
                                    ((r2.getNumSegments() + 1) - r2.getNumSegmentsUnrounded()) / r2.getNumSegments()
                            )
                    )
                    .collect(Collectors.toList());

        }

        // Add / Remove Segments from Routes
        for (Route route : sortedRouteList) {

            if (amountOfSegmentsToChange == 1) {
                if (route.getEdgeWidth() == 1) {
                    route.setNumSegments(route.getNumSegments() + segmentsDelta);
                    return; // Finished
                }
            } else {
                route.setNumSegments(route.getNumSegments() + segmentsDelta);
                amountOfSegmentsToChange -= route.getEdgeWidth();

                if (amountOfSegmentsToChange == 0) {
                    return; // Finished
                }
            }
        }
    }

    /**
     * @return the amount of total segments of all edges,
     * after calling this method
     */
    private int addEdgePropertyLength(float segmentLengthScale) {

        float longestRoute = allAddedRoutes.last().getLengthInMeters();
        int totalSegments = 0;

        for (Route route : allAddedRoutes) {
            float numSegmentsUnrounded = route.getLengthInMeters() * 6 * segmentLengthScale / longestRoute;
            int numSegments = Math.round(numSegmentsUnrounded);

            totalSegments += numSegments * route.getEdgeWidth();

            route.setNumSegments(Math.min(6, numSegments));
        }

        return totalSegments;
    }

    private void addEdgePropertyWidth() {
        for (TrainStation station : gameGraph.getNodes()) {
            countEdgeUsages(station);
        }

        for (Route route : allAddedRoutes) {
            int routeUsages = routeUsageCounter.getOrDefault(route, 0);

            if (routeUsages >= (gameGraph.getNodes().size() - 1) * 2) {
                route.setEdgeWidth(2);
            } else {
                route.setEdgeWidth(1);
            }
        }
    }

    private void increaseRouteUsageCounter(TrainStation station1, TrainStation station2) {
        Route route = new Route(station1, station2);
        routeUsageCounter.put(route, routeUsageCounter.getOrDefault(route, 0) + 1);
    }

    /**
     * This method performs a dijkstra from startStation to all other train stations.
     * <p>
     * After that the path from all train stations to the start station is followed and
     * each route, which was used increases the counter of that route.
     * <p>
     * This method is called once for every train station
     */
    private void countEdgeUsages(TrainStation startStation) {
        // Dijkstra
        HashMap<TrainStation, Float> costMap = new HashMap<>();
        PriorityQueue<TrainStation> priorityQueue = new PriorityQueue<>(Comparator.comparingDouble(costMap::get));
        HashMap<TrainStation, TrainStation> parentMap = new HashMap<>();

        for (TrainStation station : gameGraph.getNodes()) {
            if (station == startStation) {
                costMap.put(startStation, 0f);
                parentMap.put(startStation, startStation);
            } else {
                costMap.put(station, Float.POSITIVE_INFINITY);
            }

            priorityQueue.add(station);
        }

        while (!priorityQueue.isEmpty()) {
            TrainStation nextStation = priorityQueue.poll();

            for (Route route : gameGraph.getEdges(nextStation)) {
                TrainStation neighborStation = route.getDestNode(nextStation);

                float cost = OSMUtil.getDistanceInMeters(nextStation, neighborStation);

                if (costMap.get(nextStation) + cost < costMap.get(neighborStation)) {
                    costMap.put(neighborStation, costMap.get(nextStation) + cost);

                    parentMap.put(neighborStation, nextStation);
                    priorityQueue.remove(neighborStation);
                    priorityQueue.add(neighborStation);
                }
            }
        }

        // Count route usages
        for (TrainStation destinationStation : gameGraph.getNodes()) {
            TrainStation currentStation = destinationStation;

            while (currentStation != startStation) {
                TrainStation nextStation = parentMap.get(currentStation);
                if (nextStation == null) break;

                increaseRouteUsageCounter(currentStation, nextStation);

                currentStation = nextStation;
            }
        }
    }

}
