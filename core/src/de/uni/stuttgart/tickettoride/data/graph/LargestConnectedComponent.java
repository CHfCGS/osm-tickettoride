package de.uni.stuttgart.tickettoride.data.graph;

import de.uni.stuttgart.tickettoride.data.graph.stations.TrainStation;
import de.uni.stuttgart.tickettoride.data.graph.structure.EfficientGraph;
import de.uni.stuttgart.tickettoride.data.graph.structure.Route;

import java.util.*;

public class LargestConnectedComponent {

    public static void findLargestConnectedComponent(StationGraph stationGraph) {

        // Instead of Set, boolean[] is used:
        // visitedSet.contains(trainStationId) <==> visitedSet[trainStationId]
        // visitedSet.add(trainStationId) <==> visitedSet[trainStationId] = true
        boolean[] visitedSet = new boolean[stationGraph.getTrainStations().getAmount()];

        Set<Integer> largestConnectedComponent = new HashSet<>();

        for (int trainStationId = 0; trainStationId < stationGraph.getTrainStations().getAmount(); trainStationId++) {
            if (visitedSet[trainStationId]) continue;

            Set<Integer> currentConnectedComponent = new HashSet<>();
            Stack<Integer> stationsToVisit = new Stack<>();
            stationsToVisit.push(trainStationId);

            while (stationsToVisit.size() > 0) {
                int stationToVisit = stationsToVisit.pop();

                currentConnectedComponent.add(stationToVisit);
                visitedSet[stationToVisit] = true;

                stationGraph.prepareGetEdges(stationToVisit);

                int neighborStationId;
                while ((neighborStationId = stationGraph.getNextEdge()) != EfficientGraph.NO_EDGE) {
                    if (!visitedSet[neighborStationId]) {
                        stationsToVisit.add(neighborStationId);
                    }
                }
            }

            if (currentConnectedComponent.size() > largestConnectedComponent.size()) {
                largestConnectedComponent = currentConnectedComponent;
            }
        }

        System.out.println("-> " + largestConnectedComponent.size() + " Stations (Largest CC)");

        for (int trainStationId : largestConnectedComponent) {
            stationGraph.getTrainStations().getStationByTrainStationId(trainStationId)
                    .setIsInLargestConnectedComponent(true);
        }
    }

    public static void findLargestConnectedComponent(GameGraph gameGraph) {

        Set<TrainStation> visitedSet = new HashSet<>();
        Set<TrainStation> largestConnectedComponent = new HashSet<>();

        for (TrainStation station : gameGraph.getNodes()) {
            if (visitedSet.contains(station)) continue;

            Set<TrainStation> currentConnectedComponent = new HashSet<>();
            LinkedList<TrainStation> stationsToVisit = new LinkedList<>();
            stationsToVisit.add(station);

            while (stationsToVisit.size() > 0) {
                TrainStation stationToVisit = stationsToVisit.removeFirst();

                currentConnectedComponent.add(stationToVisit);
                visitedSet.add(stationToVisit);

                for (Route edge : gameGraph.getEdges(stationToVisit)) {
                    TrainStation neighborStation = edge.getDestNode(stationToVisit);

                    if (!visitedSet.contains(neighborStation)) {
                        stationsToVisit.add(neighborStation);
                    }
                }
            }

            if (currentConnectedComponent.size() > largestConnectedComponent.size()) {
                largestConnectedComponent = currentConnectedComponent;
            }
        }

        Iterator<TrainStation> iterator = gameGraph.getNodes().iterator();

        while (iterator.hasNext()) {
            if (!largestConnectedComponent.contains(iterator.next())) {
                iterator.remove();
            }
        }
    }
}
