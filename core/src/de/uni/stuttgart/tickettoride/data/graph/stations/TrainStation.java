package de.uni.stuttgart.tickettoride.data.graph.stations;

import de.uni.stuttgart.tickettoride.data.graph.structure.GraphNode;
import de.uni.stuttgart.tickettoride.data.pbf.entities.SimpleNode;
import lib.gui.osm.util.TileMath;
import org.design9.GeoNode;

import java.util.HashSet;
import java.util.Set;

/**
 * The latitude and longitude of the super class GraphNode
 * is the position of the main node of the train station
 */
public class TrainStation extends GeoNode implements GraphNode {

    private final int trainStationId;
    private int osmMainNodeId;
    private final double latitude;
    private final double longitude;

    private final double mapX;
    private final double mapY;

    private String stationName;
    private int estimatedAmountOfRails;

    public final Set<Integer> stationNodes;

    private float stationSelectionScore;
    private float distanceToNearestStation;

    private boolean isInLargestConnectedComponent;

    /**
     * Creates a train station object from a node.
     */
    public TrainStation(SimpleNode mainNode, int trainStationId) {
        this(mainNode.getStationName(), trainStationId, mainNode.getLatitude(), mainNode.getLongitude());
        osmMainNodeId = mainNode.getId();
    }

    public TrainStation(String stationName, int trainStationId, double latitude, double longitude) {
        super(latitude, longitude);

        this.trainStationId = trainStationId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.stationName = stationName;
        this.stationNodes = new HashSet<>();

        this.mapX = TileMath.longitudeToMapX(longitude);
        this.mapY = TileMath.latitudeToMapY(latitude);
    }

    public void registerNodeBelongingToStation(int nodeId, boolean isTaggedAsStationNode) {
        stationNodes.add(nodeId);

        if (isTaggedAsStationNode && nodeId != osmMainNodeId) {
            estimatedAmountOfRails++;
        }
    }

    public String getName() {
        return stationName;
    }

    public void setName(String stationName) {
        this.stationName = stationName;
    }

    public int getEstimatedAmountOfRails() {
        return Math.max(1, estimatedAmountOfRails);
    }

    @Override
    public int getNodeId() {
        return trainStationId;
    }

    public int getTrainStationId() {
        return trainStationId;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    /**
     * @return the longitude of the train station, converted to mapX
     * using {@link TileMath#longitudeToMapX(double)}
     */
    public double getMapX() {
        return mapX;
    }

    /**
     * @return the latitude of the train station, converted to mapY
     * using {@link TileMath#latitudeToMapY(double)}
     */
    public double getMapY() {
        return mapY;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(trainStationId);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TrainStation)) return false;
        return trainStationId == (((TrainStation) obj).trainStationId);
    }

    public float getStationSelectionScore() {
        return stationSelectionScore;
    }

    public void setStationSelectionScore(float stationSelectionScore) {
        this.stationSelectionScore = stationSelectionScore;
    }

    public float getDistanceToNearestStation() {
        return distanceToNearestStation;
    }

    public void setDistanceToNearestStation(float distanceToNearestStation) {
        this.distanceToNearestStation = distanceToNearestStation;
    }

    public boolean isInLargestConnectedComponent() {
        return isInLargestConnectedComponent;
    }

    public void setIsInLargestConnectedComponent(boolean isInLargestConnectedComponent) {
        this.isInLargestConnectedComponent = isInLargestConnectedComponent;
    }
}