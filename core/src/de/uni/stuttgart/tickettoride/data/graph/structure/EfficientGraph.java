package de.uni.stuttgart.tickettoride.data.graph.structure;

import java.util.Arrays;

public class EfficientGraph {

    protected static final int INITIAL_CAPACITY = 1000;
    public static final int NO_EDGE = -1;

    /**
     * Stores for each node (Array index = nodeId)
     * the first index of the first edge in edgesDestinationNodeIds.
     * <p>
     * If the node has no (outgoing) edges, the value will be NO_EDGE
     */
    private int[] edgeOffsetIndices;

    /**
     * The index where a new edge (destinationNodeId)
     * will be inserted into edgesDestinationNodeIds
     */
    protected int newEdgeInsertionIndex = 0;

    /**
     * The array edgeOffsetIndices points to an index i = edgeOffsetIndices[srcNode]
     * in this array for every scrNode (which has an edge).
     * <p>
     * The value of this entry is the dstNode of the edge.
     * If the src node has more (outgoing) edges
     * i = nextEdgeArrayIndices[i] will point to the next index in this array.
     * <p>
     * When i = NO_EDGE, there are no more edges
     */
    private int[] edgesDestinationNodeIds;
    private int[] nextEdgeArrayIndices;

    public EfficientGraph(int amountOfNodes) {
        edgeOffsetIndices = new int[amountOfNodes];
        Arrays.fill(edgeOffsetIndices, NO_EDGE);

        edgesDestinationNodeIds = new int[INITIAL_CAPACITY];
        nextEdgeArrayIndices = new int[INITIAL_CAPACITY];
    }

    private void checkEdgeArraysCapacity() {
        if (newEdgeInsertionIndex >= edgesDestinationNodeIds.length) {
            // +50% capacity when edge arrays are full
            int newCapacity = edgesDestinationNodeIds.length + (edgesDestinationNodeIds.length / 2);

            edgesDestinationNodeIds = Arrays.copyOf(edgesDestinationNodeIds, newCapacity);
            nextEdgeArrayIndices = Arrays.copyOf(nextEdgeArrayIndices, newCapacity);
        }
    }

    public void addEdge(int nodeId1, int nodeId2) {
        if (nodeId1 == nodeId2) return;

        addDirectedEdge(nodeId1, nodeId2);
        addDirectedEdge(nodeId2, nodeId1);
    }

    /**
     * This node checks if this edge already exists and
     * only if this is not the case the edge is added.
     */
    protected void addDirectedEdge(int fromNodeId, int toNodeId) {
        checkEdgeArraysCapacity();

        int edgeOffsetIndex = edgeOffsetIndices[fromNodeId];

        // No edge yet -> Add edge
        if (edgeOffsetIndex == NO_EDGE) {
            edgeOffsetIndices[fromNodeId] = newEdgeInsertionIndex;

            edgesDestinationNodeIds[newEdgeInsertionIndex] = toNodeId;
            nextEdgeArrayIndices[newEdgeInsertionIndex] = NO_EDGE;
            newEdgeInsertionIndex++;
            return;
        }

        int nextEdgeIndex = edgeOffsetIndex;
        int lastEdgeIndex = -1;

        // (nextEdgeIndex != NO_EDGE) is always true before the first iteration
        while (nextEdgeIndex != NO_EDGE) {
            // Edge does already exist -> Do not add edge
            if (edgesDestinationNodeIds[nextEdgeIndex] == toNodeId) return;

            lastEdgeIndex = nextEdgeIndex;
            nextEdgeIndex = nextEdgeArrayIndices[nextEdgeIndex];
        }

        // At this point is (nextEdgeIndex == NO_EDGE) and the edge does not exist yet -> Add edge

        edgesDestinationNodeIds[newEdgeInsertionIndex] = toNodeId;
        nextEdgeArrayIndices[newEdgeInsertionIndex] = NO_EDGE;

        // Link new edge from last edge
        nextEdgeArrayIndices[lastEdgeIndex] = newEdgeInsertionIndex;

        newEdgeInsertionIndex++;
    }

    public int getAmountOfNodes() {
        return edgeOffsetIndices.length;
    }

    public int getNodeDegree(int nodeId) {
        int nodeDegree = 0;

        int nextEdgeArrayIndex = edgeOffsetIndices[nodeId];

        while (nextEdgeArrayIndex != NO_EDGE) {
            nodeDegree++;
            nextEdgeArrayIndex = nextEdgeArrayIndices[nextEdgeArrayIndex];
        }

        return nodeDegree;
    }

    private int nextEdgeArrayIndex;
    protected int currentEdgeArrayIndex;
    private int nextEdgeArrayIndexNested;

    /**
     * Must be called before calling getNextEdge() to get all
     * edges (destNodeIds) for the node srcNodeId
     * <p>
     * Attention: Before calling prepareGetEdges() a second time
     * getNextEdge() must be called until NO_EDGE is returned.
     * <p>
     * For nested loops, use prepareGetEdgesNested(int) and getNextEdgeNested()
     * for the inner loop!
     */
    public void prepareGetEdges(int srcNodeId) {
        nextEdgeArrayIndex = edgeOffsetIndices[srcNodeId];
    }

    /**
     * @return the next destNodeId for the srcNodeId called in prepareGetEdges(int)
     * If there is no next edge, NO_EDGE is returned
     */
    public int getNextEdge() {
        if (nextEdgeArrayIndex == NO_EDGE) {
            return NO_EDGE;
        }

        currentEdgeArrayIndex = nextEdgeArrayIndex;
        nextEdgeArrayIndex = nextEdgeArrayIndices[nextEdgeArrayIndex];

        return edgesDestinationNodeIds[currentEdgeArrayIndex];
    }

    /**
     * See prepareGetEdges(int) for more info
     */
    public void prepareGetEdgesNested(int srcNodeId) {
        nextEdgeArrayIndexNested = edgeOffsetIndices[srcNodeId];
    }

    /**
     * @return the next destNodeId for the srcNodeId called in prepareGetEdgesNested(int)
     * If there is no next edge, NO_EDGE is returned
     */
    public int getNextEdgeNested() {
        if (nextEdgeArrayIndexNested == NO_EDGE) {
            return NO_EDGE;
        }

        int currentEdgeArrayIndex2 = nextEdgeArrayIndexNested;
        nextEdgeArrayIndexNested = nextEdgeArrayIndices[nextEdgeArrayIndexNested];

        return edgesDestinationNodeIds[currentEdgeArrayIndex2];
    }

    public void dispose() {
        edgeOffsetIndices = null;
        edgesDestinationNodeIds = null;
        nextEdgeArrayIndices = null;
    }
}
