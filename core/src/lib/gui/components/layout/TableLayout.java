package lib.gui.components.layout;

import lib.gui.components.UIComponent;
import lib.gui.components.panel.Panel;
import lib.gui.layout.values.Fraction;

public class TableLayout extends Panel {

    public final UIComponent[] components;

    private int rows;
    private int columns;
    private boolean fillRowsFirst;
    private float paddingW = 1f;
    private float paddingH = 1f;

    public TableLayout(UIComponent... components) {
        this.components = components;
    }

    public void setMode(int rows, int columns, boolean fillRowsFirst) {
        this.rows = rows;
        this.columns = columns;
        this.fillRowsFirst = fillRowsFirst;
    }

    public void setPadding(float fractionW, float fractionH) {
        this.paddingW = fractionW;
        this.paddingH = fractionH;
    }

    @Override
    protected void componentInitialized() {
        if (components.length > rows * columns) {
            throw new IllegalArgumentException("Too much components for " + rows + " rows and " + columns + " columns");
        }

        float elementFractionW = 1f / columns;
        float elementFractionH = 1f / rows;

        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                if (getArrayIndex(row, column) >= components.length) continue;

                UIComponent component = components[getArrayIndex(row, column)];

                component.x = new Fraction((column + (0.5f - paddingW * 0.5f)) * elementFractionW, this.w);
                component.y = new Fraction((row + (0.5f - paddingH * 0.5f)) * elementFractionH, this.h);
                component.w = new Fraction(elementFractionW * paddingW, this.w);
                component.h = new Fraction(elementFractionH * paddingH, this.h);
            }
        }
    }

    private int getArrayIndex(int row, int column) {
        if (fillRowsFirst) {
            return row + column * rows;
        } else {
            return column + row * columns;
        }
    }
}
