package lib.gui.components.panel;

import lib.gui.Renderer2D;
import lib.gui.components.UIComponent;
import lib.gui.layout.values.FixedValue;

public abstract class ComponentListPanel<T extends ListComponent> extends ListPanel {

    private Class<T> componentClass;
    private int lastListSize = 0;
    private float componentSize = -1;

    protected boolean centerList = true;

    public ComponentListPanel(Class<T> componentClass) {
        this.componentClass = componentClass;
        alwaysRenderEveryElement = true;
    }

    protected void setListComponentSize(float componentSize) {
        this.componentSize = componentSize;
    }

    protected void updateListStatus(int listSize, float sizePerElement) {
        super.updateListStatus(listSize, sizePerElement);

        if (lastListSize != listSize) {
            updateComponents();
        }

        if (centerList) {
            if (isVerticalList) {
                addPaddingToBorders = heightCanBeScrolled();
            } else {
                addPaddingToBorders = widthCanBeScrolled();
            }
        }

        lastListSize = listSize;
    }

    protected void updateListStatus(int listSize, float sizePerElement, float padding) {
        super.updateListStatus(listSize, sizePerElement, padding);

        if (lastListSize != listSize) {
            updateComponents();
        }

        if (centerList) {
            if (isVerticalList) {
                addPaddingToBorders = heightCanBeScrolled();
            } else {
                addPaddingToBorders = widthCanBeScrolled();
            }
        }

        lastListSize = listSize;
    }

    private void updateComponents() {
        allUiComponents.clear();
        allUiComponents.add(this);

        for (int i = 0; i < listSize; i++) {
            try {
                ListComponent listComponent = componentClass.newInstance();
                listComponent.setIndex(i);
                listComponent.x = new FixedValue(0);
                listComponent.y = new FixedValue(0);
                listComponent.w = new FixedValue(0);
                listComponent.h = new FixedValue(0);
                listComponent.init();

                allUiComponents.add(listComponent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void renderPanel() {
        renderBeforeClipping();

        if (listSize > 0) {
            Renderer2D.setClipBounds(this);
        }

        renderBefore();
        render();
        renderAfter();

        if (listSize > 0) {
            Renderer2D.resetClipBounds();
        }

        renderAfterClipping();
    }

    @Override
    protected final void renderItem(int index, float iX, float iY, float iW, float iH, boolean isPressed) {
        if (index >= allUiComponents.size()) return;

        UIComponent listComponent = allUiComponents.get(index + 1);

        float extraTranslationX = 0;
        float extraTranslationY = 0;

        if (centerList) {
            if (isVerticalList) {
                if (getContentHeight() < h.value()) {
                    extraTranslationY = (h.value() - getContentHeight()) / 2f;
                }
            } else {
                if (getContentWidth() < w.value()) {
                    extraTranslationX = (w.value() - getContentWidth()) / 2f;
                }
            }
        }

        if (componentSize != -1) {
            if (isVerticalList) {
                listComponent.x.updateValue(iX + xTranslation + (iW - componentSize) / 2f + extraTranslationX);
                listComponent.y.updateValue(iY + yTranslation + extraTranslationY);
                listComponent.w.updateValue(componentSize);
                listComponent.h.updateValue(iH);
            } else {
                listComponent.x.updateValue(iX + xTranslation + extraTranslationX);
                listComponent.y.updateValue(iY + yTranslation + (iH - componentSize) / 2f + extraTranslationY);
                listComponent.w.updateValue(iW);
                listComponent.h.updateValue(componentSize);
            }
        } else {
            listComponent.x.updateValue(iX + xTranslation + extraTranslationX);
            listComponent.y.updateValue(iY + yTranslation + extraTranslationY);
            listComponent.w.updateValue(iW);
            listComponent.h.updateValue(iH);
        }
    }
}
