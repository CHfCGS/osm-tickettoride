package lib.gui.components.panel;

import lib.gui.components.ComplexUIComponent;

public class ListComponent extends ComplexUIComponent {
    private int index;

    void setIndex(int index) {
        this.index = index;
    }

    protected int getIndex() {
        return index;
    }
}
