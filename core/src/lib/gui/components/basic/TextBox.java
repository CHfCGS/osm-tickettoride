package lib.gui.components.basic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import lib.gui.Draw;
import lib.gui.GUIManager;
import lib.gui.Renderer2D;
import lib.gui.components.UIComponent;
import lib.gui.util.TextRenderer;

public class TextBox extends UIComponent {

    private static final int KEY_DELETE = 8;
    private static final int KEY_ENTER = 10;
    private static final int KEY_ENTER_DESKTOP = 13;

    private final TextRenderer textRenderer;

    private final StringBuilder input;
    private boolean isInputActive;

    private boolean isPressed;
    private Runnable keyTypedEvent;

    private String characterWhitelist;
    private int characterLimit;
    private Input.OnscreenKeyboardType onscreenKeyboardType = Input.OnscreenKeyboardType.Default;

    /**
     * In ratio of the text box height
     */
    private float maxTextSizeH = 0.4f;

    public TextBox() {
        input = new StringBuilder();
        textRenderer = new TextRenderer();
        characterWhitelist = defaultCharacterWhitelist;
        characterLimit = defaultCharacterLimit;
    }

    // --- defaults ---
    private static String defaultCharacterWhitelist = null;
    private static int defaultCharacterLimit = Integer.MAX_VALUE;

    public static void setDefaultCharacterWhitelist(String defaultCharacterWhitelist) {
        TextBox.defaultCharacterWhitelist = defaultCharacterWhitelist;
    }

    public static void setDefaultCharacterLimit(int defaultCharacterLimit) {
        TextBox.defaultCharacterLimit = defaultCharacterLimit;
    }

    @Override
    protected void componentInitialized() {
        textRenderer.setText(this.componentText);
        textRenderer.moveTextWhenNecessary(true);
    }

    /**
     * @param characterWhitelist the white list encoded as string.
     *                           A valid input would be "0123456789".
     */
    public void setCharacterWhitelist(String characterWhitelist) {
        this.characterWhitelist = characterWhitelist;
    }

    public void setCharacterLimit(int characterLimit) {
        this.characterLimit = characterLimit;
    }

    public void setInput(String input) {
        clearInput();
        this.input.append(input);
        inputHasChanged();
    }

    public void clearInput() {
        this.input.setLength(0);
        inputHasChanged();
    }

    public String getInput() {
        return input.toString();
    }

    public boolean hasInput() {
        return getInput().trim().length() > 0;
    }

    public void setKeyTypedEvent(Runnable keyTypedEvent) {
        this.keyTypedEvent = keyTypedEvent;
    }

    public void setOnscreenKeyboardType(Input.OnscreenKeyboardType onscreenKeyboardType) {
        this.onscreenKeyboardType = onscreenKeyboardType;
    }

    public void enableInput() {
        Gdx.input.setOnscreenKeyboardVisible(true, onscreenKeyboardType);
        isInputActive = true;
    }

    /**
     * In ratio of the text box height
     */
    public void setMaxTextSizeH(float maxTextSizeH) {
        this.maxTextSizeH = maxTextSizeH;
    }

    private void inputHasChanged() {
        this.componentText.setValue(input.toString());
    }

    @Override
    protected void render() {
        if (w.value() == 0 || h.value() == 0) return;
        Draw.setColor(Color.DARK_GRAY);
        Draw.roundedRect(0, 0, w.value(), h.value(), h.value() * 0.15f, 1f);

        Renderer2D.setClipBounds(this);

        textRenderer.setWidth(w.value() * 0.85f);
        textRenderer.setHeight(h.value() * maxTextSizeH);
        textRenderer.render(0, 0, w.value(), h.value());

        // Cursor
        if (isInputActive && isCursorVisible()) {
            Draw.setColor(Color.WHITE);
            Draw.rect(textRenderer.getTextX() + textRenderer.getTextW() + textRenderer.getTextH() / 10f,
                    textRenderer.getTextY() - textRenderer.getTextH() * 0.05f,
                    textRenderer.getTextH() / 5f,
                    textRenderer.getTextH() * 1.1f);
        }

        Renderer2D.resetClipBounds();
    }

    private boolean isCursorVisible() {
        return MathUtils.sin(GUIManager.totalDeltaTime / 200f) > 0;
    }

    @Override
    protected boolean keyTyped(char character) {
        if (!isInputActive) return false;
        if (((int) character) == 0) return true;

        if (character == KEY_DELETE) {
            if (input.length() > 0) {
                input.deleteCharAt(input.length() - 1);
                inputHasChanged();
            }
        } else if (character == KEY_ENTER || character == KEY_ENTER_DESKTOP) {
            callEvent(0, 0);
            setInput("");
        } else {
            characterTyped(character);
        }

        if (keyTypedEvent != null)
            keyTypedEvent.run();
        return true;
    }

    private void characterTyped(char character) {
        if (characterWhitelist != null) {
            // Character is not in whitelist
            if (characterWhitelist.indexOf(character) == -1) return;
        }

        if (input.length() >= characterLimit) {
            return;
        }

        if ((int) character != 0) {
            input.append(character);
            inputHasChanged();
        }
    }

    @Override
    protected boolean touchDown(int x, int y, int pointer) {
        if (isWithinComponent(x, y)) {
            isPressed = true;
            return true;
        }
        return false;
    }

    @Override
    protected boolean touchUp(int x, int y, int pointer) {
        if (isPressed && isWithinComponent(x, y)) {
            enableInput();
            isPressed = false;
            return true;
        }

        if (!isPressed) {
            isInputActive = false;
            Gdx.input.setOnscreenKeyboardVisible(false);
        }

        isPressed = false;
        return false;
    }

    @Override
    protected void componentOffScreen() {
        if (isInputActive) {
            isInputActive = false;
            Gdx.input.setOnscreenKeyboardVisible(false);
        }
    }
}
