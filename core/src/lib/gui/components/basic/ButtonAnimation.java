package lib.gui.components.basic;

public enum ButtonAnimation {
    SCALE_ON_CLICK,
    COLOR_ON_CLICK,
    NOTHING_ON_CLICK
}
