package lib.gui.components.basic;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import lib.gui.Draw;
import lib.gui.Renderer2D;
import lib.gui.components.UIComponent;
import lib.gui.resources.Translation;
import lib.gui.util.TextRenderer;

public class ComboBox extends UIComponent {

    private boolean isOpen;
    private int selectedIndex;

    private float dY;
    private boolean isPressed;
    private int pressedIndex = -1;
    private float animationAlpha;

    private TextureRegion expand;
    private String[] itemKeys;
    private boolean useValues;

    public ComboBox(String... itemKeys) {
        processAllInput = true;
        expand = Renderer2D.getTextureRegion("buttons/expand");
        this.itemKeys = itemKeys;
    }

    public void useValuesInsteadOfKeys(boolean useValues) {
        this.useValues = useValues;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int index) {
        this.selectedIndex = index;
    }

    @Override
    protected void update(float deltaTimeInMs) {
        if (isOpen) {
            if (animationAlpha < 1f) {
                animationAlpha += deltaTimeInMs / 100f;
                animationAlpha = MathUtils.clamp(animationAlpha, 0f, 1f);
            }
        } else {
            if (animationAlpha > 0f) {
                animationAlpha -= deltaTimeInMs / 100f;
                animationAlpha = MathUtils.clamp(animationAlpha, 0f, 1f);
            }
        }
    }

    @Override
    protected void render() {
        dY = h.value() * 1.1f;
        final float pressedMul = 0.75f;
        // Background
        if (isOpen || animationAlpha > 0f) {
            Draw.setAlpha(animationAlpha);
            Draw.setColor(0.471f * pressedMul, 0.541f * pressedMul, 0.541f * pressedMul, 1f);
            float b = h.value() * 0.075f;
            Draw.roundedRect(-b, -b, w.value() + b * 2, h.value() * itemKeys.length + dY + b * 2, h.value() * 0.2f, 1f);
            Draw.setAlpha(1f);
        }

        float mul = isPressed ? pressedMul : 1f;
        Draw.setColor(0.471f * mul, 0.541f * mul, 0.541f * mul, 1f);
        Draw.roundedRect(0, 0, w.value(), h.value(), h.value() * 0.15f, 1f);

        if (isOpen || animationAlpha > 0f) {
            Draw.setAlpha(animationAlpha);
            Draw.setColor(0.471f, 0.541f, 0.541f, 1f);
            Draw.roundedRect(0, dY, w.value(), h.value() * itemKeys.length, h.value() * 0.15f, 1f);

            // Current selected item
            Draw.setColor(0.471f * pressedMul, 0.541f * pressedMul, 0.541f * pressedMul, 1f);
            Draw.roundedRect(0, dY + h.value() * selectedIndex, w.value(), h.value(), h.value() * 0.15f, 1f);

            for (int i = 0; i < itemKeys.length; i++) {
                if (i == pressedIndex) {
                    Draw.setColor(0.471f * pressedMul, 0.541f * pressedMul, 0.541f * pressedMul, 1f);
                    Draw.roundedRect(0, dY + h.value() * pressedIndex, w.value(), h.value(), h.value() * 0.15f, 1f);
                }
                TextRenderer.render(h.value() * 0.33f, dY + i * h.value(), h.value(),
                        useValues ? itemKeys[i] : Translation.get(itemKeys[i]),
                        w.value() * 0.85f, h.value() * 0.35f);
            }
            Draw.setAlpha(1f);
        }

        Draw.resetColor();
        Draw.image(expand, w.value() - h.value(), 0, h.value(), h.value());

        TextRenderer.render(h.value() * 0.33f, 0, h.value(),
                useValues ? itemKeys[selectedIndex] : Translation.get(itemKeys[selectedIndex]),
                w.value() * 0.85f, h.value() * 0.35f);
    }

    @Override
    protected boolean touchDown(int x, int y, int pointer) {
        if (isWithinComponent(x, y)) {
            isPressed = true;
            return true;
        }
        if (isOpen && x >= 0 && x <= w.value()) {
            int index = (int) ((y - dY) / h.value());
            if (y - dY >= 0 && index < itemKeys.length) {
                pressedIndex = index;
                return true;
            }
        }

        return isOpen;
    }

    @Override
    protected boolean touchUp(int x, int y, int pointer) {
        if (isOpen && x >= 0 && x <= w.value()) {
            int index = (int) ((y - dY) / h.value());
            if (y - dY >= 0 && index == pressedIndex && selectedIndex != index) {
                selectedIndex = index;
                callEvent(x, y);
            }
        }
        if (isPressed && isWithinComponent(x, y)) {
            isOpen = !isOpen;
        } else {
            isOpen = false;
        }

        pressedIndex = -1;
        isPressed = false;
        return false;
    }
}
