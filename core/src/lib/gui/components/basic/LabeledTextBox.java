package lib.gui.components.basic;

import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.Width;
import lib.gui.components.panel.Panel;

public class LabeledTextBox extends Panel {

    @Width(w = 0.325)
    private final Label label = new Label();

    @PosX(w = 0.325)
    @Width(w = 0.675)
    private final TextBox textBox = new TextBox();

    @Override
    protected void componentInitialized() {
        label.setText(this.componentText);
        label.setAlignment(Alignment.LEFT);
    }

    public boolean hasInput() {
        return textBox.hasInput();
    }

    public String getInput() {
        return textBox.getInput();
    }

    public void setInput(String input) {
        textBox.setInput(input);
    }

    public void setKeyTypedEvent(Runnable keyTypedEvent) {
        textBox.setKeyTypedEvent(keyTypedEvent);
    }
}
