package lib.gui.components.basic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lib.gui.Draw;
import lib.gui.GUIConfiguration;
import lib.gui.Renderer2D;
import lib.gui.resources.Text;
import lib.gui.util.TextRenderer;

public class TextButton extends Button {

    private TextureRegion buttonTexture;
    private Color buttonColor = Color.BLUE;
    private Color pressedColor = Color.BLUE;
    private Color deactivatedColor = Color.BLUE;

    private static final Color pressedMultiplier = new Color(0.85f, 0.85f, 0.85f, 1f);
    private static final Color deactivatedMultiplier = new Color(0.75f, 0.75f, 0.75f, 1f);

    public static final Color textColorDeactivated = new Color(Color.WHITE).mul(deactivatedMultiplier);

    public static final Color COLOR_BLUE = new Color(0f, 0.463f, 0.788f, 1f);
    public static final Color COLOR_GREEN = new Color(0f, 0.62f, 0.173f, 1f);
    public static final Color COLOR_RED = new Color(0.749f, 0f, 0f, 1f);
    public static final Color COLOR_GRAY = new Color(0.392f, 0.392f, 0.392f, 1f);

    public static final Color COLOR_BLUE_PRESSED = new Color(COLOR_BLUE).mul(pressedMultiplier);
    public static final Color COLOR_GREEN_PRESSED = new Color(COLOR_GREEN).mul(pressedMultiplier);
    public static final Color COLOR_RED_PRESSED = new Color(COLOR_RED).mul(pressedMultiplier);
    public static final Color COLOR_GRAY_PRESSED = new Color(COLOR_GRAY).mul(pressedMultiplier);

    private final TextRenderer textRenderer;

    /**
     * In ratio of the label width
     */
    private float maxTextSizeW = 0.85f;

    /**
     * In ratio of the label height
     */
    private float maxTextSizeH = 0.325f;

    public TextButton() {
        setAnimation(ButtonAnimation.SCALE_ON_CLICK);
        textRenderer = new TextRenderer();
    }

    public TextButton(Color buttonColor) {
        this(buttonColor, new Color(buttonColor).mul(pressedMultiplier));
    }

    public TextButton(Color buttonColor, Color pressedColor) {
        setAnimation(ButtonAnimation.COLOR_ON_CLICK);
        this.buttonColor = buttonColor;
        this.pressedColor = pressedColor;
        this.deactivatedColor = new Color(buttonColor).mul(deactivatedMultiplier);
        buttonTexture = Renderer2D.getTextureRegion(GUIConfiguration.simpleButtonTextureRegionName);

        textRenderer = new TextRenderer();
    }

    @Override
    protected void componentInitialized() {
        super.componentInitialized();
        textRenderer.setText(componentText);
    }

    public void setButtonColor(Color color) {
        buttonColor = new Color(color);
        pressedColor = new Color(buttonColor).mul(pressedMultiplier);
        deactivatedColor = new Color(buttonColor).mul(deactivatedMultiplier);
    }

    public void setText(String value) {
        if (this.componentText == null) {
            this.componentText = new Text();
        }
        this.componentText.setValue(value);
    }

    public void setText(Text text) {
        this.componentText = text;
    }

    public void setTextColor(Color color) {
        textRenderer.setTextColor(color);
    }

    public void underlineText(boolean underline) {
        textRenderer.underlineText(underline);
    }

    /**
     * In ratio of the label width
     * <p>
     * textScale is applied after this limit
     * and ignores this value
     */
    public void setMaxTextSizeW(float maxTextSizeW) {
        this.maxTextSizeW = maxTextSizeW;
    }

    /**
     * In ratio of the label height
     * <p>
     * textScale is applied after this limit
     * and ignores this value
     */
    public void setMaxTextSizeH(float maxTextSizeH) {
        this.maxTextSizeH = maxTextSizeH;
    }

    @Override
    public void render() {
        if (buttonTexture != null) {
            if(!isActive) {
                Draw.setColor(deactivatedColor);
            } else {
                Draw.setColor((animation == ButtonAnimation.COLOR_ON_CLICK) && isPressed ? pressedColor : buttonColor);
            }

            Draw.imageNinePatch(buttonTexture, 0, 0, w.value(), h.value(), GUIConfiguration.simpleButtonNinePatchBorder,
                    h.value() * GUIConfiguration.simpleButtonNinePatchBorderSize);
            Draw.resetColor();
        }

        if (animation == ButtonAnimation.SCALE_ON_CLICK) {
            textRenderer.setTextScale(clickScale);
        } else {
            textRenderer.setTextScale(1f);
        }

        textRenderer.setTextColor(isActive ? Color.WHITE : textColorDeactivated);
        textRenderer.setWidth(w.value() * maxTextSizeW);
        textRenderer.setHeight(h.value() * maxTextSizeH);
        textRenderer.render(0, 0, w.value(), h.value());
    }
}
