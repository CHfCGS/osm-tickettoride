package lib.gui.components.basic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lib.gui.Draw;
import lib.gui.GUIManager;
import lib.gui.Renderer2D;

public class ImageButton extends Button {

    private TextureRegion textureRegion;
    private final Color buttonColorOriginal = new Color(Color.WHITE);
    private final Color buttonColor = new Color(Color.WHITE);

    protected String textureRegionFolder;
    private String textureName;

    private float buttonRenderScale = 1f;
    private boolean keepTextureRatio;

    public ImageButton(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
        this.animation = ButtonAnimation.SCALE_ON_CLICK;
    }

    public ImageButton(String textureName) {
        this(textureName, 1f, ButtonAnimation.SCALE_ON_CLICK);
    }

    public ImageButton(String textureName, float buttonRenderScale) {
        this(textureName, buttonRenderScale, ButtonAnimation.SCALE_ON_CLICK);
    }

    public ImageButton(String textureName, float buttonRenderScale, ButtonAnimation animation) {
        this.textureName = textureName;
        this.buttonRenderScale = buttonRenderScale;
        this.animation = animation;
    }

    public void updateTexture(String textureName) {
        this.textureName = textureName;
        textureRegion = null;
    }

    public void updateTexture(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
    }

    public void setKeepTextureRatio(boolean keepTextureRatio) {
        this.keepTextureRatio = keepTextureRatio;
    }

    @Override
    public void setActive(boolean isActive) {
        boolean valueChanged = super.isActive != isActive;
        super.setActive(isActive);

        if (valueChanged) {
            if (isActive) {
                buttonColor.set(buttonColorOriginal);
            } else {
                this.buttonColorOriginal.set(buttonColor);
                buttonColor.set(Color.GRAY);
            }
        }
    }

    public void setButtonColor(Color buttonColor) {
        this.buttonColorOriginal.set(buttonColor);
        this.buttonColor.set(buttonColor);
    }

    public void setRenderScale(float buttonRenderScale) {
        this.buttonRenderScale = buttonRenderScale;
    }

    @Override
    protected void render() {
        if (textureRegion == null) {
            if (textureRegionFolder == null) {
                textureRegionFolder = GUIManager.getGUIConfig().imageButtonTextureFolder;
            }

            textureRegion = Renderer2D.getTextureRegion(textureRegionFolder + "/" + textureName);
        }

        Draw.setColor(buttonColor);

        float imageX = 0;
        float imageY = 0;
        float imageW = w.value();
        float imageH = h.value();

        if (keepTextureRatio) {
            float textureRatio = textureRegion.getRegionWidth() / (float) textureRegion.getRegionHeight();
            float buttonRatio = w.value() / h.value();

            if (textureRatio > buttonRatio) {
                imageH = h.value() * buttonRatio / textureRatio;
                imageY = (h.value() - imageH) / 2f;
            } else {
                imageW = w.value() * textureRatio / buttonRatio;
                imageX = (w.value() - imageW) / 2f;
            }
        }

        switch (animation) {
            case SCALE_ON_CLICK:
                Draw.image(textureRegion, imageX, imageY, imageW, imageH, buttonRenderScale * clickScale);
                break;
            case COLOR_ON_CLICK:
                if (isPressed) Draw.setColor(0.8f, 0.8f, 0.8f, 1f);
                Draw.image(textureRegion, imageX, imageY, imageW, imageH, buttonRenderScale);
                Draw.setColor(Color.WHITE);
                break;
            default:
                Draw.image(textureRegion, imageX, imageY, imageW, imageH, buttonRenderScale);

        }

        Draw.setColor(Color.WHITE);
    }
}
