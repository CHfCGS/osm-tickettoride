package lib.gui.components.basic;

import com.badlogic.gdx.graphics.Color;
import lib.gui.Draw;
import lib.gui.Renderer2D;
import lib.gui.components.UIComponent;
import lib.gui.util.TextRenderer;

/**
 * Very simple basic spinner UIComponent
 * Controlled by mouse wheel
 */
public class NumberSpinner extends UIComponent {

    private final TextRenderer textRenderer;

    // Value is stored as int, with one decimal place
    private int value;

    private float lastSetValue;
    private int minValue;
    private int maxValue;

    public NumberSpinner(float minValue, float maxValue) {
        textRenderer = new TextRenderer();
        setValue(minValue);

        this.minValue = (int) (minValue * 10);
        this.maxValue = (int) (maxValue * 10);
    }

    public void setValue(float value) {
        this.lastSetValue = value;
        this.value = (int) (value * 10);
        textRenderer.setText(String.valueOf(value));
    }

    public float getValue() {
        return value / 10f;
    }

    @Override
    protected void render() {
        if (w.value() == 0 || h.value() == 0) return;
        Draw.setColor(Color.DARK_GRAY);
        Draw.roundedRect(0, 0, w.value(), h.value(), h.value() * 0.15f, 1f);

        Renderer2D.setClipBounds(this);

        textRenderer.setWidth(w.value() * 0.85f);
        textRenderer.setHeight(h.value() * 0.4f);
        textRenderer.render(0, 0, w.value(), h.value());

        Renderer2D.resetClipBounds();
    }

    private boolean isWithinComponent;

    @Override
    protected boolean mouseMoved(int x, int y) {
        isWithinComponent = isWithinComponent(x, y);
        return false;
    }

    @Override
    protected boolean doubleClick(int x, int y, int pointer) {
        if (isWithinComponent(x, y)) {
            setValue(lastSetValue);
            return true;
        }
        return false;
    }

    @Override
    protected boolean scrolled(int amount) {
        if (isWithinComponent) {
            // Increase value
            if (amount < 0) {
                if (value < 50) value += 2;
                else if (value < 100) value += 5;
                else if (value < 300) value += 10;
                else value += 50;

                if (value > maxValue) value = maxValue;
            }
            // Decrease value
            else {
                if (value <= 50) value -= 2;
                else if (value <= 100) value -= 5;
                else if (value <= 300) value -= 10;
                else value -= 50;

                if (value < minValue) value = minValue;
            }

            textRenderer.setText(String.valueOf(value / 10f));
            return true;
        }

        return false;
    }
}
