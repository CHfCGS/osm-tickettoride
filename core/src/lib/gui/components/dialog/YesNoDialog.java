package lib.gui.components.dialog;

import lib.gui.annotations.dialog.DialogRatio;
import lib.gui.annotations.dialog.Scale;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Width;
import lib.gui.components.basic.TextButton;
import lib.gui.util.TextRenderer;

@Scale(0.85)
@DialogRatio(2)
class YesNoDialog extends Dialog {

    private final TextRenderer textRenderer;

    @PosX(w = -0.19, align = Alignment.CENTER)
    @PosY(h = 0.05, align = Alignment.BOTTOM)
    @Width(h = 0.65)
    @Height(h = 0.2)
    private final TextButton noButton = new TextButton(TextButton.COLOR_RED);

    @PosX(w = 0.19, align = Alignment.CENTER)
    @PosY(h = 0.05, align = Alignment.BOTTOM)
    @Width(h = 0.65)
    @Height(h = 0.2)
    private final TextButton yesButton = new TextButton(TextButton.COLOR_GREEN);

    YesNoDialog() {
        textRenderer = new TextRenderer();
        textRenderer.wrapText(true);
        noButton.setEvent(() -> Dialogs.close(this));
    }

    public void updateContent(String title, String question, String noText, String yesText, boolean invertButtonColors, Runnable yesCallback) {
        super.title.setValue(title);
        textRenderer.setText(question);
        noButton.setText(noText);
        yesButton.setText(yesText);

        if(invertButtonColors) {
            noButton.setButtonColor(TextButton.COLOR_GREEN);
            yesButton.setButtonColor(TextButton.COLOR_RED);
        } else {
            noButton.setButtonColor(TextButton.COLOR_RED);
            yesButton.setButtonColor(TextButton.COLOR_GREEN);
        }

        yesButton.setEvent(() -> {
            Dialogs.close(this);
            yesCallback.run();
        });
    }

    @Override
    protected void render() {
        textRenderer.setWidth(w.value() * 0.85f);
        textRenderer.setHeight(w.value() * 0.0275f);
        textRenderer.render(0, h.value() * 0.05f, w.value(), h.value() * 0.6f);
    }
}
