package lib.gui.components.dialog;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lib.gui.Draw;
import lib.gui.GUIConfiguration;
import lib.gui.Renderer2D;
import lib.gui.components.basic.ButtonAnimation;
import lib.gui.components.basic.ImageButton;
import lib.gui.layout.values.FixedValue;
import lib.gui.util.TextRenderer;

public abstract class Dialog extends DialogBase {

    private static TextureRegion dialogBackground;
    private static TextureRegion dialogCloseButton;
    private static TextureRegion dialogBackButton;
    private final TextRenderer titleText;

    private boolean touchDownOutsideOfDialog;

    protected final ImageButton closeButton;
    protected final ImageButton backButton;

    /**
     * Setting this value to true, limits the dialog size
     * depending on the GUIConfiguration.landscapeMode value
     */
    protected boolean limitDialogSize = false;

    /**
     * This callback is called, when the back button (on the dialog ui) is pressed
     */
    private Runnable backButtonCallback;

    protected Dialog() {
        if (dialogBackground == null) {
            dialogBackground = Renderer2D.getTextureRegion(GUIConfiguration.dialogBackgroundTextureRegionName);
            dialogCloseButton = Renderer2D.getTextureRegion(GUIConfiguration.dialogCloseButtonTextureRegionName);
            dialogBackButton = Renderer2D.getTextureRegion(GUIConfiguration.dialogBackButtonTextureRegionName);
        }

        titleText = new TextRenderer();
        titleText.setTextColor(Color.WHITE);

        closeButton = new ImageButton(dialogCloseButton);
        closeButton.setVisible(!hideCloseButton);
        closeButton.setEvent(() -> Dialogs.close(this));
        closeButton.setAnimation(ButtonAnimation.COLOR_ON_CLICK);
        closeButton.x = new FixedValue(0);
        closeButton.y = new FixedValue(0);
        closeButton.w = new FixedValue(0);
        closeButton.h = new FixedValue(0);

        backButton = new ImageButton(dialogBackButton);
        backButton.setEvent(() -> {
            if (backButtonCallback != null) {
                backButtonCallback.run();
                backButtonCallback = null;
            }
        });
        backButton.setAnimation(ButtonAnimation.COLOR_ON_CLICK);
        backButton.x = new FixedValue(0);
        backButton.y = new FixedValue(0);
        backButton.w = new FixedValue(0);
        backButton.h = new FixedValue(0);

        super.baseDialogScale = 0.785f;
        super.topBorderFraction = 0.16f;
        super.leftBorderFraction = 0.0125f;
        super.rightBorderFraction = 0.0125f;
        super.bottomBorderFraction = 0.0125f;
    }

    protected boolean isBackButtonVisible() {
        return backButtonCallback != null;
    }

    protected void showBackButton(Runnable backButtonCallback) {
        this.backButtonCallback = backButtonCallback;
    }

    protected void hideBackButton() {
        this.backButtonCallback = null;
    }

    @Override
    public void updatePanel(float deltaTimeInMs) {
        super.updatePanel(deltaTimeInMs);

        float screenRatio = Draw.screenH / Draw.screenW;

        float borderWidthMultiplier = 1.01f;
        float borderHeightMultiplier = 1.1f;

        float totalRatio = dialogRatio * borderHeightMultiplier / borderWidthMultiplier;

        float panelWidth;
        float panelHeight;

        if (totalRatio > screenRatio * 0.95f) {
            panelHeight = Draw.screenH;
            panelWidth = panelHeight / dialogRatio;
        } else {
            panelWidth = Draw.screenW;
            panelHeight = panelWidth * dialogRatio;
        }

        if (limitDialogSize) {
            if (GUIConfiguration.landscapeMode) {
                panelHeight = Math.min(Draw.screenW / 1.7f, Draw.screenH);
                panelWidth = panelHeight / dialogRatio;
            } else {
                panelWidth = Math.min(Draw.screenH / 1.7f, Draw.screenW);
                panelHeight = panelWidth * dialogRatio;
            }
        }

        panelHeight *= baseDialogScale;
        panelWidth *= baseDialogScale;

        setDialogSize(panelWidth, panelHeight);
    }

    @Override
    public final void renderPanel() {
        Draw.disableComponentTranslation();
        // "Shadow" in Background
        Draw.setColor(0, 0, 0, 0.6f);
        Draw.rect(0, 0, Draw.screenW, Draw.screenH);
        Draw.resetColor();

        // Dialog Background
        Draw.imageNinePatch(dialogBackground, xDialog, yDialog, wDialog, hDialog,
                GUIConfiguration.dialogBackgroundNinePatchBorder, topBorderSize);

        if (!hideCloseButton) {
            closeButton.w.updateValue(topBorderSize * 1.35f);
            closeButton.h.updateValue(topBorderSize * 1.35f);
            closeButton.x.updateValue(w.value() + leftBorderSize - closeButton.w.value() * 0.875f);
            closeButton.y.updateValue(-closeButton.h.value());
            closeButton.setRenderScale(0.8f);
        }

        backButton.setVisible(isBackButtonVisible());
        if (isBackButtonVisible()) {
            backButton.w.updateValue(topBorderSize * 1.35f);
            backButton.h.updateValue(topBorderSize * 1.35f);
            backButton.x.updateValue(w.value() * 0 - leftBorderSize * 3f - backButton.w.value() * 0.875f * 0f);
            backButton.y.updateValue(-backButton.h.value());
            backButton.setRenderScale(0.8f);
        }

        // Title
        titleText.setText(title);
        titleText.setWidth(wDialog - closeButton.w.value() * 1.75f);
        titleText.setHeight(topBorderSize * 0.35f);
        titleText.render(xDialog, yDialog, wDialog, topBorderSize);

        Draw.enableComponentTranslation();

        render();
    }

    @Override
    protected final float getAnimationScale() {
        double baseScale = Math.sin(getAnimationProgress() * Math.PI * 0.715) * 1.27;

        final double animationScaleDivisor = 20;

        return (float) (baseScale / animationScaleDivisor + (animationScaleDivisor - 1) / animationScaleDivisor);
    }

    @Override
    protected float getAnimationAlpha() {
        return Math.min(getAnimationProgress() * 2f, 1f);
    }

    @Override
    public final boolean touchDownPanel(int x, int y, int pointer) {
        if (!isWithinDialog(x, y)) {
            touchDownOutsideOfDialog = true;
        }

        return touchDown(x, y, pointer);
    }

    @Override
    public final boolean touchUpPanel(int x, int y, int pointer) {
        if (touchDownOutsideOfDialog && !isWithinDialog(x, y)) {
            Dialogs.close(this);
        }
        touchDownOutsideOfDialog = false;

        return touchUp(x, y, pointer);
    }

    /**
     * Return if the dialog should be closed
     * when the back key is pressed
     */
    protected boolean onBackPressed() {
        if (backButtonCallback != null) {
            backButtonCallback.run();
            backButtonCallback = null;
            return false;
        }
        return true;
    }
}
