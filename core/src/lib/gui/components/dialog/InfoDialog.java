package lib.gui.components.dialog;

import lib.gui.annotations.dialog.DialogRatio;
import lib.gui.annotations.dialog.Scale;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Width;
import lib.gui.annotations.text.TextKey;
import lib.gui.components.basic.TextButton;
import lib.gui.util.TextRenderer;

@Scale(0.8)
@DialogRatio(2)
class InfoDialog extends Dialog {

    private final TextRenderer textRenderer;

    @TextKey("ok")
    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.05, align = Alignment.BOTTOM)
    @Width(h = 0.6)
    @Height(h = 0.2)
    private final TextButton okButton;

    InfoDialog() {
        textRenderer = new TextRenderer();
        textRenderer.wrapText(true);

        okButton = new TextButton(TextButton.COLOR_BLUE);
        okButton.setEvent(() -> Dialogs.close(this));
    }

    public void updateContent(String title, String text) {
        super.title.setValue(title);
        textRenderer.setText(text);
    }

    @Override
    protected void render() {
        textRenderer.setWidth(w.value() * 0.85f);
        textRenderer.setHeight(w.value() * 0.0275f);
        textRenderer.render(0, h.value() * 0.05f, w.value(), h.value() * 0.6f);
    }
}
