package lib.gui.components.dialog;

import lib.gui.Draw;
import lib.gui.resources.Translation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dialogs {

    private static final Map<Class<? extends DialogBase>, DialogBase> dialogs = new HashMap<>();
    private static final List<DialogBase> openDialogs = new ArrayList<>();

    static void registerDialog(DialogBase dialog) {
        dialogs.put(dialog.getClass(), dialog);
    }

    public static void open(Class<? extends DialogBase> dialogClass) {
        open(get(dialogClass));
    }

    public static void open(DialogBase dialog) {
        if(openDialogs.contains(dialog)) return;

        dialog.init();
        dialog.dialogOpened();
        dialog.setIsOpen(true);
        openDialogs.add(dialog);
    }

    public static void showInfoDialog(String text) {
        showInfoDialog(Translation.get("info"), text);
    }

    public static void showInfoDialog(String title, String text) {
        try {
            InfoDialog infoDialog = get(InfoDialog.class);
            infoDialog.updateContent(title, text);
            open(infoDialog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showYesNoDialog(String title, String question, Runnable yesCallback) {
        showYesNoDialog(title, question, Translation.get("no"), Translation.get("yes"), false, yesCallback);
    }

    public static void showYesNoDialog(String title, String question, String noText, String yesText, boolean invertButtonColors, Runnable yesCallback) {
        YesNoDialog yesNoDialog = get(YesNoDialog.class);
        yesNoDialog.updateContent(title, question, noText, yesText, invertButtonColors, yesCallback);
        open(yesNoDialog);
    }

    @SuppressWarnings("unchecked")
    public static <T extends DialogBase> T get(Class<T> dialogClass) {

        if (dialogs.containsKey(dialogClass)) {
            return (T) dialogs.get(dialogClass);
        }

        try {
            return dialogClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new RuntimeException("Cannot find dialog object and unable to create a new one");
    }

    public static boolean isOpen(Class<? extends DialogBase> dialogClass) {
        return isOpen(get(dialogClass));
    }

    public static boolean isOpen(DialogBase dialog) {
        if (dialog == null)
            return false;

        return dialog.isOpen();
    }

    public static void close(Class<? extends DialogBase> dialogClass) {
        close(get(dialogClass));
    }

    public static void close(DialogBase dialog) {
        if (isOpen(dialog)) {
            dialog.dialogClosed();
            dialog.componentOffScreenComplex();
            dialog.setIsOpen(false);
        }
    }

    public static void closeCurrentDialog() {
        close(getCurrentDialog());
    }

    public static DialogBase getCurrentDialog() {
        for (int i = openDialogs.size() - 1; i >= 0; i--) {
            if (openDialogs.get(i).isOpen()) {
                return openDialogs.get(i);
            }
        }

        return null;
    }

    public static boolean isDialogOpen() {
        return openDialogs.size() > 0;
    }

    public static void render() {
        for (int i = 0; i < openDialogs.size(); i++) {
            DialogBase dialog = openDialogs.get(i);

            if (dialog.shouldBeRendered()) {
                Draw.setDialogAlpha(dialog.getAnimationAlpha());
                Draw.setComponentTranslation(dialog.x.value(), dialog.y.value());
                dialog.renderComplex();
                Draw.setComponentTranslation(0, 0);
                Draw.setDialogAlpha(1f);
            } else {
                openDialogs.get(i).dialogClosedAnimationEnded();
                openDialogs.remove(i);
                i--;
            }
        }
    }

    public static void update(float deltaTimeInMs) {
        for (int i = 0; i < openDialogs.size(); i++) {
            openDialogs.get(i).updateComplex(deltaTimeInMs);
        }
    }

    public static boolean keyTyped(char character) {
        DialogBase currentDialog = getCurrentDialog();

        if (currentDialog != null && currentDialog.isOpen()) {
            currentDialog.keyTypedComplex(character);
            return true;
        }

        return false;
    }

    public static boolean touchDown(int x, int y, int pointer) {
        DialogBase currentDialog = getCurrentDialog();

        if (currentDialog != null && currentDialog.isOpen()) {
            x -= currentDialog.x.value();
            y -= currentDialog.y.value();

            currentDialog.touchDownComplex(x, y, pointer);
            return true;
        }

        return false;
    }

    public static boolean touchUp(int x, int y, int pointer) {
        DialogBase currentDialog = getCurrentDialog();

        if (currentDialog != null && currentDialog.isOpen()) {
            x -= currentDialog.x.value();
            y -= currentDialog.y.value();

            currentDialog.touchUpComplex(x, y, pointer);
            return true;
        }

        return false;
    }

    public static boolean touchDragged(int x, int y, int pointer) {
        DialogBase currentDialog = getCurrentDialog();

        if (currentDialog != null && currentDialog.isOpen()) {
            x -= currentDialog.x.value();
            y -= currentDialog.y.value();

            currentDialog.touchDraggedComplex(x, y, pointer);
            return true;
        }

        return false;
    }

    public static boolean mouseMoved(int x, int y) {
        DialogBase currentDialog = getCurrentDialog();

        if (currentDialog != null && currentDialog.isOpen()) {
            x -= currentDialog.x.value();
            y -= currentDialog.y.value();

            currentDialog.mouseMovedComplex(x, y);
            return true;
        }

        return false;
    }

    public static boolean scrolled(int amount) {
        DialogBase currentDialog = getCurrentDialog();

        if (currentDialog != null && currentDialog.isOpen()) {
            currentDialog.scrolledComplex(amount);
            return true;
        }

        return false;
    }

    public static boolean onBackPressed() {
        DialogBase currentDialog = getCurrentDialog();

        if (currentDialog != null) {
            if (currentDialog.onBackPressed() && currentDialog.isOpen()) {
                closeCurrentDialog();
            }

            return true;
        }

        return false;
    }
}
