package lib.gui.util;

import lib.gui.resources.Translation;

import java.text.NumberFormat;

public class FormatString {

    public static String format(long value) {
        return NumberFormat.getInstance().format(value);
    }

    public static String formatTime(long timeInMs) {
        int timeInSecondsInt = (int) (timeInMs / 1000);

        int h = timeInSecondsInt / 3600;
        int m = (timeInSecondsInt % 3600) / 60;
        int s = timeInSecondsInt % 60;

        int d = h / 24;
        h = h % 24;

        final String d_t = Translation.get("days");
        final String h_t = Translation.get("hours");
        final String min_t = Translation.get("minutes");
        final String s_t = Translation.get("seconds");

        if (d > 0 && h == 0) {
            return d + d_t;
        } else if (d > 0) {
            return d + d_t + " " + h + h_t;
        } else if (h > 0 && m == 0) {
            return h + h_t;
        } else if (h > 0) {
            return h + h_t + " " + m + min_t;
        } else if (m > 0 && s == 0) {
            return m + min_t;
        } else if (m > 0) {
            return m + min_t + " " + s + s_t;
        } else {
            return s + s_t;
        }
    }

    /**
     * @param time Time in ms
     */
    public static String formatTime2(long time) {
        long sek = time / 1000;

        int m = 0;
        int h = 0;

        while (sek >= 60) {
            sek -= 60;
            m++;
        }
        while (m >= 60) {
            m -= 60;
            h++;
        }

        String zeit = "";

        String hh = "" + h;
        String mm = "";
        String sekk = "";

        if (sek < 10) {
            sekk = "0" + sek;
        } else {
            sekk = "" + sek;
        }

        if (m < 10) {
            mm = "0" + m;
        } else {
            mm = "" + m;
        }

        if (h > 0) {
            zeit = hh + ":" + mm + ":" + sekk;
        } else {
            zeit = mm + ":" + sekk;
        }

        return zeit;
    }
}
