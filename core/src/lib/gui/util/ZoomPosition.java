package lib.gui.util;

public class ZoomPosition {

    // --- Position ---
    public float x, y, w, h;

    // --- Translation ---
    private double xTranslation, yTranslation;

    private double minTranslationX = Integer.MIN_VALUE;
    private double maxTranslationX = Integer.MAX_VALUE;
    private double minTranslationY = Integer.MIN_VALUE;
    private double maxTranslationY = Integer.MAX_VALUE;

    private double deltaXSpeed, deltaYSpeed;

    // --- Zoom ---
    private double xZoom, yZoom;
    private double initialXZoom, initialYZoom;

    private double minZoomX = Double.MIN_VALUE;
    private double maxZoomX = Double.MAX_VALUE;
    private double minZoomY = Double.MIN_VALUE;
    private double maxZoomY = Double.MAX_VALUE;

    private float zoomPointX, zoomPointY;

    private float zoomSpeedX = 1f;
    private float zoomSpeedY = 1f;

    public ZoomPosition(double initialXZoom, double initialYZoom) {
        this.xZoom = initialXZoom;
        this.yZoom = initialYZoom;
        this.initialXZoom = initialXZoom;
        this.initialYZoom = initialYZoom;
    }

    public void reset() {
        xZoom = initialXZoom;
        yZoom = initialYZoom;
        setTranslationX(0);
        setTranslationY(0);
    }

    // --- Translation ---
    public void setTranslationX(double x) {
        xTranslation = x;
        xTranslation = Math.max(xTranslation, minTranslationX);
        xTranslation = Math.min(xTranslation, maxTranslationX);
    }

    public void setTranslationY(double y) {
        yTranslation = y;
        yTranslation = Math.max(yTranslation, minTranslationY);
        yTranslation = Math.min(yTranslation, maxTranslationY);
    }

    public float getTranslationX() {
        return (float) xTranslation;
    }

    public float getTranslationY() {
        return (float) yTranslation;
    }

    public void moveX(int value) {
        setTranslationX(xTranslation + value);
        deltaXSpeed += value / 2.0;
    }

    public void moveY(int value) {
        setTranslationY(getTranslationY() + value);
        deltaYSpeed += value / 2.0;
    }

    public void setMinTranslationX(int minTranslationX) {
        this.minTranslationX = minTranslationX;
    }

    public void setMaxTranslationX(int maxTranslationX) {
        this.maxTranslationX = maxTranslationX;
    }

    public void setMinTranslationY(int minTranslationY) {
        this.minTranslationY = minTranslationY;
    }

    public void setMaxTranslationY(int maxTranslationY) {
        this.maxTranslationY = maxTranslationY;
    }

    // --- Zoom ---
    public void setZoomPointX(float zoomPointX) {
        this.zoomPointX = zoomPointX;
    }

    public void setZoomPointY(float zoomPointY) {
        this.zoomPointY = zoomPointY;
    }

    public void setZoomX(double zoomX) {
        zoomX(zoomX / getZoomX());
    }

    public void setZoomY(double zoomY) {
        zoomY(zoomY / getZoomY());
    }

    public void zoomX(double factor) {
        double zoomOld = xZoom;

        xZoom *= factor;

        if (xZoom > maxZoomX) xZoom = maxZoomX;
        if (xZoom < minZoomX) xZoom = minZoomX;

        factor = xZoom / zoomOld;
        zoomSpeedX *= factor;

        xTranslation += zoomPointX;
        xTranslation *= factor;
        xTranslation -= zoomPointX;
        setTranslationX(xTranslation);
    }

    public void zoomY(double factor) {
        double zoomOld = yZoom;

        yZoom *= factor;

        if (yZoom > maxZoomY) yZoom = maxZoomY;
        if (yZoom < minZoomY) yZoom = minZoomY;

        factor = yZoom / zoomOld;
        zoomSpeedY *= factor;

        yTranslation += zoomPointY;
        yTranslation *= factor;
        yTranslation -= zoomPointY;
        setTranslationY(yTranslation);
    }

    public float getZoomX() {
        return (float) xZoom;
    }

    public float getZoomY() {
        return (float) yZoom;
    }

    public void setMaxZoomX(double max) {
        maxZoomX = max;
    }

    public void setMaxZoomY(double max) {
        maxZoomY = max;
    }

    public void setMinZoomX(double min) {
        minZoomX = min;
    }

    public void setMinZoomY(double min) {
        minZoomY = min;
    }

    public void update(boolean isXPressed, boolean isYPressed) {
        if (deltaXSpeed != 0) {
            deltaXSpeed *= 0.8;

            if (!isXPressed)
                setTranslationX(xTranslation + deltaXSpeed);
        }

        if (deltaYSpeed != 0) {
            deltaYSpeed *= 0.8;

            if (!isYPressed)
                setTranslationY(yTranslation + deltaYSpeed);
        }

        if (zoomSpeedX != 1f) {
            if (zoomSpeedX > 1f) {
                zoomSpeedX = 1 + (zoomSpeedX - 1) / 1.5f;
                if (zoomSpeedX < 1.001f) zoomSpeedX = 1f;
            } else {
                zoomSpeedX = zoomSpeedX + (1 - zoomSpeedX) / 4f;
                if (zoomSpeedX > 0.999f) zoomSpeedX = 1f;
            }

            if(!isXPressed) {
                double zoomOld = xZoom;
                xZoom *= zoomSpeedX;

                if (xZoom > maxZoomX) xZoom = maxZoomX;
                if (xZoom < minZoomX) xZoom = minZoomX;

                double factor = xZoom / zoomOld;

                xTranslation += zoomPointX;
                xTranslation *= factor;
                xTranslation -= zoomPointX;
            }
        }

        if (zoomSpeedY != 1f) {
            if (zoomSpeedY > 1f) {
                zoomSpeedY = 1 + (zoomSpeedY - 1) / 1.5f;
                if (zoomSpeedY < 1.001f) zoomSpeedY = 1f;
            } else {
                zoomSpeedY = zoomSpeedY + (1 - zoomSpeedY) / 4f;
                if (zoomSpeedY > 0.999f) zoomSpeedY = 1f;
            }

            if(!isYPressed) {
                double zoomOld = yZoom;
                yZoom *= zoomSpeedY;

                if (yZoom > maxZoomY) yZoom = maxZoomY;
                if (yZoom < minZoomY) yZoom = minZoomY;

                double factor = yZoom / zoomOld;

                yTranslation += zoomPointY;
                yTranslation *= factor;
                yTranslation -= zoomPointY;
            }
        }

        setTranslationX(xTranslation);
        setTranslationY(yTranslation);
    }

    public int getXIndexAt(float xPos) {
        return (int) ((xPos - xTranslation - x) / xZoom);
    }

    public int getYIndexAt(float yPos) {
        return (int) ((yPos - yTranslation - y) / yZoom);
    }

    public int getXIndexAt(float xPos, int resolution) {
        return (int) ((xPos - xTranslation - x) * resolution / xZoom);
    }

    public int getYIndexAt(float yPos, int resolution) {
        return (int) ((yPos - yTranslation - y) * resolution / yZoom);
    }

    public float getXPositionAt(int index) {
        return x + ((float) xTranslation) + index * ((float) xZoom);
    }

    public float getYPositionAt(int index) {
        return y + ((float) yTranslation) + index * ((float) yZoom);
    }

    public float getXPositionAt(double index) {
        return (float) (x + xTranslation + index * xZoom);
    }

    public float getYPositionAt(double index) {
        return (float) (y + yTranslation + index * yZoom);
    }

    public float getWidth(float deltaIndex) {
        return (float) (deltaIndex * xZoom);
    }

    public float getHeight(float deltaIndex) {
        return (float) (deltaIndex * yZoom);
    }
}
