package lib.gui.resources;

import com.badlogic.gdx.Gdx;
import lib.gui.GUIConfiguration;
import lib.gui.GUIManager;
import lib.gui.util.FormatString;

import java.util.HashMap;

public class Translation {

    private static GUIConfiguration config;

    private static String currentLanguage = "";
    private static HashMap<String, String> texts = new HashMap<>();

    public static void setUp() {
        config = GUIManager.getGUIConfig();
        loadTranslationIfNecessary();
    }

    private static void loadTranslationIfNecessary() {
        if (!currentLanguage.equals(config.appLanguage.toLowerCase())) {
            currentLanguage = config.appLanguage.toLowerCase();
            loadTranslation();
        }
    }

    private static void loadTranslation() {
        texts = new HashMap<>();

        if (!config.translationDirectory.endsWith("/")) {
            config.translationDirectory += "/";
        }

        String fileName = config.translationDirectory + currentLanguage + config.translationFileEnding;
        String fileContent = Gdx.files.internal(fileName).readString(config.translationCharset);

        String[] lines = fileContent.replace("\r", "").split("\n");

        for (String line : lines) {
            if (!line.contains("="))
                continue;

            if (line.trim().startsWith("#"))
                continue;

            int firstEqualsSign = line.indexOf("=");

            String key = line.substring(0, firstEqualsSign).trim();
            String value = line.substring(firstEqualsSign + 1).trim();

            texts.put(key, value);
        }
    }

    public static String get(String key) {
        if (texts.containsKey(key)) {
            return texts.get(key);
        }

        texts.put(key, key);
        System.out.println("Warning: Key " + key + " not found in translation " + currentLanguage);

        return key;
    }

    public static String get(String key, String insertedText) {
        return get(key).replace("<0>", insertedText);
    }

    public static String get(String key, int insertedValue) {
        return get(key).replace("<0>", String.valueOf(insertedValue));
    }
}
