package lib.gui.layout.values;

public class FixedValue extends DynamicValue {

    public static final DynamicValue ZERO = new FixedValue(0);

    private float fixedValue;

    public FixedValue(double fixedValue) {
        this.fixedValue = (float) fixedValue;
    }

    public FixedValue(float fixedValue) {
        this.fixedValue = fixedValue;
    }

    public void updateValue(float fixedValue) {
        if(this == FixedValue.ZERO) {
            throw new RuntimeException("Not allowed to change FixedValue.ZERO");
        }

        this.fixedValue = fixedValue;
    }

    @Override
    public float value() {
        return fixedValue;
    }
}
