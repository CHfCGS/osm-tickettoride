package lib.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.PolygonBatch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.ScreenUtils;
import lib.gui.components.UIComponent;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.screen.Screens;
import lib.gui.resources.TextureManager;

import java.nio.IntBuffer;

/**
 * Use for 2d rendering
 */
public class Renderer2D {

    private Renderer2D() {
    }

    private static Renderer2D instance;

    public static Renderer2D instance() {
        if (instance == null)
            instance = new Renderer2D();

        return instance;
    }

    private GUIConfiguration config;

    private OrthographicCamera camera;

    private PolygonSpriteBatch batch;
    private ShapeRenderer shapeRenderer;

    private TextureManager textureManager;

    /**
     * Initializes all variables required for 2d rendering
     */
    void setUp(GUIConfiguration config) {
        this.config = config;
        batch = new PolygonSpriteBatch();

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Draw.updateScreenResolution(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        batch.setProjectionMatrix(camera.combined);

        textureManager = new TextureManager(config);
        textureManager.load();

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);

        Draw.init(batch, textureManager);
    }

    void render() {
        if (textureManager.isLoading) return;

        ScreenUtils.clear(config.backgroundColor);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);

        batch.begin();

        Screens.render();
        Dialogs.render();
        Screens.renderAlwaysOpenScreens();

        batch.end();
    }

    void resize(int width, int height) {
        camera.setToOrtho(true, width, height);
        Draw.updateScreenResolution(width, height);
    }

    void dispose() {
        batch.dispose();
        shapeRenderer.dispose();
        textureManager.dispose();
    }

    boolean isLoadingResources() {
        return textureManager.isLoading;
    }

    public static PolygonBatch getBatch() {
        return instance.batch;
    }

    /**
     * Only use it if necessary.
     * Does not work by default
     * (begin() and end() needs to be called)
     */
    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public static TextureManager getTextureManager() {
        return instance.textureManager;
    }

    public static TextureRegion getTextureRegion(String name) {
        return instance.textureManager.getTextureRegion(name);
    }

    public static Camera getCamera() {
        return instance.camera;
    }

    /**
     * Limits the area where things are drawn
     * Must be called within the component using this as an argument
     * <p>
     * Attention: Causes a render call
     * (Don't call this method too often during rendering)
     */
    public static void setClipBounds(UIComponent component) {
        setClipBounds(Draw.getComponentTranslationX(), Draw.getComponentTranslationY(), component.w.value(), component.h.value());
    }

    // Used for clip bounds
    private static final Rectangle scissors = new Rectangle();
    private static final Rectangle clipBounds = new Rectangle();

    /**
     * Limits the area where things are drawn
     * <p>
     * Attention: Causes a render call
     * (Don't call this method too often during rendering)
     */
    public static void setClipBounds(float x, float y, float w, float h) {
        getBatch().flush();
        clipBounds.set(x, y, w, h);
        ScissorStack.calculateScissors(getCamera(), getBatch().getTransformMatrix(), clipBounds, scissors);
        ScissorStack.pushScissors(scissors);
    }

    /**
     * Resets the clip bound limit
     */
    public static void resetClipBounds() {
        getBatch().flush();
        ScissorStack.popScissors();
    }

    public int getGlMaxTextureSize() {
        IntBuffer intBuffer = BufferUtils.newIntBuffer(16);
        Gdx.gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, intBuffer);
        return intBuffer.get();
    }
}
