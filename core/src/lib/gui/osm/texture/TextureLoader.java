package lib.gui.osm.texture;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.StreamUtils;
import lib.gui.osm.OSMConfiguration;
import lib.gui.osm.OSMPanel;
import lib.gui.osm.tiles.TilePosition;
import lib.gui.osm.tiles.texture.TileTexture;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class TextureLoader {

    private TilePosition tilePosition;
    private FileHandle tileCacheFile;
    private boolean isLoading;

    private Texture texture;

    /**
     * Loads the Texture (blocking)
     *
     * @return the texture or null if
     * the texture cannot be loaded
     */
    public Texture load(TilePosition tilePosition, FileHandle tileCacheFile) {
        texture = null;
        this.tilePosition = tilePosition;
        this.tileCacheFile = tileCacheFile;

        if (loadCachedTexture()) {
            return texture;
        }

        loadTexture();
        return texture;
    }

    private boolean loadCachedTexture() {

        isLoading = true;

        if (tileCacheFile.exists()) {
            Gdx.app.postRunnable(() -> {
                synchronized (this) {
                    texture = new Texture(tileCacheFile);
                    texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

                    isLoading = false;
                    notify();
                }
            });

            synchronized (this) {
                while (isLoading) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            return true;
        }

        return false;
    }

    private void loadTexture() {
        isLoading = true;

        byte[] bytes = new byte[OSMPanel.getConfig().textureSize * OSMPanel.getConfig().textureSize * 3];
        int numBytes = download(bytes, tilePosition, OSMPanel.getConfig());

        if (numBytes != 0) {
            final Pixmap pixmap = new Pixmap(bytes, 0, numBytes);

            Gdx.app.postRunnable(() -> {
                synchronized (this) {
                    texture = new Texture(pixmap);
                    texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

                    isLoading = false;
                    notify();
                }
            });

            synchronized (this) {
                while (isLoading) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            // Save Image to cache
            PixmapIO.writePNG(tileCacheFile, pixmap);
            pixmap.dispose();
        }
    }

    private static int download(byte[] out, TilePosition pos, OSMConfiguration config) {

        final String url = config.urlBuilder.getUrl(pos);
        InputStream in = null;

        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod("GET");
            conn.addRequestProperty("User-Agent", config.userAgent);
            conn.setDoInput(true);
            conn.setDoOutput(false);
            conn.setUseCaches(true);
            conn.connect();

            in = conn.getInputStream();

            int amountOfBytesRead = 0;

            while (true) {
                int length = in.read(out, amountOfBytesRead, out.length - amountOfBytesRead);

                if (length == -1) break;

                amountOfBytesRead += length;
            }

            return amountOfBytesRead;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            StreamUtils.closeQuietly(in);
        }
    }
}
