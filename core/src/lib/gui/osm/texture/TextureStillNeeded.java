package lib.gui.osm.texture;

@FunctionalInterface
public interface TextureStillNeeded {
    boolean textureStillNeeded();
}
