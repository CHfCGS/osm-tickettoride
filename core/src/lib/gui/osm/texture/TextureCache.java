package lib.gui.osm.texture;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import lib.gui.osm.OSMPanel;
import lib.gui.osm.tiles.TilePosition;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class TextureCache implements Runnable {

    private static TextureCache instance;

    private boolean isRunning = true;

    // Shared texture hash map
    private final HashMap<TilePosition, Texture> textures;
    private final TreeMap<TilePosition, TextureStillNeeded> textureRequests;

    private TextureCache() {
        textures = new HashMap<>();
        textureRequests = new TreeMap<>();

        // Run in two threads
        new Thread(this).start();
        new Thread(this).start();
    }

    public static TextureCache getInstance() {
        if (instance == null) init();
        return instance;
    }

    public static void init() {
        instance = new TextureCache();
    }

    public static void dispose() {
        if (instance != null) {
            instance.isRunning = false;

            synchronized (instance) {
                for (Map.Entry<TilePosition, Texture> tilePositionTextureEntry : instance.textures.entrySet()) {
                    tilePositionTextureEntry.getValue().dispose();
                }
            }

            instance = null;
        }
    }

    public boolean isTextureLoaded(TilePosition tilePosition) {
        return textures.containsKey(tilePosition);
    }

    public Texture getTexture(TilePosition tilePosition) {
        if (isTextureLoaded(tilePosition)) {
            return textures.get(tilePosition);
        }

        return null;
    }

    public void loadTexture(TilePosition tilePosition, TextureStillNeeded textureStillNeeded) {
        if (isTextureLoaded(tilePosition)) return;

        synchronized (this) {
            if (!textureRequests.containsKey(tilePosition)) {
                textureRequests.put(tilePosition, textureStillNeeded);
            }

            notifyAll();
        }
    }

    public void unloadTexture(TilePosition tilePosition) {
        synchronized (this) {
            if (!isRunning) return;

            if (textures.containsKey(tilePosition)) {
                Texture texture = textures.get(tilePosition);
                texture.dispose();
                textures.remove(tilePosition);
            } else {
                textureRequests.remove(tilePosition);
            }
        }
    }

    public int getQueueSize() {
        return textureRequests.size();
    }

    public int getCacheSize() {
        return textures.size();
    }

    private FileHandle getCacheFile(TilePosition pos) {
        return Gdx.files.local(getFilePath(pos));
    }

    private String getFilePath(TilePosition pos) {
        return OSMPanel.getConfig().cacheFolder + "/" + pos.getZoom() + "/" + pos.getX() + "-" + pos.getY() + ".png";
    }

    @Override
    public void run() {
        TextureLoader textureLoader = new TextureLoader();

        while (isRunning) {
            TilePosition tilePosition;

            synchronized (this) {
                try {
                    while (getQueueSize() == 0) {
                        wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Map.Entry<TilePosition, TextureStillNeeded> entry = textureRequests.pollFirstEntry();
                tilePosition = entry.getKey();

                if (!entry.getValue().textureStillNeeded()) {
                    continue;
                }
            }

            Texture texture = textureLoader.load(tilePosition, getCacheFile(tilePosition));

            if (texture == null) {
                // Could not load texture.
            } else {
                if (isRunning) {
                    synchronized (this) {
                        textures.put(tilePosition, texture);
                    }
                }
            }
        }
    }
}
