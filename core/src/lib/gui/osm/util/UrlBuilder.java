package lib.gui.osm.util;

import lib.gui.osm.tiles.TilePosition;

@FunctionalInterface
public interface UrlBuilder {
    String getUrl(TilePosition pos);
}