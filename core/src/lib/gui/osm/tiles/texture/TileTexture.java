package lib.gui.osm.tiles.texture;

import com.badlogic.gdx.graphics.Texture;
import lib.gui.Draw;
import lib.gui.osm.texture.TextureCache;
import lib.gui.osm.tiles.TilePosition;

public class TileTexture {
    private final TilePosition position;
    private Texture texture;

    private long lastTimeTextureUsed;

    public static float lastUpdateDelaTime;
    private long timeSinceTextureLoaded;
    private final float maxAnimationTime = 225;

    public TileTexture(TilePosition position) {
        this.position = position;
        lastTimeTextureUsed = System.currentTimeMillis();
    }

    public void draw(float x, float y, float w, float h, TextureSourcePosition src) {
        draw(x, y, w, h, src, true);
    }

    public void draw(float x, float y, float w, float h, TextureSourcePosition src, boolean drawAnimation) {

        if (texture == null) {
            texture = TextureCache.getInstance().getTexture(position);
        }

        if (drawAnimation) {
            timeSinceTextureLoaded += lastUpdateDelaTime;
        }

        if (texture != null) {
            if (isAnimationActive() && drawAnimation) {
                Draw.setAlpha(timeSinceTextureLoaded / maxAnimationTime);
                Draw.image(texture, x, y, w, h, src.x, src.y, src.w, src.h, 0f, 1f);
                Draw.setAlpha(1f);
            } else {
                Draw.image(texture, x, y, w, h, src.x, src.y, src.w, src.h, 0f, 1f);
            }
        }

        lastTimeTextureUsed = System.currentTimeMillis();
    }

    public boolean isAnimationActive() {
        return timeSinceTextureLoaded < maxAnimationTime;
    }

    public boolean finishedLoading() {
        return TextureCache.getInstance().isTextureLoaded(position);
    }

    public long getLastTimeTextureUsed() {
        return lastTimeTextureUsed;
    }
}
