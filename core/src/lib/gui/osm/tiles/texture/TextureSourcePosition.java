package lib.gui.osm.tiles.texture;

import lib.gui.osm.OSMPanel;

public class TextureSourcePosition {

    private static TextureSourcePosition DEFAULT;

    public static TextureSourcePosition DEFAULT() {
        if(DEFAULT == null) {
            DEFAULT = new TextureSourcePosition(0, 0, OSMPanel.getConfig().textureSize, OSMPanel.getConfig().textureSize);
        }
        return DEFAULT;
    }

    public int x;
    public int y;
    public int w;
    public int h;

    public TextureSourcePosition() {
        this(DEFAULT());
    }

    public TextureSourcePosition(TextureSourcePosition src) {
        this.x = src.x;
        this.y = src.y;
        this.w = src.w;
        this.h = src.h;
    }

    public TextureSourcePosition(int srcX, int srcY, int srcW, int srcH) {
        this.x = srcX;
        this.y = srcY;
        this.w = srcW;
        this.h = srcH;
    }

    public void resetToDefault() {
        this.x = DEFAULT.x;
        this.y = DEFAULT.y;
        this.w = DEFAULT.w;
        this.h = DEFAULT.h;
    }
}
