package lib.gui.osm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.Vector2;
import de.uni.stuttgart.tickettoride.data.graph.structure.GraphNode;
import lib.gui.osm.tiles.texture.TileTexture;
import lib.gui.Draw;
import lib.gui.components.panel.Panel;
import lib.gui.osm.texture.TextureCache;
import lib.gui.osm.tiles.Tile;
import lib.gui.osm.tiles.TileManager;
import lib.gui.osm.util.MapInputProcessor;
import lib.gui.osm.util.MapPosition;
import lib.gui.osm.util.TileMath;
import lib.gui.util.DynamicFrameBuffer;
import org.openstreetmap.osmosis.core.domain.v0_6.Node;

import java.util.ArrayList;

public class OSMPanel extends Panel {

    private static MapPosition mapPosition;
    private static TileManager tileManager;
    private MapInputProcessor mapInputProcessor;

    private DynamicFrameBuffer frameBuffer;

    private static OSMConfiguration config;

    public static OSMConfiguration getConfig() {
        return config;
    }

    public static void setConfig(OSMConfiguration config) {
        OSMPanel.config = config;
    }

    public OSMPanel() {
        if (getConfig() == null) {
            throw new RuntimeException("You must call OSMPanel.setConfig first!");
        }
    }

    @Override
    protected final void componentInitialized() {
        if (mapPosition == null) {
            mapPosition = new MapPosition(this, 0, 0, 300 * w.value() / h.value());
            tileManager = new TileManager(this, mapPosition);

            if (config.saveMapPosition) {
                Preferences prefs = Gdx.app.getPreferences("OSMPanel");

                if (prefs.contains("w") && !Float.isNaN(prefs.getFloat("w"))
                && !Float.isNaN(prefs.getFloat("x")) && !Float.isNaN(prefs.getFloat("y"))) {
                    mapPosition.setVisibleMapW(prefs.getFloat("w"));
                    mapPosition.setMapX(prefs.getFloat("x"));
                    mapPosition.setMapY(prefs.getFloat("y"));
                }
            }
        }

        mapInputProcessor = new MapInputProcessor(this, mapPosition);
    }

    @Override
    protected void pause() {
        if (config.saveMapPosition && mapPosition != null) {
            Preferences prefs = Gdx.app.getPreferences("OSMPanel");
            prefs.putFloat("w", (float) mapPosition.getVisibleMapW());
            prefs.putFloat("x", (float) mapPosition.getMapX());
            prefs.putFloat("y", (float) mapPosition.getMapY());
            prefs.flush();
        }
    }

    public void zoom(double factor) {
        mapPosition.setVisibleMapW(mapPosition.getVisibleMapW() / factor);
    }

    public void moveMap(double dX, double dY) {
        mapPosition.moveMapX((dX * mapPosition.getVisibleMapW()) / w.value());
        mapPosition.moveMapY((dY * mapPosition.getVisibleMapH()) / h.value());
    }

    @Override
    protected void render() {
        if (frameBuffer == null) {
            frameBuffer = new DynamicFrameBuffer(w, h);
        }

        ArrayList<Tile> tiles = tileManager.getVisibleTiles();
        if (tiles.size() == 0) {
            Draw.rect(0, 0, w.value(), h.value(), config.backgroundColor);
            return;
        }

        double tileSizeFraction = 1.0 / (Math.pow(2, tiles.get(0).getPosition().getZoom()));
        double tileSize = (w.value() * tileSizeFraction / mapPosition.getVisibleMapW());

        frameBuffer.begin();

        Draw.rect(0, 0, w.value(), h.value(), config.backgroundColor);

        for (Tile tile : tiles) {
            double tileDeltaX = (w.value() * (tile.getPosition().getMapX() - mapPosition.getMapX()) / mapPosition.getVisibleMapW());
            double tileDeltaY = (h.value() * (tile.getPosition().getMapY() - mapPosition.getMapY()) / mapPosition.getVisibleMapH());

            tile.render(w.value() / 2f + (float) tileDeltaX, h.value() / 2f + (float) tileDeltaY,
                    (float) (tileSize + tileSize / 400f), (float) (tileSize + tileSize / 400f));
        }

        frameBuffer.end();
        frameBuffer.drawFrameBufferTexture(0, 0);
    }

    @Override
    protected void update(float deltaTimeInMs) {
        TileTexture.lastUpdateDelaTime = deltaTimeInMs;
        mapInputProcessor.update();
        mapPosition.update(deltaTimeInMs);
    }

    public Vector2 getPixelPosition(GraphNode node) {
        Vector2 localPixelPosition = getLocalPixelPosition(node.getLatitude(), node.getLongitude());

        localPixelPosition.x += this.x.value();
        localPixelPosition.y += this.y.value();

        return localPixelPosition;
    }

    public Vector2 getPixelPosition(Node node) {
        Vector2 localPixelPosition = getLocalPixelPosition(node.getLatitude(), node.getLongitude());

        localPixelPosition.x += this.x.value();
        localPixelPosition.y += this.y.value();

        return localPixelPosition;
    }

    public Vector2 getMapPixelPosition(float mapX, float mapY) {
        float tileDeltaX = (float) (w.value() * (mapX - mapPosition.getMapX()) / mapPosition.getVisibleMapW());
        float tileDeltaY = (float) (h.value() * (mapY - mapPosition.getMapY()) / mapPosition.getVisibleMapH());

        return new Vector2(
                tileDeltaX + w.value() / 2f + this.x.value(),
                tileDeltaY + h.value() / 2f + this.y.value());
    }

    protected Vector2 getLocalPixelPosition(double latitude, double longitude) {
        float tileDeltaX = (float) (w.value() * (TileMath.longitudeToMapX(longitude) - mapPosition.getMapX()) / mapPosition.getVisibleMapW());
        float tileDeltaY = (float) (h.value() * (TileMath.latitudeToMapY(latitude) - mapPosition.getMapY()) / mapPosition.getVisibleMapH());

        return new Vector2(tileDeltaX + w.value() / 2f, tileDeltaY + h.value() / 2f);
    }

    public MapPosition getMapPosition(float screenX, float screenY) {
        float tileDeltaX = screenX - w.value() / 2;
        float tileDeltaY = screenY - h.value() / 2;

        double longitude = TileMath.mapXToLongitude(((tileDeltaX * mapPosition.getVisibleMapW()) / w.value()) + mapPosition.getMapX());
        double latitude = TileMath.mapYToLatitude(((tileDeltaY * mapPosition.getVisibleMapH()) / h.value()) + mapPosition.getMapY());

        return new MapPosition(this, latitude, longitude, mapPosition.getVisibleDegreesLon());
    }

    public int getTileTextureZoomLevel() {
        return mapPosition.getTileTextureZoomLevel(mapPosition.getVisibleTilesW());
    }

    public float getZoomLevel() {
        return mapPosition.getZoomLevel(mapPosition.getVisibleTilesW());
    }

    public double getVisibleMapWidth() {
        return mapPosition.getVisibleMapW();
    }

    @Override
    protected void dispose() {
        TextureCache.dispose();
        if (tileManager != null) {
            tileManager.dispose();
        }
    }

    /**
     * Attention: This deletes all files in the tile cache folder
     */
    public void clearMapCache() {
        //FileHandle cache = Gdx.files.local("tiles/");
        //cache.deleteDirectory();

        throw new RuntimeException("Not implemented yet");
    }

    public MapPosition getMapPosition() {
        return mapPosition;
    }

    public void zoomMapTo(double latitude, double longitude, double mapW, boolean animate) {
        mapPosition.zoomMapTo(TileMath.longitudeToMapX(longitude), TileMath.latitudeToMapY(latitude), mapW, animate);
    }

    public void zoomMapTo(double lat1, double lon1, double lat2, double lon2, boolean animate) {
        zoomMapToUsingMapXY(
                TileMath.longitudeToMapX(lon1), TileMath.latitudeToMapY(lat1),
                TileMath.longitudeToMapX(lon2), TileMath.latitudeToMapY(lat2),
                animate
        );
    }

    public void zoomMapToUsingMapXY(double mapX1, double mapY1, double mapX2, double mapY2, boolean animate) {
        if (mapX1 == mapX2 || mapY1 == mapY2) return;

        double targetMapX = (mapX1 + mapX2) / 2f;
        double targetMapY = (mapY1 + mapY2) / 2f;
        double targetMapW = Math.abs(mapX1 - mapX2);
        double targetMapH = Math.abs(mapY1 - mapY2);

        float panelRatio = h.value() / w.value();
        double targetRatio = targetMapH / targetMapW;

        boolean useTargetW = panelRatio > targetRatio;

        mapPosition.zoomMapTo(targetMapX, targetMapY, useTargetW ? targetMapW : targetMapH / panelRatio, animate);
    }

    @Override
    protected boolean touchDown(int x, int y, int pointer) {
        return mapInputProcessor.touchDown(x, y, pointer);
    }

    @Override
    protected boolean touchUp(int x, int y, int pointer) {
        return mapInputProcessor.touchUp(x, y, pointer);
    }

    @Override
    protected boolean touchDragged(int x, int y, int pointer) {
        return mapInputProcessor.touchDragged(x, y, pointer);
    }

    @Override
    protected boolean doubleClick(int x, int y, int pointer) {
        if (!isWithinComponent(x, y)) return false;
        if (mapPosition.blockTouchInput()) return false;

        MapPosition pos = getMapPosition(x, y);
        /* Double click to zoom is currently disabled */
        //mapPosition.zoomMapTo(pos.getMapX(), pos.getMapY(), mapPosition.getVisibleMapW() / 1.75f, true);

        return true;
    }

    @Override
    protected boolean mouseMoved(int x, int y) {
        return mapInputProcessor.mouseMoved(x, y);
    }

    @Override
    protected boolean scrolled(int amount) {
        return mapInputProcessor.scrolled(amount);
    }
}
