package lib.gui;

import com.badlogic.gdx.graphics.Color;

public class GUIConfiguration {

    /**
     * The language of the app.
     * <p>
     * If the language is "EN", a "en.txt"
     * (if translationFileEnding == ".txt)
     * file in the translation directory
     * must exist.
     * (The filename must be lowercase)
     * <p>
     * # Can be used to create a comment in the translation file
     */
    public String appLanguage = "EN";

    /**
     * The folder in the assets directory,
     * in which the translation files are saved
     */
    public String translationDirectory = "translation/";

    public String translationCharset = "UTF-8";

    public String translationFileEnding = ".txt";


    /**
     * The default color which is drawn in the background
     */
    public Color backgroundColor = Color.WHITE;

    /**
     * Specifies where in the assets folder the texture atlas is located.
     * <p>
     * [assets/] drawable/atlas.atlas is the default file path.
     */
    public String textureAtlasFilePath = "drawable/atlas.atlas";

    public String imageButtonTextureFolder = "buttons";

    static GUIConfiguration createDefault() {
        return new GUIConfiguration();
    }

    // --- Static final (compile-time) configurations ---

    public static final boolean debugMode = false;

    /**
     * Set this value to true, if the app is in landscape
     * and false, if the app is in portrait mode
     */
    public static final boolean landscapeMode = true;


    /**
     * Specifies the name of the font texture region
     */
    public static final String fontTextureRegionName = "font";

    /**
     * Specifies the file path of the font.fnt file
     */
    public static final String fontFntFilePath = "font/font.fnt";

    /**
     * The resolution of the font in pixels
     */
    public static final int fontResolution = 150;

    /**
     * Used for drawing colored rectangles and more.
     */
    public static final String whitePixelsTextureRegionName = "library_white_pixels";
    public static final String shadowHorizontalTextureRegionName = "library_shadow_horizontal";
    public static final String shadowVerticalTextureRegionName = "library_shadow_vertical";

    // Dialog
    public static final String dialogBackgroundTextureRegionName = "library_dialog";
    public static final String dialogCloseButtonTextureRegionName = "library_dialog_close";
    public static final String dialogBackButtonTextureRegionName = "library_dialog_back";
    public static final int dialogBackgroundNinePatchBorder = 75;

    // Other Textures
    public static final String simpleButtonTextureRegionName = "library_simple_button";
    public static final int simpleButtonNinePatchBorder = 48;
    public static final float simpleButtonNinePatchBorderSize = 0.125f;

    public static final String roundedRectTextureRegionName = "library_simple_button";
    public static final int roundedRectNinePatchBorder = 48;
}
