package lib.gui.annotations.position;

public enum Alignment {
    CENTER,
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
}
