package lib.gui.annotations.position;

import lib.gui.components.UIComponent;
import lib.gui.layout.values.Addition;
import lib.gui.layout.values.DynamicValue;
import lib.gui.layout.values.Fraction;
import lib.gui.layout.values.Subtraction;
import lib.gui.layout.values.TermParser;

public abstract class AnnotationPosition {

    public static DynamicValue getValue(PosX annotation, UIComponent parent, DynamicValue childWidth) {

        if (annotation.align() == Alignment.RIGHT) {
            return getValue(-annotation.w(), -annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(),
                    parent, new Subtraction(parent.w, childWidth));
        }

        if (annotation.align() == Alignment.CENTER) {
            return getValue(annotation.w(), annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(),
                    parent, new Fraction(0.5, new Subtraction(parent.w, childWidth)));
        }

        return getValue(annotation.w(), annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(), parent);
    }

    public static DynamicValue getValue(PosY annotation, UIComponent parent, DynamicValue childHeight) {

        if (annotation.align() == Alignment.BOTTOM) {
            return getValue(-annotation.w(), -annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(),
                    parent, new Subtraction(parent.h, childHeight));
        }

        if (annotation.align() == Alignment.CENTER) {
            return getValue(annotation.w(), annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(),
                    parent, new Fraction(0.5, new Subtraction(parent.h, childHeight)));
        }

        return getValue(annotation.w(), annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(), parent);
    }

    public static DynamicValue getValue(Width annotation, UIComponent parent) {
        return getValue(annotation.w(), annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(), parent);
    }

    public static DynamicValue getValue(Height annotation, UIComponent parent) {
        return getValue(annotation.w(), annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(), parent);
    }

    public static DynamicValue getValue(Size annotation, UIComponent parent) {
        return getValue(annotation.w(), annotation.h(), annotation.screenW(), annotation.screenH(), annotation.term(), parent);
    }

    private static DynamicValue getValue(double fractionW, double fractionH,
                                         double screenFractionW, double screenFractionH,
                                         String term,
                                         UIComponent parent) {
        if (term.length() > 0) {
            return TermParser.parseTerm(term, parent);
        } else if (fractionW != 0) {
            return new Fraction(fractionW, parent.w);
        } else if (fractionH != 0) {
            return new Fraction(fractionH, parent.h);
        } else if (screenFractionW != 0) {
            return new Fraction(screenFractionW, DynamicValue.SCREEN_W);
        } else {
            return new Fraction(screenFractionH, DynamicValue.SCREEN_H);
        }
    }

    private static DynamicValue getValue(double fractionW, double fractionH,
                                         double screenFractionW, double screenFractionH,
                                         String term,
                                         UIComponent parent, DynamicValue additionValue) {

        return new Addition(getValue(fractionW, fractionH, screenFractionW, screenFractionH, term, parent), additionValue);
    }
}
