package lib.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.LifecycleListener;
import lib.gui.components.UIComponent;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.screen.Screens;
import lib.gui.resources.Translation;
import lib.gui.signals.UISignal;

import java.util.ArrayList;
import java.util.List;

public class GUIManager implements InputProcessor, LifecycleListener {

    private static GUIManager instance;
    private static GUIConfiguration config;

    private final Renderer2D renderer2D;

    private final int[] inputButton;
    private final List<Integer> currentTouches;

    /**
     * Only use this in update() or render() methods
     */
    public static long currentTimeMillis;
    public static long nanoTime;

    public static float currentDeltaTime;
    public static float totalDeltaTime;

    public static int totalClickCounter;
    public static int totalClickCounterPointer0;

    private GUIManager(GUIConfiguration config) {
        GUIManager.config = config;

        renderer2D = Renderer2D.instance();
        renderer2D.setUp(config);

        Translation.setUp();

        inputButton = new int[Math.min(Gdx.input.getMaxPointers(), 100)];
        currentTouches = new ArrayList<>(Math.min(Gdx.input.getMaxPointers(), 100));

        Gdx.input.setInputProcessor(this);
        Gdx.app.addLifecycleListener(this);
    }

    public static void setUp() {
        setUp(GUIConfiguration.createDefault());
    }

    public static void setUp(GUIConfiguration config) {
        instance = new GUIManager(config);
    }

    public static void render() {
        instance.update();
        instance.renderer2D.render();
    }

    private long lastUpdateTimeInNs = 0;

    private void update() {
        currentTimeMillis = System.currentTimeMillis();
        nanoTime = System.nanoTime();

        if (lastUpdateTimeInNs == 0) {
            lastUpdateTimeInNs = nanoTime;
        }

        long deltaTimeInNs = nanoTime - lastUpdateTimeInNs;
        lastUpdateTimeInNs = nanoTime;

        float deltaTimeInMs = (deltaTimeInNs / 1_000_000f);

        if (deltaTimeInMs > 1000) {
            deltaTimeInMs = 1000;
        }

        GUIManager.currentDeltaTime = deltaTimeInMs;
        GUIManager.totalDeltaTime += deltaTimeInMs;

        Screens.update(deltaTimeInMs);
        Dialogs.update(deltaTimeInMs);
    }

    public static void broadcastSignal(UISignal signal) {
        UIComponent.broadcastSignal(signal);
    }

    /**
     * Must be called in overridden resize method of ApplicationAdapter
     */
    public static void resize(int width, int height) {
        instance.renderer2D.resize(width, height);
    }

    @Override
    public void pause() {
        UIComponent.pauseComponents();
    }

    @Override
    public void resume() {
        UIComponent.resumeComponents();
    }

    @Override
    public void dispose() {
        renderer2D.dispose();
        UIComponent.disposeComponents();
    }

    /**
     * @return false, if onBackPressed should be handled by the android activity.
     * This will probably finish the current activity
     */
    public static boolean onBackPressed() {
        try {
            if (instance == null) return true;
            if (instance.renderer2D == null) return true;
            if (instance.renderer2D.isLoadingResources()) return true;

            if (Dialogs.onBackPressed()) return true;
            return Screens.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static GUIConfiguration getGUIConfig() {
        return config;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        if (Dialogs.keyTyped(character))
            return true;

        return Screens.keyTyped(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        inputButton[pointer] = button;
        currentTouches.add(pointer);

        totalClickCounter++;
        if (pointer == 0) {
            totalClickCounterPointer0++;
        }

        if (Dialogs.touchDown(screenX, screenY, pointer))
            return true;

        return Screens.touchDown(screenX, screenY, pointer);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        inputButton[pointer] = button;
        currentTouches.remove(Integer.valueOf(pointer));

        if (Dialogs.touchUp(screenX, screenY, pointer))
            return true;

        return Screens.touchUp(screenX, screenY, pointer);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (Dialogs.touchDragged(screenX, screenY, pointer))
            return true;

        return Screens.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (Dialogs.mouseMoved(screenX, screenY))
            return true;

        return Screens.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        if (Dialogs.scrolled((int) amountY))
            return true;

        return Screens.scrolled((int) amountY);
    }

    public static int getInputButton(int pointer) {
        return instance.inputButton[pointer];
    }

    public static int getPointerCount() {
        return instance.currentTouches.size();
    }

    public static int getTouchPointer(int index) {
        return instance.currentTouches.get(index);
    }
}
