# OSM Ticket to Ride

In this project we use OpenStreetMap data to create new maps for the game Ticket to Ride.
The result is a `.map`-file that can be imported in an open source version of TTR by Kiryushin Andrey
(https://github.com/Kiryushin-Andrey/TicketToRide).

## Setup and run project
- Open the gradle project (e.g. using IntelliJ: `File -> Open`).
- After the project has been loaded, open the module `desktop`.
- To start the algorithm, right-click on DesktopLauncher in `desktop/src/de.uni.stuttgart.tickettoride` of the project structure.
- Press DesktopLauncher.main() in the dialog box.
- Enter the file path to the filtered input .pbf-file in the console. 
(A filtered example file is included in this repo `bavaria_filtered.osm.pbf`.
Custom files need to be filtered, see next section for more information)
- The resulting output file is `export.map`


## Input - OSM Data

The algorithm requires a filtered `.pbf`-file as input.
A small example file (bavaria.osm.pbf) is included in this repository.

Additional .pbf-files can be downloaded using Geofabrik (https://download.geofabrik.de/) and filtered with the command line tool Osmium (https://osmcode.org/osmium-tool/)
using the command:

```
osmium tags-filter input.osm.pbf railway=rail,halt,station,stop public_transport=stop_position route=railway,train,tracks -o output.osm.pbf
```


## Output - Configuration

The configuration (amount of stations, edges, segments, ..., in the output file) can be changed in the file `game_graph_config.json`.
